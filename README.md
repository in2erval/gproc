# GProc - XProc authoring made easy.

![GProc logo](/docs/screenshots/logo_gproc.png)
![GProc main screenshot](/docs/screenshots/main_screenshot.png)

GProc is a **graphical authoring tool** for the XML pipeline language [XProc](https://www.w3.org/TR/xproc/). The XML-based syntax of XProc makes it difficult to develop (because of its verbosity) and visualise the pipeline - GProc aims to abstract the concepts in the XProc language as graphical elements that you can manipulate.


## Installing GProc

### Releases

For a pre-compiled release, simply download the archive for the platform and run the appropriate script/executable:

* Linux: [tarball goes here]
* Windows: [zip goes here]

### Running from source

If you want to build/run from source:

1. Clone this git repository using `git clone https://gitlab.com/in2erval/gproc.git`
1. Install the package requirements:
    - Python 3.
    - GTK 3.0 (currently tested on version 3.22.30). On Debian-based systems it's the apt package `libgtk-3-dev`.
    - PyGObject dependencies, such as GObject introspection. More info can be found [here](https://pygobject.readthedocs.io/en/latest/getting_started.html).
    - GooCanvas, a canvas library for GTK. On Debian-based systems it's the apt package `gir1.2-goocanvas-2.0`.
1. Install the python packages. I recommend using virtualenv and then installing using `pip install -r requirements.txt`.
1. Run `main.py` in Python 3.


## Features

Currently, GProc allows you to author pipelines from scratch and export them. Here are some things you can do right now, and incomplete things:

- [x] Create a `p:pipeline`, `p:declare-step`, or `p:library` document.
- [x] Insert atomic steps and specify its type and properties.
- [x] Insert compound steps and view inside the subpipeline.
- [x] Insert multicontainer steps and view its subpipelines - for `p:choose` you can insert multiple cases (`p:when`) and a default case (`p:otherwise`).
- [x] Insert many other XProc language elements into the pipeline.
- [x] Create connections to ports of various steps, and other elements which accept connections.
- [x] Export the visual representation as an XProc document.
- [ ] Specify namespace mappings for the whole document, for `p:with-option` and for `p:with-param`.
- [ ] Create custom step specifications which can be referenced as atomic steps.
- [ ] Save the visual representation to load in again after closing GProc.
- [ ] Import an XProc pipeline into GProc to visualise and manipulate.
- [ ] Customise the interface, including the elements in the visual representation.
- [ ] Display help topics.

For more info, you can check out the [list of features](/docs/features.md).

