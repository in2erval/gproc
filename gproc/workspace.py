"""
This module contains classes related to the workspace, which holds the main content of GProc. This workspace will appear on the right side of the main window.
"""

import gi, os
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.popover         import create_popover
from gproc.publisher       import publisher
from gproc.shared          import data
from gproc.objects.canvas  import GProcCanvas
from gproc.objects.context import GProcCanvasContext
from gproc.utils.enums     import Environment
from gproc.utils.misc      import print_exception
from xproc                 import object_manager, export_to_file 



class GProcWorkspaceTabs(Gtk.Notebook):
    """
    A derived class which implements the workspace tabs. 
    
    This will contain all the logic related to creating, opening, and closing of workspace pages.

    See the docs for Gtk.Notebook: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/Notebook.html
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, margin=5, scrollable=True, **kwargs)

        # Class variables
        self.ether      = {} # For temporarily setting the canvas parents when tab pages are removed
        self.can_delete = False

        # Miscellaneous setup.
        self.get_style_context().add_class("tabs")

        # Connect GTK and publisher signals. 
        self.connect("switch-page",           self.__event_page_change)
        publisher.connect("open_canvas",      self.__event_open_canvas)
        publisher.connect("open_inline",      self.__event_open_inline)
        publisher.connect("close_canvas",     self.__event_close_canvas)
        publisher.connect("close_inline",     self.__event_close_inline)
        publisher.connect("set_held_key",     self.__event_key_change)
        publisher.connect("invoke_export",    self.__event_export)
        publisher.connect("new_document",     self.__event_new_document)
        publisher.connect("update_tab_names", self.__event_update_tab_names)
        # HACK:
        self.connect("button-press-event", self.__event_focus)
        publisher.connect("focus_change",  self.__event_unfocus)


    # ┏━━━━━━━━━━━━━━━━━━━━┓
    # ┃                    ┃
    # ┃ Overridden methods ┃
    # ┃                    ┃
    # ┗━━━━━━━━━━━━━━━━━━━━┛ 

    def remove_page(self, page_num):
        """
        Invoked when a page is being removed. Checks if the removing page is the last page available on the document, and prompts the user before deletion.
        """

        same_pool_pages = [] # Canvas pages.
        other_pages     = [] # Other pages related to the same pool.
        
        # Populate the above lists.
        pool_id = self.get_nth_page(page_num).pool_id
        for i in range(0, self.get_n_pages()):
            if i == page_num: continue # Skip page_num since this is what we're deleting.

            child = self.get_nth_page(i)
            if isinstance(child, GProcCanvasTab):
                if child.pool_id == pool_id: 
                    same_pool_pages.append(i)
            else:
                if child.pool_id == pool_id:
                    other_pages.append(i)

        # Decide whether to delete the page or not based on (or not) user input.
        delete = True
        if len(same_pool_pages) == 0:
            prompt = Gtk.MessageDialog(
                parent       = self.get_toplevel(),
                title        = "Confirmation", #TRANSLATE#
                text         = "Are you sure you want to close this document?", #TRANSLATE#
                buttons      = Gtk.ButtonsType.YES_NO,
                message_type = Gtk.MessageType.QUESTION
            )
            result = prompt.run()
            prompt.close()
            if result == Gtk.ResponseType.YES:
                pages_to_remove = sorted([page_num, *other_pages], reverse=True) # Delete in reverse order so it doesn't mess up the page numbering.
                for page_num in pages_to_remove: super().remove_page(page_num)
            else:
                delete = False
        else: # No need for prompt.
            super().remove_page(page_num)

        if self.get_current_page() == -1: publisher.set_exportable(self, False) # Ran out of pages, no longer exportable.

        return delete


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_page_change(self, widget, page, page_num):
        """
        Unselect the current node, and update exportability.
        """

        # Unselect.
        prev_page = self.get_nth_page(self.get_current_page())
        if isinstance(prev_page, GProcCanvasTab):
            prev_page.canvas.unselect_current_node()

        # Set export.
        if isinstance(page, GProcCanvasTab):
            publisher.set_exportable(self, page.canvas.canvas_context.is_exportable)
        else:
            publisher.set_exportable(self, False)   


    def __event_focus(self, widget, event):
        """
        #HACK# Sets internal flag to be used for deletion to True.
        """

        self.can_delete = True


    def __event_unfocus(self, caller):
        """
        #HACK# Sets internal flag to be used for deletion to False.
        """

        self.can_delete = False


    def __event_open_canvas(self, caller, canvas):
        """
        Create a new page with the canvas, or set the current page to the opening canvas if it already exists as a tab.
        """

        # Find the page with the same canvas in the current set of tabs.
        for i in range(0, self.get_n_pages()):
            child = self.get_nth_page(i)
            if isinstance(child, GProcCanvasTab):
                if child.canvas == canvas:
                    self.set_current_page(i)
                    publisher.set_exportable(self, canvas.canvas_context.is_exportable)
                    return
                
        # None found, so open as a new page.
        self.__open_canvas(canvas)
        self.set_current_page(-1)

    
    def __event_open_inline(self, caller, pool_id, object_handle):
        """
        Create a new page which lets the user edit inline-XML, or set the current page to the editor if it already exists as a tab.
        """

        # Find the page with the same pool ID and object handle in the current set of tabs.
        for i in range(0, self.get_n_pages()):
            child = self.get_nth_page(i)
            if isinstance(child, GProcInlineTab):
                if (child.pool_id == pool_id) and (object_handle == object_handle):
                    self.set_current_page(i)
                    publisher.set_exportable(self, False) # You can't export inline XML, not yet anyway.
                    return

        # None found, so open as a new page.
        self.__open_inline(pool_id, object_handle)
        self.set_current_page(-1)

    
    def __event_close_canvas(self, caller, canvas):
        """
        Closes the page with the given canvas.
        """

        # Find the page with the same canvas in the current set of tabs.
        for i in range(0, self.get_n_pages()):
            child = self.get_nth_page(i)
            if isinstance(child, GProcCanvasTab):
                if child.canvas is canvas:
                    self.remove_page(i)
                    return


    def __event_close_inline(self, caller, pool_id, object_handle):
        """
        Close the inline-XML edit page with the given pool ID and object handle.
        """

        # Find the page with the same pool ID and object handle in the current set of tabs.
        for i in range(0, self.get_n_pages()):
            child = self.get_nth_page(i)
            if isinstance(child, GProcInlineTab):
                if (child.pool_id == pool_id) and (child.object_handle == object_handle):
                    self.remove_page(i)
                    return


    def __event_key_change(self, caller, keyval):
        """
        Detect a delete key and (sends a command to) remove the current canvas' selected node.
        """
        
        if keyval == 65535: # Delete key
            page = self.get_nth_page(self.get_current_page())
            if isinstance(page, GProcCanvasTab) and self.can_delete: 
                page.canvas.delete_current_node()


    def __event_export(self, caller, filename):
        """
        Exports the current page as a file.
        """

        overlay = self.get_nth_page(self.get_current_page())
        if overlay is not None:
            try:
                canvas_context = overlay.canvas.canvas_context
                export_to_file(
                    pool_id           = canvas_context.pool_id, 
                    object_handle     = canvas_context.object_handle, 
                    namespace_mapping = canvas_context.namespace_map, 
                    filename          = filename,
                    export_settings   = {
                        "always-use-extended-options": True
                    }
                ) 
                publisher.infobar_message(self, "Success: Export successful.", Gtk.MessageType.INFO)

            except Exception as e:
                publisher.infobar_message(self, "Error: Failed to export: " + str(e), Gtk.MessageType.ERROR)


    def __event_new_document(self, caller, doc_class, doc_type):
        """
        Creates a new document based on the given step type. Expects either a pipeline, declare-step, or library.
        """

        # Create new pool and root canvas.
        new_pool_id_result = object_manager.create_pool()
        if new_pool_id_result.exception is not None:
            print_exception(new_pool_id_result.exception)
            return

        pool_id           = new_pool_id_result.data
        new_object_result = object_manager.create_object(pool_id, doc_class, doc_type)
        if new_object_result.exception is not None:
            print_exception(new_object_result.exception)
            return
        
        object_handle = new_object_result.data
        environment   = Environment.get_value(doc_type)
        parent_canvas = GProcCanvas(canvas_context=GProcCanvasContext(
            environment   = environment,
            parent_canvas = None,
            parent_node   = None,            
            pool_id       = pool_id,
            object_handle = object_handle
        ))
        parent_canvas.canvas_context.is_exportable = True

        # Create a new node inside the root canvas.
        node = parent_canvas.add_gproc_node("declaration", node_type=doc_type, object_handle=object_handle)
        node.is_moveable   = False
        node.is_deleteable = False

        # Manually open the subpipeline canvas of the created node.
        canvas = node.subpipeline.default.linked_canvas
        self.__open_canvas(canvas)


    def __event_close_button_click(self, widget):
        """
        Close the page corresponding to the clicked close-button.
        """

        # Look through the current pages to find the index.
        page = widget.page_widget # Widget is a Gtk.Button with 'page_widget' attr.
        for index in range(0, self.get_n_pages()):
            if page is self.get_nth_page(index): break

        # Attempt to close the page, move canvas to the ether if it was a canvas tab.
        if self.remove_page(index):
            if (page is not None) and isinstance(page, GProcCanvasTab): 
                self.ether[page.canvas] = Gtk.Layout()
                if page.scrolled_window is not None: page.scrolled_window.remove(page.canvas)
                page.canvas.set_parent(self.ether[page.canvas])


    def __event_update_tab_names(self, caller):
        """
        Refreshes all tab titles to reflect name changes.
        """
        
        for i in range(0, self.get_n_pages()):
            child = self.get_nth_page(i)
            child.update_title()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛

    def __open_canvas(self, canvas):
        """
        Opens the given canvas as a new tab in the workspace.
        """

        # Unparent the canvas and create the new page.
        canvas.unparent() # Remove from the ether if applicable.
        canvas_tab = GProcCanvasTab(canvas=canvas)

        # Create page title bar, with close button.
        close_button = Gtk.Button()
        close_button.set_image(Gtk.Image.new_from_icon_name("window-close", Gtk.IconSize.BUTTON))
        close_button.set_relief(Gtk.ReliefStyle.NONE)
        close_button.page_widget = canvas_tab
        close_button.connect("clicked", self.__event_close_button_click)

        page_actions = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        page_actions.add(canvas_tab.title_label)
        page_actions.add(close_button)
        page_actions.show_all() # GTK bug workaround!

        # Finalise new page creation by adding to the notebook.
        self.append_page(canvas_tab, page_actions)
        self.child_set_property(canvas_tab, "reorderable", True)

        # Show all widgets and notify exportability change.
        self.show_all()
        publisher.set_exportable(self, canvas_tab.canvas.canvas_context.is_exportable)


    def __open_inline(self, pool_id, object_handle):
        """
        Opens an inline-XML editor based on the given pool ID and object handle.
        """

        inline_tab = GProcInlineTab(pool_id=pool_id, object_handle=object_handle)

        # Create page title bar, with close button.
        close_button = Gtk.Button()
        close_button.set_image(Gtk.Image.new_from_icon_name("window-close", Gtk.IconSize.BUTTON))
        close_button.set_relief(Gtk.ReliefStyle.NONE)
        close_button.page_widget = inline_tab
        close_button.connect("clicked", self.__event_close_button_click)

        page_actions = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        page_actions.add(inline_tab.title_label)
        page_actions.add(close_button)
        page_actions.show_all() # GTK bug workaround!

        # Finalise new page creation by adding to the notebook.
        self.append_page(inline_tab, page_actions)
        self.child_set_property(inline_tab, "reorderable", True)

        # Show all widgets and notify exportability change.
        self.show_all()
        publisher.set_exportable(self, False)



class GProcCanvasTab(Gtk.Overlay):
    """
    A derived class which implements a tab page. This will be used as the root page widget of canvas tabs.

    See the docs for Gtk.Overlay: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/Overlay.html
    """

    def __init__(self, *args, canvas, **kwargs):
        super().__init__(*args, **kwargs)

        # Class variables.
        self.canvas          = canvas
        self.pool_id         = canvas.canvas_context.pool_id
        self.scrolled_window = Gtk.ScrolledWindow(None, None)
        self.title_label     = Gtk.Label()

        # Set up hierarchy of widgets.
        self.scrolled_window.add(canvas)
        self.add(self.scrolled_window)

        # Add a floating button for node creation if it's not the root canvas.
        if canvas.canvas_context.parent_canvas is not None:
            # Set up float button.
            self.float_button = Gtk.Button(
                label          = "+", 
                margin_bottom  = 20, 
                margin_right   = 20, 
                halign         = Gtk.Align.END, 
                valign         = Gtk.Align.END,
                focus_on_click = False
            )
            self.float_button.set_size_request(50, 50)
            self.float_button.set_name("float-button")

            # Set up popover and float button click events.
            popover = create_popover(canvas.add_gproc_node, canvas.canvas_context.environment) 
            def show_popover(button):
                popover.set_relative_to(button)
                popover.show_all()
                popover.popup()
            self.float_button.connect("clicked", show_popover)

            self.add_overlay(self.float_button)

        self.update_title()

        # Logic for passing the size of the tab to the canvas.
        self.__allocated = False
        self.connect("size-allocate", self.__event_draw)


    def update_title(self):
        """
        Sets the title of the page based on the node it's associated with.  
        """

        parent_node = self.canvas.canvas_context.parent_node

        if parent_node is None:
            # Root canvas.
            self.title_label.set_text("Root view")
        elif parent_node.node_class == "multicontainer":
            # For multicontainers, also append the subpipeline details.
            self.title_label.set_text("{} ({})".format(
                str(parent_node).replace('\n', ' '), 
                parent_node.subpipeline.get_subpipeline_title(self.canvas.canvas_context.object_handle)
            ))
        else:
            # Default to simple string representation
            self.title_label.set_text(str(parent_node).replace('\n', ' '))


    def __event_draw(self, *args):
        """
        When the page has been allocated a size in the window, update the canvas bounds.
        I don't know why this is here at the moment, I think I tried to fix some things...        
        """

        # Do only once.
        if not self.__allocated:
            self.canvas.update_bounds()
            self.__allocated = True



class GProcInlineTab(Gtk.Frame):
    """
    A derived class which implements a tab page. This will be used as the root page widget of inline-XML editor tabs.

    See the docs for Gtk.Frame: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/Frame.html
    """
    
    def __init__(self, *args, pool_id, object_handle, **kwargs):
        super().__init__(
            *args, 
            label       = "Contents", #TRANSLATE#
            margin      = 6, 
            expand      = True, 
            shadow_type = Gtk.ShadowType.NONE,
            **kwargs
        )

        # Class variables.
        self.pool_id       = pool_id
        self.object_handle = object_handle
        self.title_label   = Gtk.Label()

        # Create the main TextView.
        self.text_view = Gtk.TextView(
            margin_top    = 7,
            margin_bottom = 7,
            margin_left   = 4,
            margin_right  = 7
        )
        self.text_view.get_style_context().add_class("monospace")    
        self.text_view.get_buffer().connect("changed", self.__event_inline_buffer_change)

        # Set up scrolled window.
        scrolled_window = Gtk.ScrolledWindow(None, None, margin_top=4)
        scrolled_window.set_shadow_type(Gtk.ShadowType.IN)

        # Set up hierarchy of widgets.
        scrolled_window.add(self.text_view)
        self.add(scrolled_window)

        self.populate_contents()
        self.update_title()


    def populate_contents(self):
        """
        Gets the current contents based on pool ID and object handle.
        """

        # Check success of object manager methods.
        get_contents_result = object_manager.get_additionals(pool_id=self.pool_id, object_handle=self.object_handle)
        if get_contents_result.exception is None:
            self.text_view.get_buffer().set_text(get_contents_result.data.get("contents", ""))


    def update_title(self):
        """
        Sets the title of the page based on the inline it's associated with.  
        """

        # TODO: So, I think that just "Inline XML" is a little bit vague. But I can't really think of a 
        # better way to express 'which' p:inline the tab belongs to...
        self.title_label.set_text("Inline XML")


    def __event_inline_buffer_change(self, widget):
        """
        Sets the current contents based on pool ID and object handle.
        """

        # Check success of object manager methods, print error if something went wrong.
        set_contents_result = object_manager.set_additional(
            pool_id          = self.pool_id,
            object_handle    = self.object_handle,
            additional_name  = "contents",
            additional_value = widget.props.text
        )
        if set_contents_result.exception is not None: print_exception(set_contents_result.exception)