"""
This module contains classes for step-related canvas node elements.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.entity      import GProcCanvasEntity
from gproc.objects.subpipeline import GProcCanvasSubpipelineComponent
from gproc.publisher           import publisher
from gproc.shared              import theme
from gproc.utils.enums         import AggregateStepType
from gproc.utils.errors        import GProcError
from gproc.utils.misc          import str_none_or_empty, is_qname, is_ncname, print_exception, XPROC_NAMESPACE
from gproc.utils.shapes        import Shapes
from xproc                     import object_manager



class GProcCanvasStep(GProcCanvasEntity):
    """
    A descriptor class for steps. It provides one override, but otherwise is used for identifying step nodes.
    """

    def notify_incoming_port_change(self, connection, old_port_name):
        """
        Determine whether to connect or disconnect based on the new port name.
        """

        if (connection in self.input_connections.keys()) and (not str_none_or_empty(self.input_connections[connection])):
            new_port_name = connection.from_port

            # from_port was changed to a valid port.
            if str_none_or_empty(new_port_name):
                # TODO: Change this to a 'change_entity_port_on_connection' or something?
                connect_result = object_manager.connect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.input_connections[connection],
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = new_port_name
                )
                if connect_result.exception is not None: raise connect_result.exception

            # from_port was removed.
            else:
                object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.input_connections[connection],
                    from_object_handle = connection.from_node.get_object_handle()
                )



class GProcCanvasAtomicStep(GProcCanvasStep):
    """
    The class which implements an atomic step on the canvas. 
    """
    
    def __init__(self, *args, node_type, **kwargs):
        # Validate the type.
        validate_qname = is_qname(node_type)
        if not validate_qname[0]: raise GProcError("Expected a valid qualified name for node_type.")

        # Also the name if given.
        if "node_name" in kwargs.keys():
            if not is_ncname(kwargs["node_name"])[0]: del kwargs["node_name"] # Delete arg before it gets passed to parent constructor.

        super().__init__(
            *args, 
            node_class       = "atomic",
            node_type        = validate_qname[2],
            node_ns_prefix   = validate_qname[1],
            container_width  = float(theme["ENTITY_NODE_WIDTH"]),
            container_height = float(theme["ENTITY_NODE_HEIGHT"]),
            **kwargs
        )

        # For atomic steps, we keep track of connected with-option and with-param differently.
        self.with_options = []
        self.with_params  = []

        # Set up UI.
        self.container = GooCanvas.CanvasRect(
            **self.container_properties,
            parent   = self,
            width    = self.container_width,
            height   = self.container_height,
            radius_x = float(theme["ATOMIC_NODE_CORNER_RADIUS"]),
            radius_y = float(theme["ATOMIC_NODE_CORNER_RADIUS"])
        )
        self.label = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        # Initialise text and connection points.
        self.update_text()
        self.create_connection_points()


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Setters override ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛

    # See the parent class for details.

    def set_node_type(self, node_type):
        validate_qname = is_qname(node_type)
        if validate_qname[0]:
            # Set namespace and prefix
            namespace_url = self.canvas_context.find_namespace(validate_qname[1])
            super().set_node_namespace(namespace_url)
            self.node_ns_prefix = validate_qname[1]

            # Set type
            super().set_node_type(validate_qname[2])

            # Other things which rely on the type being changed.
            self.__allocate_port_names()
            self.update_text()


    def set_node_name(self, node_name):
        if is_ncname(node_name)[0]:
            super().set_node_name(node_name)
            self.update_text()


    def set_node_property(self, prop_name, prop_value):
        """
        Detect whether a with-option is already connected, and raise a warning before passing onto the default implementation.
        """
        
        if len(self.with_options) > 0 and not str_none_or_empty(prop_value): 
            publisher.infobar_message(self, "Warning: This atomic step already has options set using with-option.", Gtk.MessageType.WARNING) #TRANSLATE#
        super().set_node_property(prop_name, prop_value)


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 

    def connect_input(self, connection):
        """
        Connect the nodes via the object manager.
        """

        # with-option and with-param are treated differently here.
        node_class = connection.from_node.node_class
        if node_class == "with-option" or node_class == "with-param":
            (self.with_options if node_class == "with-option" else self.with_params).append(connection)
            object_manager.connect_input(
                pool_id            = self.canvas_context.pool_id,
                to_object_handle   = self.object_handle,
                from_object_handle = connection.from_node.get_object_handle(),
                to_port            = "", # ┓ These two are irrelevant for with-option and with-param.
                from_port          = ""  # ┛
            )

            # Raise a warning if there's already an option set from the sidebar (https://www.w3.org/TR/xproc/#err.S0027)
            if len(self.get_node_properties()) > 0: publisher.infobar_message(self, "Warning: This atomic step already has options set in short form.", Gtk.MessageType.WARNING) #TRANSLATE#

            return

        # Get the first available port, then set the connection's label.
        port_name = self.__get_unallocated_input_port()
        connection.set_to_port_name(port_name)
        self.input_connections[connection] = port_name or ""

        # Interact with the object manager.
        from_port_name = connection.from_port
        if not str_none_or_empty(port_name) and not str_none_or_empty(from_port_name):
            object_manager.connect_input(
                pool_id            = self.canvas_context.pool_id,
                to_object_handle   = self.object_handle,
                to_port            = port_name,
                from_object_handle = connection.from_node.get_object_handle(),
                from_port          = from_port_name
            )


    def connect_output(self, connection):
        """
        Set the connection's output label.
        """

        # Get the primary (or any that's available) output port, and set the connection's label.
        port_name = self.__get_primary_output_port()
        connection.set_from_port_name(port_name)
        self.output_connections[connection] = port_name or ""


    def disconnect_input(self, connection):
        """
        Disconnect the nodes via the object manager.
        """

        # with-option and with-param are treated differently here.
        node_class = connection.from_node.node_class
        if node_class == "with-option" or node_class == "with-param":
            (self.with_options if node_class == "with-option" else self.with_params).remove(connection)
            object_manager.disconnect_input(
                pool_id            = self.canvas_context.pool_id,
                to_object_handle   = self.object_handle,
                from_object_handle = connection.from_node.get_object_handle(),
            )
            return

        object_manager.disconnect_input( # It might fail, but we don't care.
            pool_id            = self.canvas_context.pool_id,
            to_object_handle   = self.object_handle,
            to_port            = self.input_connections[connection],
            from_object_handle = connection.from_node.get_object_handle(),
            from_port          = connection.from_port
        )
        connection.set_to_port_name(None)
        super().disconnect_input(connection)


    def assign_input_port_name(self, connection, new_port_name):
        """
        Disconnect old input if it exists, and connect the new input.
        """

        if connection in self.input_connections.keys():
            # Disconnect existing connection.
            if not str_none_or_empty(self.input_connections[connection]): 
                disconnect_result = object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.input_connections[connection],
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = connection.from_port
                )
                if disconnect_result.exception is not None: raise disconnect_result.exception

            # Create new connection if the port names aren't empty.
            if (not str_none_or_empty(new_port_name)) and (not str_none_or_empty(connection.from_port)):
                connectable = False

                # Check for p:empty behaviour.
                if connection.from_node.node_class == "source" and connection.from_node.get_node_type() == "empty":
                    connectable = new_port_name not in self.input_connections.values() # New port already has a connection, so reject.
                else: 
                    connectable = True

                if connectable:
                    from_port_name = connection.from_port
                    connect_result = object_manager.connect_input(
                        pool_id            = self.canvas_context.pool_id,
                        to_object_handle   = self.object_handle,
                        to_port            = new_port_name,
                        from_object_handle = connection.from_node.get_object_handle(),
                        from_port          = from_port_name
                    )
                    if connect_result.exception is not None: raise connect_result.exception
                else: 
                    new_port_name = ""
                    publisher.infobar_message(self, "Error: Cannot set port name, as it conflicts with other connections.", Gtk.MessageType.ERROR) #TRANSLATE#

            super().assign_input_port_name(connection, new_port_name)


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛

    def __allocate_port_names(self):
        """
        Reloads the input and output port names from the specification, and allocates port names to existing connections if there are new names.        
        """

        # Remember old port names
        old_input_port_names  = [*self.input_port_names]
        old_output_port_names = [*self.output_port_names]

        # Find difference between old and new.
        self.set_port_names_from_spec()
        new_input_port_names  = list(set(self.input_port_names)  - set(old_input_port_names))
        new_output_port_names = list(set(self.output_port_names) - set(old_output_port_names))

        # Loop through the input connections, use new name from list if available.
        for connection in self.input_connections.keys():
            # Names are still available.
            if len(new_input_port_names) > 0:
                # Port name is not in the list.
                if self.input_connections[connection] not in self.input_port_names:
                    self.assign_input_port_name(connection, new_input_port_names.pop())

            # No new names are available.
            else:
                port_name = self.input_connections[connection]

                # An existing connection, but not in the list.
                if (not str_none_or_empty(port_name)) and (self.input_connections[connection] not in self.input_port_names):
                    object_manager.disconnect_input(
                        pool_id = self.canvas_context.pool_id,
                        to_object_handle   = self.object_handle,
                        to_port            = self.input_connections[connection],
                        from_object_handle = connection.from_node.get_object_handle(),
                        from_port          = connection.from_port
                    )
                    connection.set_to_port_name(None)
                    self.input_connections[connection] = None

        # Loop through the output connections, use first available name from list.
        for connection in self.output_connections.keys():
            if self.output_connections[connection] not in self.output_port_names:
                self.assign_output_port_name(connection, new_output_port_names[0] if len(new_output_port_names) > 0 else "")


    def __get_unallocated_input_port(self):
        """
        Look through the currently existing connections and its port names, and return one that's not being used.
        """

        allocated_ports = self.input_connections.values()
        for port_name in self.input_port_names:
            if port_name not in allocated_ports: return port_name
        return None

    
    def __get_primary_output_port(self):
        """
        Find some kind of port name to use for the output port.
        """

        return self.output_port_names[0] if len(self.output_port_names) > 0 else None



class GProcCanvasAggregateStep(GProcCanvasStep):
    """
    The base class for steps which have subpipelines in them.
    """
    
    def __init__(self, *args, aggregate_step_type, **kwargs):
        super().__init__(
            *args, 
            node_ns_prefix   = kwargs["canvas_context"].get_xproc_prefix(),
            container_width  = float(theme["ENTITY_NODE_WIDTH"]),
            container_height = float(theme["ENTITY_NODE_HEIGHT"]),
            **kwargs
        )

        self.aggregate_step_type = aggregate_step_type
        self.input_port_names.append("") # Pseudo-input

        # Set up UI elements
        chamfer = float(theme["AGGR_NODE_CHAMFER_WIDTH"])
        self.container = GooCanvas.CanvasPolyline(
            **self.container_properties,
            parent     = self,
            points     = Shapes.octagon(self.container_width, self.container_height, chamfer),
            close_path = True
        )
        self.subpipeline = GProcCanvasSubpipelineComponent(
            parent              = self,
            canvas_context      = self.canvas_context,
            object_handle       = self.object_handle,
            aggregate_step_type = aggregate_step_type,
            x                   = self.container_width - float(theme["SUBPIPELINE_EXPANDER_MARGIN"]),
            y                   = self.container_height/2 - 10
        )
        self.label = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        # Initialise text and connection points.
        self.update_text()
        self.create_connection_points()


    # ┏━━━━━━━━━━━━━━━━━━━┓
    # ┃                   ┃
    # ┃ Port name methods ┃
    # ┃                   ┃
    # ┗━━━━━━━━━━━━━━━━━━━┛

    def add_output_port(self, port_name):
        """
        Allows subpipelines to add ports names. 

        Note that this does not check for existing port names - this is fine. If 
        two different subpipelines add the same named port, removing it from one
        does not completely remove it from the list of output port names.
        """

        self.output_port_names.append(port_name)


    def remove_output_port(self, port_name):
        """
        Remove the port name and update connections.
        """

        # This one we do have the check since it's removal.
        if port_name in self.output_port_names: 
            self.output_port_names.remove(port_name)

            # Look through the output connections and update the connection's port name.
            relevant_connections = [c for (c, p) in self.output_connections.items() if p == port_name]
            for connection in relevant_connections:
                new_port_name = self.output_port_names[-1] if len(self.output_port_names) > 0 else None
                self.assign_output_port_name(connection, new_port_name)


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 
        
    def connect_input(self, connection):
        """
        Determines whether to add a pseudo-input or a normal input.
        """

        # Loop through the available input port names.
        for input_port_name in self.input_port_names:
            # Ignore the empty string, since it's for pseudo-inputs
            if input_port_name == "": continue

            # Port name found (and is unique), so set connection to this port
            if not input_port_name in self.input_connections.values():
                connection.set_to_port_name(input_port_name)
                self.input_connections[connection] = input_port_name
                if connection.from_port is not None:
                    object_manager.connect_input(
                        pool_id            = self.canvas_context.pool_id,
                        to_object_handle   = self.object_handle,
                        to_port            = input_port_name,
                        from_object_handle = connection.from_node.get_object_handle(),
                        from_port          = connection.from_port 
                    ) # Ignore error.
                return
                
        # Add pseudo-input (if it's not a source node), notify all subpipelines
        self.input_connections[connection] = ""
        if connection.from_node.node_class != "source":
            self.subpipeline.add_pseudo_input(connection.from_node.get_object_handle(), connection.from_port)


    def connect_output(self, connection):
        """
        Pick a port name, and if found the connection will be updated.
        """

        # Outputs available.
        if len(self.output_port_names) > 0:
            port_name = self.output_port_names[0]
            connection.set_from_port_name(port_name)
            self.output_connections[connection] = port_name

        # Outputs not available.
        else:
            self.output_connections[connection] = ""


    def disconnect_input(self, connection):
        """
        Determines the connection type (either actual or pseudo) and removes it from itself or the pipeline.
        """

        if connection in self.input_connections.keys():
            # Pseudo-input.
            if self.input_connections[connection] == "":
                self.subpipeline.remove_pseudo_input(connection.from_node.get_object_handle(), connection.from_port)

            # Actual input.
            else:
                disconnect_result = object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.input_connections[connection],
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = connection.from_port
                )
                if disconnect_result.exception is not None: raise disconnect_result.exception
    
            # Finally, delete from internal collection.
            del self.input_connections[connection]


    def assign_input_port_name(self, connection, new_port_name):
        """
        Changes the connection type based on what it's being changed from and to.
        """

        if connection in self.input_connections.keys():
            # Being changed to a port name.
            if not str_none_or_empty(new_port_name):
                connectable = False
                error = False
                
                # Check for p:empty behaviour.
                if connection.from_node.node_class == "source" and connection.from_node.get_node_type() == "empty":
                    connectable = new_port_name not in self.input_connections.values() # New port already has a connection, so reject.
                else: 
                    connectable = True

                if connectable:
                    # Remove pseudo-input.
                    if self.input_connections[connection] == "":
                        self.subpipeline.remove_pseudo_input(connection.from_node.get_object_handle(), connection.from_port)
                        self.input_connections[connection] = "__TEMP" # For satisfying the condition for re-adding pseudo-input on fail.

                    # Disconnect previous.
                    else:
                        object_manager.disconnect_input(
                            pool_id            = self.canvas_context.pool_id,
                            to_object_handle   = self.object_handle,
                            to_port            = self.input_connections[connection],
                            from_object_handle = connection.from_node.get_object_handle(),
                            from_port          = connection.from_port
                        ) # Ignore error.

                    # Notify the connection, and connect if it's coming from a valid output port of another node.
                    connection.set_to_port_name(new_port_name)
                    if connection.from_port is not None:
                        connect_result = object_manager.connect_input(
                            pool_id            = self.canvas_context.pool_id,
                            to_object_handle   = self.object_handle,
                            to_port            = new_port_name,
                            from_object_handle = connection.from_node.get_object_handle(),
                            from_port          = connection.from_port 
                        )
                        if connect_result.exception is not None:
                            # Failed due to some violation of representation constraints.
                            new_port_name = ""
                            self.input_connections[connection] = "__TEMP"
                            error = True

                else:
                    new_port_name = ""
                    self.input_connections[connection] = "__TEMP"
                    error = True

                if error:
                    publisher.infobar_message(self, "Error: Cannot set port name, as it conflicts with other connections.", Gtk.MessageType.ERROR) #TRANSLATE#

            # Being changed to a pseudo-input. Note the lack of 'elif' - this also runs if the above fails. 
            if (new_port_name == "") and (not str_none_or_empty(self.input_connections[connection])):
                # Disconnect previous and add new pseudo-input.
                object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.input_connections[connection],
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = connection.from_port
                ) # Ignore error.
                if connection.from_node.node_class != "source":
                    self.subpipeline.add_pseudo_input(connection.from_node.get_object_handle(), connection.from_port)
                connection.set_to_port_name(None)
            
            self.input_connections[connection] = new_port_name


    def notify_incoming_port_change(self, connection, old_port_name):
        """
        Overrides the GProcCanvasStep implementation by dealing with the pseudo-inputs first.
        """

        # Notify subpipeline canvases about port name change
        if (connection in self.input_connections.keys()) and (self.input_connections[connection] == ""):
            self.subpipeline.change_pseudo_input(connection.from_node.get_object_handle(), old_port_name, connection.from_port)
        else: super().notify_incoming_port_change(connection, old_port_name)


    def remove(self):
        """
        When the node is deleted, also delete everything in the subpipelines.        
        """

        self.subpipeline.remove_all_subpipelines()
        super().remove()



class GProcCanvasStepDeclaration(GProcCanvasAggregateStep):
    """
    The class which implements a step declaration on the canvas. 
    """

    def __init__(self, *args, node_type, **kwargs):
        # Validate the type.
        validate_ncname = is_ncname(node_type)
        if not validate_ncname[0]: raise GProcError("Expected a valid non-colonised name for node_type.")

        super().__init__(
            *args, 
            aggregate_step_type = AggregateStepType.DECLARATION, 
            node_class          = "declaration", 
            node_type           = validate_ncname[1],
            **kwargs
        )

        # Step declarations are always non-connectable.
        self.is_connectable = False



class GProcCanvasCompoundStep(GProcCanvasAggregateStep):
    """
    The class which implements a compound step on the canvas. 
    """

    def __init__(self, *args, node_type, **kwargs):
        # Validate the type.
        validate_ncname = is_ncname(node_type)
        if not validate_ncname[0]: raise GProcError("Expected a valid non-colonised name for node_type.")

        super().__init__(
            *args, 
            aggregate_step_type = AggregateStepType.COMPOUND, 
            node_class          = "compound", 
            node_type           = validate_ncname[1],
            **kwargs
        )



class GProcCanvasMulticontainerStep(GProcCanvasAggregateStep):
    """
    The class which implements a multicontainer step on the canvas. There are only two valid types, and each one acts slightly differently.
    """
    
    def __init__(self, *args, node_type, **kwargs):
        # Validate the type.
        agst_map = {
            "choose": AggregateStepType.CHOOSE,
               "try": AggregateStepType.TRY
        }
        if node_type not in agst_map.keys(): raise GProcError("Expected a valid multicontainer step type for node_type.")

        super().__init__(
            *args, 
            aggregate_step_type = agst_map[node_type], 
            node_class          = "multicontainer", 
            node_type           = node_type,
            **kwargs
        )

        # Add an input port for the main xpath-context if it's a p:choose. 
        if self.aggregate_step_type == AggregateStepType.CHOOSE:
            self.set_available_xpath_contexts() # xpath-context identifier -> object handle. 


    # ┏━━━━━━━━━━━━━━━━━━━┓
    # ┃                   ┃
    # ┃ Port name methods ┃
    # ┃                   ┃
    # ┗━━━━━━━━━━━━━━━━━━━┛

    def set_available_xpath_contexts(self):
        """
        Allows subpipelines to set the mapping of available xpath-contexts. Exclusive for p:when, where each case may have its own p:xpath-context input.
        """

        if self.aggregate_step_type == AggregateStepType.CHOOSE:
            case_handles   = [spl.object_handle for spl in self.subpipeline.subpipelines]
            default_handle = None if self.subpipeline.default is None else self.subpipeline.default.object_handle
            default_port_name = "xpath-context: {}"

            # Recreate the dictionary.
            self.xpath_context = {default_port_name.format("default"): self.object_handle}
            for idx, object_handle in enumerate(case_handles):
                self.xpath_context[default_port_name.format("case " + str(idx + 1))] = object_handle
            if default_handle is not None:
                self.xpath_context[default_port_name.format("otherwise")] = default_handle

            # Since the sidebar input ports update happens before new xpath-contexts are found, force refresh via the publisher.
            publisher.get_input_port_names(self)

    
    def remove_xpath_context(self, object_handle):
        """
        Removes the xpath-context (and its connection) associated with the given object handle.
        """

        # Reverse lookup the object handle.
        for xpcid, handle in self.xpath_context.items():
            if handle == object_handle:
                # Find a connection for the xpath-context, convert to pseudo-input if it exists.
                for connection in self.input_connections.keys():
                    if self.input_connections[connection] == xpcid:
                        self.assign_input_port_name(connection, "")
                        break
                break

        # Remove from xpath-context dictionary.
        del self.xpath_context[xpcid]

        # Change the labels on the connections to reflect the change in xpath-context IDs.
        reverse_xpath_context_dict = {item: key for (key, item) in self.xpath_context.items()} # Works because object handles are assumed unique.
        self.set_available_xpath_contexts()

        for xpcid, handle in self.xpath_context.items():
            if reverse_xpath_context_dict[handle] != xpcid:
                # old[ID1] == new[ID2], where ID1 and ID2 are different.
                # Look through the connections to see if any of them are ID1, change to ID2.
                for connection in self.input_connections.keys():
                    if self.input_connections[connection] == reverse_xpath_context_dict[handle]:
                        self.input_connections[connection] = xpcid
                        connection.set_to_port_name(xpcid)

        # Also update the tab names.
        publisher.update_tab_names(self)


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 

    def get_input_port_names(self):
        """
        Returns a list of input port names. This will be the default for p:try, but for p:choose it will return a list of xpath-contexts.
        """

        if self.aggregate_step_type == AggregateStepType.TRY: return super().get_input_port_names()
        return [""] + sorted([*self.xpath_context.keys()]) # Pseudo-input, and list of available xpath-contexts.


    def disconnect_input(self, connection):
        """
        Determines the connection type (either actual or pseudo) and removes it from itself or the pipeline. For p:choose, if it's not a pseudo-input, it will disconnect from the xpath-context.
        """

        # If this is a p:try or otherwise a pseudo-input, the logic is already in the parent.
        if (self.aggregate_step_type == AggregateStepType.TRY) or (self.input_connections[connection] == ""): 
            super().disconnect_input(connection)
            return
        
        if connection in self.input_connections.keys():
            old_entry = self.input_connections[connection]
            if old_entry in self.xpath_context.keys():
                # Remove the xpath-context from the appropriate object.
                disconnect_result = object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.xpath_context[old_entry],
                    to_port            = "xpath-context"
                )
                if disconnect_result.exception is not None: raise disconnect_result.exception
                del self.input_connections[connection]

            else: 
                super().disconnect_input(connection)
            

    def assign_input_port_name(self, connection, new_port_name):
        """
        Changes the connection type based on what it's being changed from and to. If it's a p:choose, it may be attempting to set to an xpath-context, so deal with that in here.
        """       
        
        # If this is a p:try or otherwise a pseudo-input, the logic is already in the parent.
        if (self.aggregate_step_type == AggregateStepType.TRY) or (new_port_name == ""): 
            super().assign_input_port_name(connection, new_port_name)
            return

        if connection in self.input_connections.keys():
            new_object_handle = self.xpath_context.get(new_port_name, None)

            # It's a new xpath-context!
            if (new_object_handle is not None) and (self.input_connections[connection] != new_port_name):
                # Remove previous input. pseudo or otherwise.
                if self.input_connections[connection] == "":
                    self.subpipeline.remove_pseudo_input(connection.from_node.get_object_handle(), connection.from_port)
                else:
                    old_entry         = self.input_connections[connection]
                    old_object_handle = self.xpath_context.get(old_entry, None) or self.object_handle
                    old_port_name     = "xpath-context" if old_entry in self.xpath_context.keys() else old_entry
                    object_manager.disconnect_input(
                        pool_id            = self.canvas_context.pool_id,
                        to_object_handle   = old_object_handle,
                        to_port            = old_port_name,
                        from_object_handle = connection.from_node.get_object_handle(),
                        from_port          = connection.from_port
                    ) # Ignore error

                # Remove previous xpath-context.
                object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = new_object_handle,
                    to_port            = "xpath-context"
                ) # Ignore error.

                # Add new, if it's possible.
                connect_result = object_manager.connect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = new_object_handle,
                    to_port            = "xpath-context",
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = connection.from_port
                ) 
                if connect_result.exception is not None: raise connect_result.exception
                
                # Set the object handle in self, and update the connection labels.
                self.input_connections[connection] = new_port_name
                connection.set_to_port_name(new_port_name)

            # Normal input.
            elif new_object_handle is None: 
                super().assign_input_port_name(connection, new_port_name)


    def notify_incoming_port_change(self, connection, old_port_name):
        """
        For a p:try this is exactly the same as the parent. For p:choose, we do a similar thing but with different object handles.
        """
        
        if connection in self.input_connections.keys():
            # Is p:try, the logic is already in the parent.       
            if (self.aggregate_step_type == AggregateStepType.TRY) or (self.input_connections[connection] == ""): 
                super().notify_incoming_port_change(connection, old_port_name)
                return

            if connection in self.input_connections.keys():
                object_handle = self.input_connections[connection]

                # Remove previous.
                object_manager.disconnect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = object_handle,
                    to_port            = "xpath-context"
                ) # Ignore error.

                # Add new.
                object_manager.connect_input(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = object_handle,
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = connection.from_port
                ) # Ignore error.

    