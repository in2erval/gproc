"""
This module contains the class for the import specification node.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.node import GProcCanvasNode
from gproc.publisher    import publisher
from gproc.shared       import theme
from gproc.utils.misc   import str_none_or_empty
from xproc              import object_manager



class GProcCanvasImport(GProcCanvasNode):
    """
    The class which implements an import specification node on the canvas.
    """
    
    def __init__(self, *args, href="", **kwargs):
        super().__init__(
            *args, 
            container_width  = float(theme["IMPORT_RADIUS"]),
            container_height = float(theme["IMPORT_RADIUS"]),
            **kwargs
        )

        # Class variables.
        self.href           = href
        self.node_class     = "import"
        self.node_ns_prefix = self.canvas_context.get_xproc_prefix() 
        self.object_handle  = self.canvas_context.object_handle

        if not str_none_or_empty(href):
            object_manager.set_import(
                pool_id       = self.canvas_context.pool_id,
                object_handle = self.object_handle,
                href          = href
            )

        # Set up UI elements.
        self.container = GooCanvas.CanvasRect(
            **self.container_properties,
            parent   = self,
            width    = self.container_width,
            height   = self.container_height,
            radius_x = 2,
            radius_y = 2
        )


    def __repr__(self):
        """
        String representation override.
        """

        return "import ({})".format(self.href)


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Getters & setters override ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

    # See the parent class for more details.

    def get_object_handle(self):
        return self.object_handle


    def get_node_type(self, ncname=True):
        return "import" if ncname else "{}:import".format(self.node_ns_prefix)


    def get_node_properties(self):
        return {"href": self.href}


    def get_specification(self):
        return object_manager.get_specification_from_name(
            pool_id  = self.canvas_context.pool_id,
            ent_type = "import"
        ).data


    def set_node_property(self, prop_name, prop_value):
        # Only allow 'href' property, and update via object manager.
        if prop_name == "href":
            object_manager.set_import(
                pool_id       = self.canvas_context.pool_id,
                object_handle = self.object_handle,
                href          = prop_value,
                old_href      = self.href
            )
            self.href = prop_value


    def remove(self):
        """
        Remove the import using the object manager.
        """
        
        object_manager.set_import(
            pool_id         = self.canvas_context.pool_id, 
            object_handle   = self.object_handle,
            href            = None,
            old_href        = self.href
        ) # Ignore error.

        super().remove()


