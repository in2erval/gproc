"""
This module contains the class for the source canvas node element.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.entity import GProcCanvasEntity
from gproc.publisher      import publisher
from gproc.shared         import theme
from gproc.utils.errors   import GProcError
from gproc.utils.misc     import is_ncname, is_qname
from gproc.utils.shapes   import Shapes



class GProcCanvasSource(GProcCanvasEntity):
    """
    The class which implements a source node on the canvas.
    """

    def __init__(self, *args, node_type, **kwargs):
        # Validate the type.
        validate_ncname = is_ncname(node_type)
        if not validate_ncname[0]: raise GProcError("Expected a valid non-colonised name for node_type.")

        super().__init__(
            *args, 
            node_class       = "source", 
            node_type        = validate_ncname[1],
            node_ns_prefix   = kwargs["canvas_context"].get_xproc_prefix(),
            container_width  = float(theme["SOURCE_WIDTH"]),
            container_height = float(theme["SOURCE_HEIGHT"]),
            **kwargs
        )

        # Set up UI elements.
        chamfer = float(theme["AGGR_NODE_CHAMFER_WIDTH"])
        self.container = GooCanvas.CanvasPolyline(
            **self.container_properties,
            parent     = self,
            points     = Shapes.right_trapezoid(self.container_width, self.container_height, chamfer),
            close_path = True
        )
        self.label = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        # Initialise text and connection points.
        self.update_text()
        self.create_connection_points()


    def __repr__(self):
        """
        String representation override. Only shows the type.        
        """
        
        return "<{}>".format(self.get_node_type(ncname=False))


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 

    def set_node_type(self, node_type):
        """
        Check for QName and only use the NCName part of it. Also performs special operations if it's being changed to p:empty.
        """

        validate_qname = is_qname(node_type)
        if validate_qname[0]:
            super().set_node_type(validate_qname[2])


    def connect_output(self, connection):
        """
        Set an invisible "port name" for sources.
        """

        connection.set_from_port_name("document")
        super().connect_output(connection)


    def remove(self):
        """
        Override for the node removal method.        
        """

        # When the node is going to be removed, notify the workspace to close the inline.
        publisher.close_inline(self, self.canvas_context.pool_id, self.object_handle)
        super().remove()