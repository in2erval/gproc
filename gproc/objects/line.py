"""
This module contains the connection line class.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, Gdk, GooCanvas, Pango

from gproc.objects.interpolate import generate_points 
from gproc.utils.enums         import Orientation
from gproc.utils.misc          import str_none_or_empty



class GProcCanvasLine(GooCanvas.CanvasPolyline):
    """
    The class which implements a connection line, which is a polyline going from one node to another, with optional labels at each ends. Note that this element (and the labels) will be a direct children of the canvas, instead of any nodes.

    See the docs for GooCanvas.CanvasPolyline: http://lazka.github.io/pgi-docs/GooCanvas-2.0/classes/CanvasPolyline.html
    """

    def __init__(self, *args, canvas, from_node, from_orientation, to_node, to_orientation, **kwargs):
        super().__init__(*args, end_arrow = True, **kwargs)

        # Class variables
        self.from_node        = from_node
        self.from_orientation = from_orientation
        self.to_node          = to_node
        self.to_orientation   = to_orientation

        # UI components and variables
        default_fill_color = Gdk.RGBA(1, 1, 1, 1) # white
        default_line_color = Gdk.RGBA(0, 0, 0, 1) # black
        default_font_color = Gdk.RGBA(0, 0, 0, 1) # black
        shade_properties = {
              "fill_color_gdk_rgba": default_fill_color,
            "stroke_color_gdk_rgba": default_line_color,
                            "width": 1,
                           "height": 1
        }
        text_properties = {
                      "alignment": Pango.Alignment.CENTER,
                      "font_desc": Pango.FontDescription("Arial 14px"),
            "fill_color_gdk_rgba": default_font_color
        }

        # We define a class-private class here.
        class GProcCanvasLineLabel(GooCanvas.CanvasGroup):
            """
            A simple CanvasGroup class for the port label.
            """

            def __init__(self, *args, **kwargs):
                super().__init__(
                    *args, 
                    parent         = canvas.get_root_item(), 
                    pointer_events = GooCanvas.CanvasPointerEvents.NONE,
                    visibility     = GooCanvas.CanvasItemVisibility.INVISIBLE,
                    **kwargs
                )
                self.text  = GooCanvas.CanvasText(**text_properties)
                self.shade = GooCanvas.CanvasRect(**shade_properties)
                self.add_child(self.shade, -1)
                self.add_child(self.text,  -1)

        # Create the labels.
        self.label_from = GProcCanvasLineLabel()
        self.label_to   = GProcCanvasLineLabel()
        self.update()
        self.lower(None) # Put to the bottom of canvas stack


    # ┏━━━━━━━━━━━━━┓
    # ┃             ┃
    # ┃   Setters   ┃
    # ┃             ┃
    # ┗━━━━━━━━━━━━━┛

    def set_from_port_name(self, port_name):
        """
        Sets the content of the origin label. If the value is None or empty, it will be hidden.
        """

        if str_none_or_empty(port_name):
            self.label_from.props.visibility = GooCanvas.CanvasItemVisibility.INVISIBLE
        else:
            self.label_from.props.visibility = GooCanvas.CanvasItemVisibility.VISIBLE
            self.__update_label(self.label_from, port_name)


    def set_to_port_name(self, port_name):
        """
        Sets the content of the destination label. If the value is None or empty, it will be hidden.
        """

        if str_none_or_empty(port_name):
            self.label_to.props.visibility = GooCanvas.CanvasItemVisibility.INVISIBLE
        else:
            self.label_to.props.visibility = GooCanvas.CanvasItemVisibility.VISIBLE
            self.__update_label(self.label_to, port_name)


    def set_orientation(self, from_orientation, to_orientation):
        """
        Sets the orientation on each node to connect to.
        """

        self.from_orientation = from_orientation
        self.to_orientation   = to_orientation


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛ 

    def update(self):
        self.props.points = generate_points(self.from_node, self.from_orientation, self.to_node, self.to_orientation)
        start_point = self.props.points.get_point(0)
        next_point  = self.props.points.get_point(1)
        prev_point  = self.props.points.get_point(self.props.points.num_points - 2)
        end_point   = self.props.points.get_point(self.props.points.num_points - 1)

        k = 0.8

        from_width, from_height = (self.label_from.shade.props.width, self.label_from.shade.props.height)
        to_width,   to_height   = (self.label_to.shade.props.width,   self.label_to.shade.props.height)
        self.label_from.props.x = next_point.x + k * (start_point.x - next_point.x) - from_width / 2
        self.label_from.props.y = next_point.y + k * (start_point.y - next_point.y) - from_height / 2

        # Adjustment for the second label to not obscure the arrow.
        x_gap = 20 + to_width / 2.1
        y_gap = 25
        x_adjustment = x_gap * (self.to_orientation.value - 2) if self.to_orientation.value % 2 else 0 # -1 for EAST, 1 for WEST
        y_adjustment = 0 if self.to_orientation.value % 2 else -y_gap * (self.to_orientation.value - 1) # -1 for NORTH, 1 for SOUTH
        self.label_to.props.x = end_point.x - to_width / 2 - x_adjustment
        self.label_to.props.y = end_point.y - to_height / 2 - y_adjustment

    
    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 

    def remove(self):
        """
        When the line is being removed, remove the two labels too.        
        """

        self.label_from.remove()
        self.label_to.remove()
        super().remove()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛

    def __update_label(self, label, text):
        """
        Sets the text on the given label, by calculating bounds and resizing the label.
        """

        # Change text.
        label.text.props.text = text

        # Calculate some values
        bounds = label.text.get_bounds()
        width  = (bounds.x2 - bounds.x1)
        height = (bounds.y2 - bounds.y1)
        margin_x = 5
        margin_y = 1

        # Size and positioning.
        label.shade.props.width  = width + (2 * margin_x)
        label.shade.props.height = height + (2 * margin_y)
        label.text.props.x       = margin_x
        label.text.props.y       = margin_y