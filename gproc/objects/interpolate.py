"""
This module ontains the method to create points for the connection line.
"""

import gi, os
gi.require_version("GooCanvas", "2.0")
from gi.repository import GooCanvas

from gproc.utils.enums import Orientation


def generate_points(from_node, from_orientation, to_node, to_orientation, deviation=10):
    """
    Generates a set of points to use for the connection line, based on where the nodes and what their connection orientations are.
    """

    # First, get the positions of the nodes.
    from_bounds = from_node.get_node_bounds()
    to_bounds   = to_node.get_node_bounds()

    # Generate a dictionary of specific points, and determine the start and end points.
    from_nesw   = __nesw(from_bounds)
    to_nesw     = __nesw(to_bounds)
    start_point = from_nesw[from_orientation]
    end_point   = to_nesw[to_orientation]

    # There will be 4 scenarios: the line goes horizontally or vertically, from the start and to the end (therefore 2 * 2).
    start_direction = "horizontal" if from_orientation.value % 2 else "vertical"
    end_direction   = "horizontal" if to_orientation.value   % 2 else "vertical"

    # For each scenario, find additional points and make the points struct.
    if   start_direction == "horizontal" and end_direction == "horizontal":
        x_diff    = end_point[0] - start_point[0]
        start_mid = (start_point[0] + x_diff / 2, start_point[1])
        end_mid   = (start_point[0] + x_diff / 2, end_point[1]) 
        return __make_points([start_point, start_mid, end_mid, end_point])

    elif start_direction == "vertical"   and end_direction == "vertical":
        y_diff    = end_point[1] - start_point[1]
        start_mid = (start_point[0], start_point[1] + y_diff / 2)
        end_mid   = (end_point[0],   start_point[1] + y_diff / 2) 
        return __make_points([start_point, start_mid, end_mid, end_point])

    else:
        bend_x = end_point[0] if start_direction == "horizontal" else start_point[0]
        bend_y = end_point[1] if start_direction == "vertical"   else start_point[1]
        return __make_points([start_point, (bend_x, bend_y), end_point])
        
    # None found, this should never happen...
    return __make_points([start_point, end_point])


def __make_points(points):
    """
    Creates a GooCanvas.CanvasPoints struct from the given set of points.
    """

    canvas_points = GooCanvas.CanvasPoints.new(len(points))
    for index, point in enumerate(points): 
        canvas_points.set_point(index, point[0], point[1])
    return canvas_points


def __nesw(bounds):
    """
    Creates the north, east, south, and west points from the rectangular bounds.
    """

    return {
        Orientation.NORTH: ((bounds.x2 + bounds.x1) / 2, bounds.y1),
        Orientation.EAST:  (bounds.x2, (bounds.y2 + bounds.y1) / 2),
        Orientation.SOUTH: ((bounds.x2 + bounds.x1) / 2, bounds.y2),
        Orientation.WEST:  (bounds.x1, (bounds.y2 + bounds.y1) / 2)
    }
