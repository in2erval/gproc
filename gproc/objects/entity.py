"""
This module contains the base class for all entity-based canvas node elements.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.node import GProcCanvasNode
from gproc.utils.misc   import print_exception, str_none_or_empty
from xproc              import object_manager



class GProcCanvasEntity(GProcCanvasNode):
    """
    The base class for all entity-based nodes. Characterised by interacting with the object manager for creating object handles and getting values.
    """

    def __init__(self, *args, node_class, object_handle, node_type=None, node_name=None, node_ns_prefix=None, **kwargs):
        super().__init__(*args, **kwargs)

        # Class variables.
        self.node_class     = node_class
        self.node_ns_prefix = node_ns_prefix

        # Try to find the namespace from the given prefix.
        node_ns = self.canvas_context.find_namespace(node_ns_prefix)

        # Initialise new object handle if not given.
        self.object_handle = object_handle
        if object_handle is None:
            create_object_result = object_manager.create_object(
                pool_id       = self.canvas_context.pool_id, 
                ent_class     = node_class,
                ent_type      = node_type,
                ent_name      = node_name,
                ent_ns        = node_ns,
                parent_handle = self.canvas_context.object_handle
            )
            if create_object_result.exception is not None: raise create_object_result.exception # Something went wrong.
            self.object_handle = create_object_result.data
            
        # Get specification and populate input port names.
        self.set_port_names_from_spec()


    def __repr__(self):
        """
        String representation override.
        """

        name_str   = self.get_node_name() or "" # If None, use empty string.
        name_str  += "\n" if name_str != "" else ""
        type_str   = "<{}>".format(self.get_node_type(ncname=False))
        return name_str + type_str


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Getters & setters override ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

    # See the parent class for details.

    def get_object_handle(self):
        return self.object_handle


    def get_node_type(self, ncname=True):
        prefix = "" if str_none_or_empty(self.node_ns_prefix) else (self.node_ns_prefix + ":")
        return (("" if ncname else prefix) + object_manager.get_type(self.canvas_context.pool_id, self.object_handle).data) or ""


    def get_node_name(self):
        return object_manager.get_name(self.canvas_context.pool_id, self.object_handle).data or ""


    def get_node_documentation(self):
        return object_manager.get_documentation(self.canvas_context.pool_id, self.object_handle).data or ""


    def get_node_properties(self):
        return object_manager.get_properties(self.canvas_context.pool_id, self.object_handle).data or {}


    def get_node_additionals(self):
        return object_manager.get_additionals(self.canvas_context.pool_id, self.object_handle).data or {}


    def set_node_type(self, node_type):
        set_type_result = object_manager.set_type(self.canvas_context.pool_id, self.object_handle, node_type)
        #if set_type_result.exception is not None: print_exception(set_type_result.exception)
        self.update_text()


    def set_node_name(self, node_name):
        set_name_result = object_manager.set_name(self.canvas_context.pool_id, self.object_handle, node_name)
        #if set_name_result.exception is not None: print_exception(set_name_result.exception)
        self.update_text()

    
    def set_node_namespace(self, node_namespace):
        set_namespace_result = object_manager.set_namespace(self.canvas_context.pool_id, self.object_handle, node_namespace)
        #if set_namespace_result.exception is not None: print_exception(set_namespace_result.exception)


    def set_node_documentation(self, node_documentation):
        set_documentation_result = object_manager.set_documentation(self.canvas_context.pool_id, self.object_handle, node_documentation)
        #if set_documentation_result.exception is not None: print_exception(set_documentation_result.exception)


    def set_node_property(self, prop_name, prop_value):
        set_property_result = object_manager.set_property(self.canvas_context.pool_id, self.object_handle, prop_name, prop_value)
        #if set_property_result.exception is not None: print_exception(set_property_result.exception)


    def set_node_additional(self, addi_name, addi_value):
        set_additional_result = object_manager.set_additional(self.canvas_context.pool_id, self.object_handle, addi_name, addi_value)
        #if set_additional_result.exception is not None: print_exception(set_additional_result.exception)
    
     
    # ┏━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                        ┃
    # ┃ Other override methods ┃
    # ┃                        ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━┛ 

    def remove(self):
        """
        Remove the object in the in-memory representation when this node is about to be deleted.        
        """

        # Call the method on the object manager, print if there's an error.
        remove_object_result = object_manager.remove_object(
            pool_id       = self.canvas_context.pool_id,
            object_handle = self.object_handle
        )
        if remove_object_result.exception is not None: print_exception(remove_object_result.exception)

        super().remove()


    