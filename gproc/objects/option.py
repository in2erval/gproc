"""
This module contains the class for the option specification node.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.node import GProcCanvasNode
from gproc.publisher    import publisher
from gproc.shared       import theme
from gproc.utils.misc   import str_none_or_empty
from xproc              import object_manager



class GProcCanvasOption(GProcCanvasNode):
    """
    The class which implements an option specification node on the canvas.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(
            *args, 
            container_width  = float(theme["OPTION_RADIUS"]),
            container_height = float(theme["OPTION_RADIUS"]),
            **kwargs
        )

        # Class variables.
        self.options        = {}
        self.node_class     = "option"
        self.node_ns_prefix = self.canvas_context.get_xproc_prefix() 
        self.object_handle  = self.canvas_context.object_handle

        self.option_name  = self.__get_unique_name()
        self.canvas_context.option_names.append(self.option_name)

        object_manager.set_step_specification(
            pool_id         = self.canvas_context.pool_id, 
            object_handle   = self.object_handle,
            spec_type       = "option",
            name            = self.option_name,
            spec_properties = self.options
        )

        # Set up UI elements.
        self.container = GooCanvas.CanvasRect(
            **self.container_properties,
            parent   = self,
            width    = self.container_width,
            height   = self.container_height,
            radius_x = float(theme["OPTION_RADIUS"]) / 2,
            radius_y = float(theme["OPTION_RADIUS"]) / 2
        )


    def __repr__(self):
        """
        String representation override.
        """

        return self.option_name


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Getters & setters override ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

    # See the parent class for more details.

    def get_object_handle(self):
        return self.object_handle


    def get_node_type(self, ncname=True):
        return "option" if ncname else "{}:option".format(self.canvas_context.get_xproc_prefix())


    def get_node_name(self):
        return self.option_name


    def get_node_properties(self):
        return {**self.options}


    def get_specification(self):
        return object_manager.get_specification_from_name(
            pool_id  = self.canvas_context.pool_id,
            ent_type = "option"
        ).data


    def set_node_name(self, new_name):
        # Make sure the name is unique, and update the input spec.
        unique = new_name not in self.canvas_context.option_names
        if unique and (not str_none_or_empty(new_name)):
            # Remove the old name from the canvas context list.
            old_name = self.option_name
            self.canvas_context.option_names.remove(old_name)

            # Add the new name, and notify all incoming connections.
            self.canvas_context.option_names.append(new_name)
            self.option_name = new_name

            # Remove the option spec and re-add as new name.
            self.__update_option_spec(old_name)


    def set_node_property(self, prop_name, prop_value):
        # Validate the name and value.
        valid = True
        if prop_name == "required" and prop_value.lower() not in ["true", "false", ""]: valid = False

        # Set property only after successful validation.
        if valid: 
            self.options[prop_name] = prop_value
            self.__update_option_spec(self.option_name)
        else:
            publisher.infobar_message(self, "Error: Invalid property value.", Gtk.MessageType.ERROR)


    def remove(self):
        """
        Remove the option name from the canvas context list. Also removes from the specs.
        """

        # Remove info from parent.
        if self.option_name in self.canvas_context.option_names:
            self.canvas_context.option_names.remove(self.option_name)

        # Remove spec.
        object_manager.set_step_specification(
            pool_id         = self.canvas_context.pool_id, 
            object_handle   = self.object_handle,
            spec_type       = "option",
            name            = self.option_name,
            spec_properties = None
        ) # Ignore error.

        super().remove()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __get_unique_name(self):
        """
        Determine whether the given default name is available to use, and if not create a new name which is available.
        """

        default = "new-option"

        counter = 0
        template = default
        while template in self.canvas_context.option_names:
            counter += 1
            template = default + ("_" + str(counter))
        return template

    
    def __update_option_spec(self, old_name):
        """
        Remove the old option spec and replace with a new one.
        """

        # Only remove if the old name is different from current one.
        if old_name != self.option_name:
            object_manager.set_step_specification(
                pool_id         = self.canvas_context.pool_id,
                object_handle   = self.object_handle,
                spec_type       = "option",
                name            = old_name,
                spec_properties = None
            )

        object_manager.set_step_specification(
            pool_id         = self.canvas_context.pool_id,
            object_handle   = self.object_handle,
            spec_type       = "option",
            name            = self.option_name,
            spec_properties = self.options
        )


