"""
This module contains the canvas for inserting and modifying XProc elements in the pipeline.
"""

import gi, os
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, Gdk, GdkPixbuf, GooCanvas

from gproc.objects.connections import GProcConnections
from gproc.objects.import_     import GProcCanvasImport
from gproc.objects.io          import GProcCanvasInput, GProcCanvasOutput 
from gproc.objects.node        import GProcCanvasNode
from gproc.objects.steps       import GProcCanvasAtomicStep, GProcCanvasCompoundStep, GProcCanvasMulticontainerStep, GProcCanvasStepDeclaration
from gproc.objects.source      import GProcCanvasSource
from gproc.objects.option      import GProcCanvasOption
from gproc.objects.opv         import GProcCanvasVariable, GProcCanvasWithOption, GProcCanvasWithParam
from gproc.publisher           import publisher
from gproc.shared              import data, theme
from gproc.utils.misc          import parse_rgba, pointer_on_hover
from xproc                     import object_manager



class GProcCanvas(GooCanvas.Canvas):
    """
    A derived class which implements the canvas for authoring pipelines. The base class creates a way to interact with a 2D space, and this class abstracts may of the node and connection creation methods.
    
    The canvas context should be created before the canvas, which will determine the available names in the scope and other values.

    See the docs for GooCanvas.Canvas: http://lazka.github.io/pgi-docs/GooCanvas-2.0/classes/Canvas.html
    """
    
    def __init__(self, *args, canvas_context, **kwargs):
        super().__init__(
            *args, 
            automatic_bounds          = True, 
            has_tooltip               = True, 
            background_color_gdk_rgba = parse_rgba(theme["CANVAS_BACKGROUND_COLOUR"]),
            **kwargs
        )
        canvas_context.register_canvas(self) # Update the reference in the context.

        # Class constants.
        self.SCALES        = [0.3, 0.6, 0.8, 1, 1.5]
        self.CANVAS_BOUNDS = {'x': 1, 'y': 1}
        self.CTRL_KEYVAL   = 65507

        # Class variables.
        self.canvas_context = canvas_context
        self.selected_node  = None
        self.dragging_node  = None
        self.connections    = GProcConnections(self)

        self.current_scale  = 3 # Index into self.SCALES.
        self.held_key       = None
        self.prev_mouse_pos = None

        # UI components and variables.
        self.parent_navigation = None if canvas_context.parent_canvas is None else self.__create_parent_navigation()
        self.input_nodes       = []
        self.canvas_bounds_pin = GooCanvas.CanvasRect( # An invisible object to control the canvas bounds.
            parent       = self.get_root_item(),
            width        = 1, 
            height       = 1,
            fill_color   = "transparent",
            stroke_color = "transparent"
        )

        # Get the specifications and set up default inputs and outputs.
        if canvas_context.parent_node is not None:
            node_type   = canvas_context.parent_node.get_node_type()
            spec_result = object_manager.get_specification_from_name(canvas_context.pool_id, node_type)            
            if spec_result.exception is None:
                spec = spec_result.data
                self.__setup_io(spec.inputs, spec.outputs)

        # Connect GTK and publisher signals.
        self.connect("button_press_event",   self.__event_mouse_down)
        self.connect("button_release_event", self.__event_mouse_up)
        self.connect("motion_notify_event",  self.__event_mouse_move)
        self.connect("scroll-event",         self.__event_mouse_scroll) 
        publisher.connect("set_held_key",    self.__event_held_key_change)
        publisher.connect("update_bounds",   self.__event_bound_update)

        # Other initialisations.
        self.set_scale(self.SCALES[self.current_scale])


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                        ┃
    # ┃ Initialisation Methods ┃
    # ┃                        ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━┛

    def __create_parent_navigation(self):
        """
        Creates the parent navigation button on the top left. The user can use this to navigate upwards in the subpipeline hierarchy.
        """

        # Base values.
        width  = 80
        height = 80
        margin = 4

        # UI elements.
        button = GooCanvas.CanvasGroup(
            parent = self.get_root_item(),
            x      = -(width/2) + 6,
            y      = -(height/2) + 6
        )
        shape  = GooCanvas.CanvasRect(
            parent              = button,
            fill_color_gdk_rgba = parse_rgba(theme["CANVAS_BACKGROUND_COLOUR"]),
            width               = width,
            height              = height,
            line_width          = 1
            # (Quarter-)Circular? Then uncomment these two:
            #radius_x            = width,
            #radius_y            = height
        )
        image  = GooCanvas.CanvasImage(
            parent       = button,
            scale_to_fit = True,
            pixbuf       = GdkPixbuf.Pixbuf.new_from_file_at_scale(
                filename = os.path.join(data.images_dir, "icon_navigate_parent.svg"),
                width    = width/2,
                height   = -1,
                preserve_aspect_ratio = True
            ),
            width  = width/3,
            height = height/3,
            x      = (width/2) + margin,
            y      = (height/2) + margin
        )
        pointer_on_hover(button)
        button.connect("button-release-event", self.__event_parent_navigation_click)
        return button


    def __setup_io(self, inputs, outputs):
        """
        Create input and output nodes based on the given specifications.
        """

        for input_name, input_spec in inputs.items():
            # Create default input. Cannot be edited or deleted.
            input_node = GProcCanvasInput(
                parent         = self.get_root_item(), 
                canvas_context = self.canvas_context,
                is_default     = True,
                input_name     = input_name,
                object_handle  = self.canvas_context.object_handle,
                options        = {
                    "sequence": str(input_spec.sequence),
                     "primary": str(input_spec.primary),
                        "kind": input_spec.kind,
                      "select": input_spec.select
                }
            )
            input_node.is_deleteable = False
            input_node.is_editable   = False
            self.input_nodes.append(input_node)
            self.__align_input_nodes()

        for output_name, output_spec in outputs.items():
            # Create default output. Cannot be edited or deleted.
            output_node = GProcCanvasOutput(
                parent         = self.get_root_item(),
                canvas_context = self.canvas_context,
                is_default     = True,
                output_name    = output_name,
                object_handle  = self.canvas_context.object_handle,
                options        = {
                    "sequence": str(output_spec.sequence),
                     "primary": str(output_spec.primary)
                },
                # Default position:
                x = 165,
                y = 400
            )
            output_node.is_deleteable = False
            output_node.is_editable   = False


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_parent_navigation_click(self, ci, ti, e):
        """
        Open a new tab in the workspace by notifying via the publisher.
        """

        publisher.open_canvas(self, self.canvas_context.parent_canvas)


    def __event_mouse_down(self, canvas, event):
        """
        Find the item to select, and potentially connect nodes.
        """

        # Disable automatic resizing of canvas until mouse up.
        self.props.automatic_bounds = False

        # Find item and keep track of previous item.
        cursor_x, cursor_y = self.convert_from_pixels(event.x, event.y)
        previous_item      = self.selected_node
        new_item           = self.__find_gproc_node(self.get_item_at(cursor_x, cursor_y, True))

        # Connect (if appropriate) and select.
        if new_item is not None: new_item.find_connection_point(cursor_x, cursor_y) # Has to happen before connecting
        self.__connect_nodes(previous_item, new_item)
        self.__set_current_node(previous_item, new_item)


    def __event_mouse_up(self, canvas, event):
        """
        Reset variables.
        """

        self.props.automatic_bounds = True
        self.__move_pin()
        self.scroll_to(0,0) # TODO: Keep track of where the click happened so that it doesn't snap to (0,0)?
        self.dragging_node = None
        self.prev_mouse_pos = None


    def __event_mouse_move(self, canvas, event):
        """
        Keeps track of mouse movements and calls a method to move the item currently being dragged.
        """

        # If the dragging action is being recorded, move either the item or the canvas.
        cursor_x, cursor_y  = self.convert_from_pixels(event.x, event.y)
        if self.prev_mouse_pos:
            if self.dragging_node and self.dragging_node.is_moveable:
                self.__move_item(self.dragging_node, cursor_x, cursor_y)
            else:
                if Gdk.ModifierType.BUTTON1_MASK & int(event.state):
                    self.__move_canvas(cursor_x, cursor_y)
        self.prev_mouse_pos = (cursor_x, cursor_y)


    def __event_mouse_scroll(self, canvas, event):
        """
        If the user is holding the control key, set canvas scale to simulate zooming in and out.
        """

        # Scroll down the canvas, or zoom out.
        if self.held_key == self.CTRL_KEYVAL:
            if event.direction == Gdk.ScrollDirection.DOWN:
                self.current_scale = 0 if self.current_scale <= 0 else self.current_scale - 1
            elif event.direction == Gdk.ScrollDirection.UP:
                self.current_scale = (len(self.SCALES) - 1) if self.current_scale >= (len(self.SCALES) - 1) else self.current_scale + 1
            self.set_scale(self.SCALES[self.current_scale])

            self.__move_pin()
            self.scroll_to(0,0)


    def __event_held_key_change(self, caller, keyval):
        """
        Keep track of current held key. Invoked by publisher signal.
        """

        self.held_key = keyval


    def __event_bound_update(self, caller, _):
        """
        Update the canvas bounds. Invoked by publisher signal.
        """

        self.update_bounds()


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing Methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛

    def add_gproc_node(self, node_class, node_type=None, object_handle=None):
        """
        Creates a new node in the canvas based on the class and type. If the object handle is given, it will be passed to the newly created node.
        """
        
        # Common to any creates nodes.
        node_args = {
            "canvas_context": self.canvas_context,
                    "parent": self.get_root_item(),
                         "x": 100,
                         "y": 100
        }

        # Specific to entity nodes.
        entity_args = {
            "object_handle": object_handle,
                "node_type": node_type
        }
        entity_map = {
                    "atomic": GProcCanvasAtomicStep,
               "declaration": GProcCanvasStepDeclaration,
                  "compound": GProcCanvasCompoundStep,
            "multicontainer": GProcCanvasMulticontainerStep,
                    "source": GProcCanvasSource,
                  "variable": GProcCanvasVariable,
               "with-option": GProcCanvasWithOption,
                "with-param": GProcCanvasWithParam
        }

        # Deal with other node classes first.        
        if node_class == "input":
            if object_handle is None: object_handle = self.canvas_context.object_handle
            new_node = GProcCanvasInput(
                **node_args,
                is_pseudo     = False,
                object_handle = object_handle
            )
            self.input_nodes.append(new_node)
            self.__align_input_nodes()

        elif node_class == "output":
            if object_handle is None: object_handle = self.canvas_context.object_handle
            new_node = GProcCanvasOutput(
                **node_args,
                object_handle = object_handle
            )

        elif node_class == "option":
            new_node = GProcCanvasOption(
                **node_args
            )

        elif node_class == "import":
            new_node = GProcCanvasImport(
                **node_args
            )
        
        # Create entities which are in the mapping.
        elif node_class in entity_map.keys():
            if node_class == "atomic": # Amend the node_type with proper prefix if it's atomic
                entity_args["node_type"] = "{}:{}".format(self.canvas_context.get_xproc_prefix(), node_type)
            return entity_map[node_class](**node_args, **entity_args)
        
        return None


    def add_pseudo_input(self, object_handle, port_name):
        """
        Creates a new input node for the given pseudo-input, unless it already exists.
        """

        # Check if it already exists.
        existing_pseudo_input = [c for c in self.input_nodes if (c.input_name == port_name) and (c.object_handle == object_handle)]
        if len(existing_pseudo_input) > 0: return

        # Create new input node.
        input_node = GProcCanvasInput(
            parent         = self.get_root_item(),
            canvas_context = self.canvas_context,
            input_name     = port_name,
            is_pseudo      = True,
            object_handle  = object_handle,
        )
        input_node.is_deleteable = False
        input_node.is_editable   = False
        self.input_nodes.append(input_node)
        self.__align_input_nodes()


    def remove_pseudo_input(self, object_handle, port_name):
        """
        Finds a pseudo-input with the given handle and port name and disconnects + removes it.
        """

        for node in self.input_nodes:
            if (node.object_handle == object_handle) and (node.input_name == port_name):
                self.connections.remove_all_connections(node)
                node.remove()
                self.input_nodes.remove(node)
                self.__align_input_nodes() # Update the alignment of input nodes after removal.
                break

        
    def change_pseudo_input(self, object_handle, old_port_name, new_port_name):
        """
        Finds a pseudo-input with the given handle and port name and changes the port. Also updates the label.
        """

        for node in self.input_nodes:
            if (node.object_handle == object_handle) and (node.input_name == old_port_name):
                node.input_name = new_port_name
                node.update_text()
                for connection in self.connections.find_all_connections(node): # Also update all connections related to the pseudo-input.
                    connection.set_from_port_name(new_port_name)
                break


    def delete_current_node(self):
        """
        Removes the current node from the canvas.
        """

        # Only delete nodes which are deleteable.
        if (self.selected_node is not None) and (self.selected_node.is_deleteable):
            # Remove connections first, then the node itself.
            self.connections.remove_all_connections(self.selected_node) 
            self.selected_node.remove()

            # Also remove from list if it was an input node.
            if isinstance(self.selected_node, GProcCanvasInput): 
                self.input_nodes.remove(self.selected_node)
                self.__align_input_nodes()

            # Reset variables.
            self.selected_node = None
            self.dragging_node = None        

        self.unselect_current_node()


    def delete_all_nodes(self):
        """
        Removes all elements from the canvas. Invoked when the subpipeline is deleted from somewhere above the hierarchy.
        """

        # Iterate over the children of the canvas root item.
        for _ in range(0, self.get_root_item().get_n_children()):
            item = self.get_root_item().get_child(0)
            if item is not None: item.remove()

        # Also notify the workspace to close the canvas tab. 
        publisher.close_canvas(self, self)


    def unselect_current_node(self):
        """
        Unselects the current node and notifies other components via the publisher. Invoked by the workspace tabs.        
        """

        # If a node is selected, then we want to remove the highlighting.
        if self.selected_node is not None: 
            self.selected_node.toggle_highlight(False)
            self.selected_node = None
            
        # Notify publisher about node change        
        publisher.set_current_node(self, self.selected_node)


    def update_bounds(self):
        """
        Exposes the pin moving method to other components.        
        """

        self.__move_pin()
        self.props.automatic_bounds = True

    
    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛

    def __align_input_nodes(self):
        """
        Position the current set of input nodes at the top.        
        """

        width = float(theme["INPUT_WIDTH"])
        for idx, node in enumerate(self.input_nodes):
            node.props.x  = (width + 5) * (idx + 1) 
            node.props.y  = -5 #  Hide the top border.
            for connection in self.connections.find_all_connections(node): connection.update() # Make sure the lines move to the right places


    def __find_gproc_node(self, item):
        """
        Climbs up the hierarchy from the exact clicked item to find the actual node item.
        """

        # When objects are obtained from get_item_at() method on the canvas, it should find the GProcCanvasNode that it is inside.
        while (not isinstance(item, GProcCanvas)) and (item is not None):
            if isinstance(item, GProcCanvasNode) and item.is_selectable: return item
            item = item.get_parent()
        return None


    def __set_current_node(self, previous_item, new_item):
        """
        Updates highlighting for the selected and previously selected item, then notifies selection change via publisher.
        """

        # Update variable
        self.selected_node = new_item

        # Reset previous item to normal state.
        if previous_item is not None:
            previous_item.toggle_highlight(False)
            if new_item is not previous_item: previous_item.clear_orientation()

        # Highlights the current item, and update variable if it can be dragged.
        if new_item is not None:
            if new_item.is_moveable: self.dragging_node = new_item
            new_item.toggle_highlight(True)

        # Item was clicked twice.
        if previous_item is not None and new_item is not None:
            if previous_item is new_item: previous_item.activate()

        # Notify the configuration pane via publisher.
        publisher.set_current_node(self, new_item)


    def __connect_nodes(self, previous_item, new_item):
        """
        Attempts to connect the two nodes by passing it into GProcConnections.
        """

        # Null check.
        if (previous_item is not None) and (new_item is not None):
            # Orientation required.
            if previous_item.connection_orientation is not None and new_item.connection_orientation is not None:
                # self.connections.connect returns a boolean.
                connect_success = self.connections.connect(previous_item, previous_item.connection_orientation, new_item, new_item.connection_orientation)
                
                # New connection made.
                if connect_success:
                    previous_item.clear_orientation()
                    new_item.clear_orientation()

                # Connection already exists.
                if connect_success is None:
                    self.connections.remove_connection(previous_item, new_item)

    
    def __move_item(self, item, cursor_x, cursor_y):
        """
        Translates the canvas item based on difference between the current cursor position and the previous cursor position.
        """

        # Initialise variables to use for calculation.
        bounds              = item.get_bounds()
        item_left, item_top = bounds.x1, bounds.y1
        diff_x, diff_y      = cursor_x - self.prev_mouse_pos[0], cursor_y - self.prev_mouse_pos[1]

        # Check if the difference takes the item out of bounds.
        if item_left + diff_x <= 0: diff_x = -item_left
        if item_top  + diff_y <= 0: diff_y = -item_top
        
        # Translate, while making sure not to translate if the item is already on the left or top edge.
        translate_x = 0 if (item_left <= self.CANVAS_BOUNDS['x']) and (diff_x <= 0) else diff_x
        translate_y = 0 if (item_top  <= self.CANVAS_BOUNDS['y']) and (diff_y <= 0) else diff_y
        item.translate(translate_x, translate_y)
        
        # Also move the connection arrows
        item_connections = self.connections.find_all_connections(item)
        for connection in item_connections: connection.update()


    def __move_canvas(self, cursor_x, cursor_y):
        """
        Pans the canvas based on cursor movement.
        """

        # TODO: Implement canvas panning.
        pass


    def __move_pin(self):
        """
        Moves the invisible pin which controls the canvas bounds.        
        """

        # Get the allocated size of the tab page.
        parent = self.get_parent()
        if parent is not None and parent.get_parent() is not None:
            widget     = parent.get_parent()
            size       = widget.get_allocated_size().allocation

            # TODO: The exact calculation is... finicky. I think it works? But it's good to do more tests.
            multiplier = 1 / self.SCALES[self.current_scale] 
            self.canvas_bounds_pin.props.x = size.width  * multiplier - 3
            self.canvas_bounds_pin.props.y = size.height * multiplier - 3
    