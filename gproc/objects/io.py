"""
This module contains classes for the input and output nodes.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.node   import GProcCanvasNode
from gproc.publisher      import publisher
from gproc.shared         import theme
from gproc.utils.enums    import Orientation
from gproc.utils.misc     import str_none_or_empty
from gproc.utils.shapes   import Shapes
from xproc                import object_manager



class GProcCanvasInput(GProcCanvasNode):
    """
    The class which implements an input node on the canvas.
    """

    def __init__(self, *args, object_handle, input_name="", is_pseudo=False, is_default=False, options=None, **kwargs):
        super().__init__(
            *args, 
            container_width  = float(theme["INPUT_WIDTH"]),
            container_height = float(theme["INPUT_HEIGHT"]),
            **kwargs
        )

        # Class variables.
        self.object_handle  = object_handle # Object handles may be passed in as part of a pseudo-input.
        self.options        = options or {}
        self.node_class     = "input"
        self.node_ns_prefix = self.canvas_context.get_xproc_prefix() 

        self.is_moveable = False # Input nodes are always anchored at the top.
        self.is_pseudo   = is_pseudo
        self.is_default  = is_default
        self.input_name  = self.__get_unique_name(input_name)

        if not self.is_pseudo: 
            self.canvas_context.input_names.append(self.input_name)
            if not self.is_default: # No point adding another spec for default inputs.
                object_manager.set_step_specification(
                    pool_id         = self.canvas_context.pool_id, 
                    object_handle   = self.object_handle,
                    spec_type       = "input",
                    name            = self.input_name,
                    spec_properties = self.options
                )

        # Set up UI elements.
        self.container = GooCanvas.CanvasRect(
            **self.container_properties,
            parent   = self,
            width    = self.container_width,
            height   = self.container_height + 10
        )
        self.label = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        # Initialise text and connection points - an input can only have connection points at the bottom.
        self.update_text()
        self.create_connection_points(allowed_orientations=[Orientation.SOUTH])


    def __repr__(self):
        """
        String representation override, provides two different ways of representing, differentiated by whether the input is pseudo or not.
        """

        if self.is_pseudo: 
            object_type = object_manager.get_type(self.canvas_context.pool_id, self.object_handle).data or ""
            return "{}\n<{}>".format(self.input_name, object_type)
        return self.input_name


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Getters & setters override ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

    # See the parent class for more details.

    def get_object_handle(self):
        return self.object_handle


    def get_node_type(self, ncname=True):
        return "input" if ncname else "{}:input".format(self.canvas_context.get_xproc_prefix())


    def get_node_name(self):
        return self.input_name


    def get_node_properties(self):
        return {**self.options}


    def get_specification(self):
        return object_manager.get_specification_from_name(
            pool_id  = self.canvas_context.pool_id,
            ent_type = "input"
        ).data


    def set_node_name(self, new_name):
        # Make sure the name is unique, and update the input spec.
        unique = new_name not in self.canvas_context.input_names
        if unique and (not str_none_or_empty(new_name)):
            # Remove the old name from the canvas context list.
            old_name = self.input_name
            self.canvas_context.input_names.remove(old_name)

            # Add the new name, and notify all outgoing connections.
            self.canvas_context.input_names.append(new_name)
            self.input_name = new_name
            for connection in self.output_connections:
                connection.set_from_port_name(new_name)

            # Update text and specs.
            self.update_text()
            if not self.is_pseudo and not self.is_default: self.__update_input_spec(old_name)


    def set_node_property(self, prop_name, prop_value):
        # Validate the name and value.
        valid = True
        if prop_name == "kind":
            if prop_value.lower() not in ["document", "parameter", ""]: valid = False
        elif prop_name != "select":
            if prop_value.lower() not in ["true", "false", ""]: valid = False

        # Set property only after successful validation.
        if valid: 
            self.options[prop_name] = prop_value
            self.__update_input_spec(self.input_name)
        else:
            publisher.infobar_message(self, "Error: Invalid property value.", Gtk.MessageType.ERROR)


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 

    def connect_output(self, connection):
        """
        Set the connection's output label.
        """

        self.output_connections[connection] = self.input_name
        connection.set_from_port_name(self.input_name)


    def update_text(self):
        """
        Centres the text in the container.        
        """

        self.label.props.text = str(self)
        self.label.props.y    = 2 * self.container_height / 3


    def remove(self):
        """
        Remove the input name from the canvas context list, and remove the spec via the object manager.        
        """

        # Remove from canvas context.
        if self.input_name in self.canvas_context.input_names:
            self.canvas_context.input_names.remove(self.input_name)

        # Remove spec.
        if (not self.is_pseudo) and (not self.is_default):
            object_manager.set_step_specification(
                pool_id         = self.canvas_context.pool_id, 
                object_handle   = self.object_handle,
                spec_type       = "input",
                name            = self.input_name,
                spec_properties = None
            )

        super().remove()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __get_unique_name(self, default_name):        
        """
        Generates an input name based on template.
        """

        # If it's a pseudo-input or a default input, use the given default name.
        if self.is_pseudo or self.is_default: 
            return default_name

        default = "new-input"

        counter = 0
        template = default
        while template in self.canvas_context.input_names:
            counter += 1
            template = default + ("_" + str(counter))
        return template


    def __update_input_spec(self, old_name):
        # Remove old.
        object_manager.set_step_specification(
            pool_id         = self.canvas_context.pool_id, 
            object_handle   = self.object_handle,
            spec_type       = "input",
            name            = old_name,
            spec_properties = None
        )

        # Add new.
        object_manager.set_step_specification(
            pool_id         = self.canvas_context.pool_id, 
            object_handle   = self.object_handle,
            spec_type       = "input",
            name            = self.input_name,
            spec_properties = self.options
        )



class GProcCanvasOutput(GProcCanvasNode):
    """
    The class which implements an output node on the canvas.
    """
    
    def __init__(self, *args, object_handle, output_name="", is_default=False, options=None, **kwargs):
        super().__init__(
            *args, 
            container_width  = float(theme["OUTPUT_WIDTH"]),
            container_height = float(theme["OUTPUT_HEIGHT"]),
            **kwargs
        ) 
        
        # Class variables.
        self.object_handle  = object_handle # Object handles may be passed in as part of a pseudo-input.
        self.options        = options or {}
        self.node_class     = "output"
        self.is_default     = is_default
        self.node_ns_prefix = self.canvas_context.get_xproc_prefix() 

        self.output_name  = output_name if self.is_default else self.__get_unique_name()
        self.canvas_context.output_names.append(self.output_name)

        if not is_default:
            object_manager.set_step_specification(
                pool_id         = self.canvas_context.pool_id, 
                object_handle   = self.object_handle,
                spec_type       = "output",
                name            = self.output_name,
                spec_properties = self.options
            )

        # Set up UI elements.
        self.container = GooCanvas.CanvasPolyline(
            **self.container_properties,
            parent     = self,
            points     = Shapes.diamond(self.container_width, self.container_height),
            close_path = True
        )
        
        # Update the parent to include the new port, and create connection points.
        self.canvas_context.parent_node.add_output_port(self.output_name)
        self.create_connection_points()


    def __repr__(self):
        """
        String representation override.
        """

        return self.output_name


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Getters & setters override ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

    # See the parent class for more details.

    def get_object_handle(self):
        return self.object_handle


    def get_node_type(self, ncname=True):
        return "output" if ncname else "{}:output".format(self.canvas_context.get_xproc_prefix())


    def get_node_name(self):
        return self.output_name


    def get_node_properties(self):
        return {**self.options}


    def get_specification(self):
        return object_manager.get_specification_from_name(
            pool_id  = self.canvas_context.pool_id,
            ent_type = "output"
        ).data


    def set_node_name(self, new_name):
        # Make sure the name is unique, and update the input spec.
        unique = new_name not in self.canvas_context.output_names
        if unique and (not str_none_or_empty(new_name)):
            # Remove the old name from the canvas context list.
            old_name = self.output_name
            self.canvas_context.output_names.remove(old_name)

            # Add the new name, and notify all incoming connections.
            self.canvas_context.output_names.append(new_name)
            self.output_name = new_name
            for connection in self.input_connections:
                connection.set_to_port_name(new_name)

            # Change the output port.
            object_manager.change_output_port_name(
                pool_id       = self.canvas_context.pool_id,
                object_handle = self.object_handle,
                old_port_name = old_name,
                new_port_name = new_name
            )
                
            # Update the parent node's output ports.
            self.canvas_context.parent_node.add_output_port(new_name)
            self.canvas_context.parent_node.remove_output_port(old_name)


    def set_node_property(self, prop_name, prop_value):
        # Validate the name and value.
        valid = True
        if prop_value.lower() not in ["true", "false", ""]: valid = False

        # Set property only after successful validation.
        if valid: 
            self.options[prop_name] = prop_value
            self.__update_output_spec(self.output_name)
        else:
            publisher.infobar_message(self, "Error: Invalid property value.", Gtk.MessageType.ERROR)


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛ 

    def connect_input(self, connection):
        """
        Set the connection's input label, and connect to the parent's output.
        """

        connectable = False

        # Check for p:empty behaviour.
        if connection.from_node.node_class == "source" and connection.from_node.get_node_type() == "empty":
            connectable = not (len(self.input_connections) > (0 if connection not in self.input_connections.keys() else 1))
        else:
            connectable = not self.__check_existing_empty_source()

        if connectable:
            # Update internal dict and connection label
            self.input_connections[connection] = self.output_name
            connection.set_to_port_name(self.output_name)

            # Connect to the parent object's output.
            if not str_none_or_empty(connection.from_port):
                connect_result = object_manager.connect_output(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.output_name,
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = connection.from_port 
                )
                if connect_result.exception is not None: raise connect_result.exception

        else:
            self.input_connections[connection] = ""
            connection.set_to_port_name(None)
            publisher.infobar_message(self, "Error: Cannot create connection, as it conflicts with other connections.", Gtk.MessageType.ERROR) #TRANSLATE#


    def disconnect_input(self, connection):
        """
        Removes the connection, and updates the output specification. Also adjusts other connections based on what was disconnected.
        """

        object_manager.disconnect_output(
            pool_id            = self.canvas_context.pool_id,
            to_object_handle   = self.object_handle,
            to_port            = self.output_name,
            from_object_handle = connection.from_node.get_object_handle()
        ) # Ignore error.
        
        super().disconnect_input(connection)

        # If the deleted connection was a p:empty, connect everything else to this output port.
        if connection.from_node.node_class == "source" and connection.from_node.get_node_type() == "empty":
            for conn in self.input_connections: 
                if str_none_or_empty(self.input_connections[conn]): self.connect_input(conn)

        # If there is one last remaining connection and it's not connected via object manager, update it.
        elif len(self.input_connections) == 1 and str_none_or_empty(list(self.input_connections.values())[0]):
            conn = list(self.input_connections.keys())[0]
            self.connect_input(conn)


    def notify_incoming_port_change(self, connection, old_port_name):
        if connection in self.input_connections.keys():
            # Disconnect old connection.
            object_manager.disconnect_output(
                pool_id            = self.canvas_context.pool_id,
                to_object_handle   = self.object_handle,
                to_port            = self.output_name,
                from_object_handle = connection.from_node.get_object_handle(),
                from_port          = old_port_name
            )

            # Create new connection.
            new_port_name = connection.from_port
            if not str_none_or_empty(new_port_name):
                object_manager.connect_output(
                    pool_id            = self.canvas_context.pool_id,
                    to_object_handle   = self.object_handle,
                    to_port            = self.output_name,
                    from_object_handle = connection.from_node.get_object_handle(),
                    from_port          = new_port_name
                )


    def remove(self):
        """
        Remove the input name from the canvas context list and the parent node. Also update the specs if relevant.
        """

        # Remove info from parent.
        if self.output_name in self.canvas_context.output_names:
            self.canvas_context.output_names.remove(self.output_name)
            self.canvas_context.parent_node.remove_output_port(self.output_name)

        # Remove spec.
        if not self.is_default:
            object_manager.set_step_specification(
                pool_id         = self.canvas_context.pool_id, 
                object_handle   = self.object_handle,
                spec_type       = "output",
                name            = self.output_name,
                spec_properties = None
            ) # Ignore error.

        super().remove()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __get_unique_name(self):
        """
        Generates an output name based on template.
        """

        default = "new-output"

        counter = 0
        template = default
        while template in self.canvas_context.output_names:
            counter += 1
            template = default + ("_" + str(counter))
        return template


    def __update_output_spec(self, old_name):
        if not self.is_default:
            # Remove old.
            object_manager.set_step_specification(
                pool_id         = self.canvas_context.pool_id, 
                object_handle   = self.object_handle,
                spec_type       = "output",
                name            = old_name,
                spec_properties = None
            )

            # Add new.
            object_manager.set_step_specification(
                pool_id         = self.canvas_context.pool_id, 
                object_handle   = self.object_handle,
                spec_type       = "output",
                name            = self.output_name,
                spec_properties = self.options
            )
        

    def __check_existing_empty_source(self):
        """
        Checks whether a p:empty is already connected to this output.
        """

        for connection in self.input_connections.keys():
            if connection.from_node.node_class == "source" and connection.from_node.get_node_type() == "empty" and not str_none_or_empty(self.input_connections[connection]):
                return True

        return False