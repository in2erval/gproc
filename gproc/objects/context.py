"""
This module contains the context object for canvases.
"""

from gproc.utils.enums import Environment
from gproc.utils.misc  import XPROC_NAMESPACE



class GProcCanvasContext:
    """
    A class which holds contextual information for canvases. This is used to keep track of common values (such as pool ID) and naming scopes.    
    """

    def __init__(self, *args, environment, parent_canvas, parent_node, pool_id, object_handle, **kwargs):
        # Class variables.
        self.environment   = environment   # For determining what nodes can be inserted.
        self.parent_canvas = parent_canvas # For navigating upwards in the hierarchy.
        self.parent_node   = parent_node   # For reflecting modified values (such as output port names).
        self.object_handle = object_handle # For interfacing with the object manager.
        self.pool_id       = pool_id       # For interfacing with the object manager.
        self.is_exportable = False         # For determining whether the canvas represents an exportable object.
        self.canvas        = None          # Will be updated when the canvas is created.

        # Scoping.        
        self.input_names    = [] # Relevant for declaration environment.
        self.output_names   = [] # Relevant for declaration environment.
        self.option_names   = [] # Relevant for declaration environment.
        self.variable_names = [] # Relevant in any subpipeline.
        self.namespace_map  = {} # Relevant in any subpipeline.
        
        # Any additional values.
        for key, value in kwargs.items():
            self.__setattr__(key, value)


    def register_canvas(self, canvas):
        """
        Update the context's canvas reference.
        """

        if self.canvas is None: self.canvas = canvas


    def get_xproc_prefix(self):
        """
        Returns the prefix for the XProc namespace. Many documents will default to using 'p', but
        this implementation can be changed to allow the user to set their own.
        """

        return "p"

    
    def find_prefix(self, namespace):
        """
        Returns the first found prefix for the given namespace.
        """

        if namespace == XPROC_NAMESPACE: return self.get_xproc_prefix()
            
        for prefix, ns in self.namespace_map.items():
            if ns == namespace: return prefix

        return None

    
    def find_namespace(self, prefix):
        if prefix == self.get_xproc_prefix(): return XPROC_NAMESPACE

        return self.namespace_map.get(prefix, None)