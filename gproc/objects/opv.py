"""
This module contains classes for canvas node elements related to user-provided values.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, GooCanvas

from gproc.objects.entity import GProcCanvasEntity
from gproc.shared         import theme
from gproc.utils.errors   import GProcError
from gproc.utils.misc     import str_none_or_empty, is_qname
from gproc.utils.shapes   import Shapes
from xproc                import object_manager



class GProcCanvasVariable(GProcCanvasEntity):
    """
    The class which implements a variable node on the canvas.
    """
    
    def __init__(self, *args, canvas_context, **kwargs):
        # Generate a variable name first.
        self.canvas_context = canvas_context # Early assignment because __get_unique_name() depends on it.
        node_name = self.__get_unique_name()

        super().__init__(
            *args, 
            node_class       = "variable",
            node_name        = node_name,
            node_ns_prefix   = self.canvas_context.get_xproc_prefix(),
            canvas_context   = canvas_context,
            container_width  = float(theme["VARIABLE_WIDTH"]),
            container_height = float(theme["VARIABLE_HEIGHT"]),
            **kwargs
        )

        # Set up UI elements.
        self.container = GooCanvas.CanvasPolyline(
            **self.container_properties,
            parent     = self,
            points     = Shapes.hexagon(self.container_width, self.container_height),
            close_path = True
        )
        self.label = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        self.set_node_name(node_name)
        self.update_text()
        self.create_connection_points()


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛

    def set_node_name(self, node_name):
        """
        Sets the node name. The name must be unique.
        """

        # Check nonemptiness, and uniqueness of scope.
        if (not str_none_or_empty(node_name)) and (node_name not in self.canvas_context.variable_names):
            # Remove the old variable name from the canvas context list.
            old_name = self.get_node_name()
            if old_name in self.canvas_context.variable_names:
                self.canvas_context.variable_names.remove(old_name)

            # Add it to the canvas context list, and update the node name via the object handle.
            self.canvas_context.variable_names.append(node_name)
            set_name_result = object_manager.set_name(
                pool_id       = self.canvas_context.pool_id, 
                object_handle = self.object_handle, 
                new_name      = node_name
            )
            if set_name_result.exception: raise set_name_result.exception

            # Update the label text.
            self.update_text()


    def connect_input(self, connection):
        """
        Update the object manager when a new connection is made.
        """

        # We just set it to the empty string because it doesn't matter.
        self.input_connections[connection] = ""
        connect_result = object_manager.connect_input(
            pool_id            = self.canvas_context.pool_id,
            to_object_handle   = self.object_handle,
            to_port            = "default",
            from_object_handle = connection.from_node.get_object_handle(),
            from_port          = connection.from_port
        )
        if connect_result.exception is not None: raise connect_result.exception

    
    def disconnect_input(self, connection):
        """
        Update the object manager when a connection is removed.
        """

        if connection in self.input_connections.keys():
            disconnect_result = object_manager.disconnect_input(
                pool_id          = self.canvas_context.pool_id,
                to_object_handle = self.object_handle,
                to_port          = "default"
            )
            if disconnect_result.exception is not None: raise disconnect_result.exception
            del self.input_connections[connection]


    def remove(self):
        """
        Overriding the removal so that the variable name is made available in the canvas context.        
        """

        name = self.get_node_name()
        if name in self.canvas_context.variable_names: self.canvas_context.variable_names.remove(name)
        super().remove()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛
     
    def __get_unique_name(self):
        """
        Generates a variable name based on template.
        """

        default = "new-variable"

        counter = 0
        template = default
        while template in self.canvas_context.variable_names:
            counter += 1
            template = default + ("_" + str(counter))
        return template



class GProcCanvasWithBase(GProcCanvasEntity):
    """
    The base class for with-option and with-param.
    """
    
    def __init__(self, *args, default, **kwargs):
        super().__init__(
            *args,
            node_name        = default,
            node_ns_prefix   = kwargs["canvas_context"].get_xproc_prefix(),
            container_width  = float(theme["WITH-X_WIDTH"]),
            container_height = float(theme["WITH-X_HEIGHT"]),
            **kwargs
        )

        # Set up UI elements.
        self.container = GooCanvas.CanvasPolyline(
            **self.container_properties,
            parent     = self,
            points     = Shapes.parallelogram(self.container_width, self.container_height, float(theme["WITH-X_ADJACENT"])),
            close_path = True
        )
        self.label = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        self.set_node_name(default)
        self.update_text()
        self.create_connection_points()


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛    

    def get_node_bounds(self):
        """
        Adjusted node bounds to make the connection points fit the shape.
        """

        full_bounds = super().get_node_bounds()
        full_bounds.x1 += float(theme["WITH-X_ADJACENT"]) / 2
        full_bounds.x2 -= float(theme["WITH-X_ADJACENT"]) / 2
        return full_bounds

    
    def connect_input(self, connection):
        """
        Update the object manager when a new connection is made.
        """

        # We just set it to the empty string because it doesn't matter.
        self.input_connections[connection] = ""
        if not str_none_or_empty(connection.from_port):
            connect_result = object_manager.connect_input(
                pool_id            = self.canvas_context.pool_id,
                to_object_handle   = self.object_handle,
                to_port            = "default",
                from_object_handle = connection.from_node.get_object_handle(),
                from_port          = connection.from_port
            )
            if connect_result.exception is not None: raise connect_result.exception

    
    def disconnect_input(self, connection):
        """
        Update the object manager when a connection is removed.
        """

        if connection in self.input_connections.keys():
            disconnect_result = object_manager.disconnect_input(
                pool_id          = self.canvas_context.pool_id,
                to_object_handle = self.object_handle,
                to_port          = "default"
            )
            if disconnect_result.exception is not None: raise disconnect_result.exception
            del self.input_connections[connection]


    def notify_incoming_port_change(self, connection, old_port_name):
        """
        Reconnect the input so that it overwrites the existing connection.
        """

        if connection in self.input_connections.keys():
            self.disconnect_input(connection)
            self.connect_input(connection)


    def create_connection_points(self, *args, **kwargs):
        """
        Set x and y adjustments before calling the default method.
        """
    
        super().create_connection_points(*args, **kwargs, x_adj=float(theme["WITH-X_ADJACENT"]) / 2)



class GProcCanvasWithOption(GProcCanvasWithBase):
    """
    The class which implements a with-option node on the canvas - most of the implementation comes from the base class.
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs,
            default    = "new-option", 
            node_class = "with-option"
        )



class GProcCanvasWithParam(GProcCanvasWithBase):
    """
    The class which implements a with-param node on the canvas - most of the implementation comes from the base class.
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs,
            default    = "new-param", 
            node_class = "with-param"
        )

