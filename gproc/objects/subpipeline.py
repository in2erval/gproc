"""
This module contains classes which together manage a set of subpipelines.
"""

import gi, gproc, functools
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, Gdk, GooCanvas, Pango

from gproc.objects.context import GProcCanvasContext
from gproc.objects.node    import GProcCanvasNode
from gproc.publisher       import publisher
from gproc.shared          import theme
from gproc.utils.enums     import AggregateStepType, Environment
from gproc.utils.misc      import print_exception, parse_rgba, pointer_on_hover, str_none_or_empty
from gproc.utils.shapes    import Shapes
from xproc                 import object_manager


def sync_with_parent(func):
    @functools.wraps(func)
    def sync(self, *args, **kwargs):
        if self.sync_with_parent: 
            func_name = func.__name__
            parent    = self.sp_component.parent_node
            return parent.__getattribute__(func_name)(*args, **kwargs)
        else: return func(self, *args, **kwargs)
    return sync



class GProcCanvasSubpipelineNode(GProcCanvasNode):
    """
    The class which implements a single subpipeline to appear in a (potentially singular) list of subpipelines. This element lets users open lower-level canvases.
    """

    def __init__(self, *args, sp_component, node_type=None, label_string=None, **kwargs):
        super().__init__(
            *args, 
            container_width  = float(theme["SUBPIPELINE_WIDTH"]),
            container_height = float(theme["SUBPIPELINE_HEIGHT"]),
            **kwargs
        )

        # Class variables.
        self.sp_component   = sp_component
        self.node_class     = sp_component.aggregate_step_type.node_class()
        self.node_ns_prefix = self.canvas_context.get_xproc_prefix()
        self.is_connectable = False
        self.is_moveable    = False
        self.object_handle  = None

        # Create the new object based on aggregate step type and what's been passed in for node type.
        self.__setup_object_handle(node_type)

        # If the object handle isn't different, then selecting the subpipeline should be the same as selecting the node.
        self.sync_with_parent = self.object_handle == sp_component.object_handle

        # Create the child canvas.        
        child_canvas_context = GProcCanvasContext(
            environment    = Environment.get_value(self.get_node_type()),
            parent_canvas  = self.canvas_context.canvas,
            parent_node    = sp_component.parent,
            object_handle  = self.object_handle,
            pool_id        = self.canvas_context.pool_id,
            is_exportable  = self.sp_component.aggregate_step_type == AggregateStepType.DECLARATION,
            input_names    = [],
            output_names   = [],
            option_names   = [],
            namespace_map  = self.canvas_context.namespace_map, # ┓ Both of these are references!
            variable_names = self.canvas_context.variable_names # ┛
        )
        self.linked_canvas = gproc.objects.canvas.GProcCanvas(canvas_context = child_canvas_context)

        # Set up UI elements.
        self.label_string = label_string or "subpipeline"
        self.label_properties["font_desc"] = Pango.FontDescription(theme["NODE_FONT_FACE_SMALL"])

        self.container = GooCanvas.CanvasPolyline(
            **self.container_properties,
            parent     = self,
            points     = Shapes.hexagon(self.container_width, self.container_height),
            close_path = True
        )
        self.label     = GooCanvas.CanvasText(
            **self.label_properties,
            parent = self,
            width  = self.container_width
        )

        # Update the text and connect GTK signal.
        self.update_text()
        self.connect("button-release-event", self.__event_click)


    def __repr__(self):
        """
        String representation override, for showing p:when's test case.
        """

        if self.get_node_type() == "when":
            return "{}: {}".format(self.label_string, self.get_node_properties().get("test", ""))
        else: return self.label_string 


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Getters & setters override ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

    # See the parent class for details. These are more or less the same as GProcCanvasEntity, but it's not derived from that based on inconveniences.

    def get_object_handle(self):
        return self.object_handle


    @sync_with_parent
    def get_node_type(self, ncname=True):
        prefix = "" if str_none_or_empty(self.node_ns_prefix) else (self.node_ns_prefix + ":")
        return (("" if ncname else prefix) + object_manager.get_type(self.canvas_context.pool_id, self.object_handle).data) or ""


    @sync_with_parent
    def get_node_name(self):
        return object_manager.get_name(self.canvas_context.pool_id, self.object_handle).data or ""


    @sync_with_parent
    def get_node_namespace(self):
        return object_manager.get_namespace(self.canvas_context.pool_id, self.object_handle).data or ""


    @sync_with_parent
    def get_node_documentation(self):
        return object_manager.get_documentation(self.canvas_context.pool_id, self.object_handle).data or ""


    @sync_with_parent
    def get_node_properties(self):
        return object_manager.get_properties(self.canvas_context.pool_id, self.object_handle).data or {}


    @sync_with_parent
    def get_node_additionals(self):
        return object_manager.get_additionals(self.canvas_context.pool_id, self.object_handle).data or {}


    def get_input_connections(self):
        return []#self.sp_component.parent_node.get_input_connections()


    def get_output_connections(self):
        return []#self.sp_component.parent_node.get_output_connections()


    @sync_with_parent
    def set_node_name(self, node_name):
        set_name_result = object_manager.set_name(self.canvas_context.pool_id, self.object_handle, node_name)
        if set_name_result.exception is not None: print_exception(set_name_result.exception)
        self.update_text()


    @sync_with_parent
    def set_node_documentation(self, node_documentation):
        set_documentation_result = object_manager.set_documentation(self.canvas_context.pool_id, self.object_handle, node_documentation)
        if set_documentation_result.exception is not None: print_exception(set_documentation_result.exception)


    @sync_with_parent
    def set_node_property(self, prop_name, prop_value):
        set_property_result = object_manager.set_property(self.canvas_context.pool_id, self.object_handle, prop_name, prop_value)
        if set_property_result.exception is not None: print_exception(set_property_result.exception)
        self.update_text()


    @sync_with_parent
    def set_node_additional(self, addi_name, addi_value):
        set_additional_result = object_manager.set_additional(self.canvas_context.pool_id, self.object_handle, addi_name, addi_value)
        if set_additional_result.exception is not None: print_exception(set_additional_result.exception)


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛ 

    def add_pseudo_input(self, object_handle, port_name):
        """
        Creates a pseudo-input in the linked canvas.
        """

        self.linked_canvas.add_pseudo_input(object_handle, port_name)


    def remove_pseudo_input(self, object_handle, port_name):
        """
        Removes a pseudo-input from the linked canvas.
        """

        self.linked_canvas.remove_pseudo_input(object_handle, port_name)


    def change_pseudo_input(self, object_handle, old_port_name, new_port_name):
        """
        Changes a pseudo-input in the linked canvas.
        """

        self.linked_canvas.change_pseudo_input(object_handle, old_port_name, new_port_name)


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_click(self, ci, ti, e):
        """
        Open the canvas if this node has been clicked before.
        """

        if self.is_activated: 
            publisher.open_canvas(self, self.linked_canvas)


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛

    def __setup_object_handle(self, node_type):      
        """
        Copies the object handle from the sp_component, or creates a new object under the sp_component's object handle.
        """

        # Decide based on aggregate step type.
        aggregate_step_type = self.sp_component.aggregate_step_type
        if aggregate_step_type == AggregateStepType.CHOOSE or aggregate_step_type == AggregateStepType.TRY:
            # Create a compound step under the multicontainer step.
            create_object_result = object_manager.create_object(
                pool_id       = self.canvas_context.pool_id,
                ent_class     = "compound",
                ent_type      = node_type,
                parent_handle = self.sp_component.object_handle
            )
            if create_object_result.exception is not None: raise create_object_result.exception
            self.object_handle = create_object_result.data
            
        # No need to create an object, just use the parent object handle.
        else:
            self.object_handle = self.sp_component.object_handle


    def __remove_from_object_manager(self):
        """
        Remove the object from the object manager, if relevant.
        """

        object_manager.remove_object(
            pool_id       = self.canvas_context.pool_id,
            object_handle = self.object_handle
        ) # Ignore error.


    # ┏━━━━━━━━━━━━━━━━━━┓
    # ┃                  ┃
    # ┃ Override methods ┃
    # ┃                  ┃
    # ┗━━━━━━━━━━━━━━━━━━┛     

    def remove(self, override=False):
        """
        Checks if it's suitable for removal, and removes everything in the linked canvas.
        """

        if override: # This is separate to avoid the call to sp_component.remove_subpipeline().
            self.linked_canvas.delete_all_nodes()
            self.__remove_from_object_manager()

        # Deletion confirmed by sp_component
        elif self.sp_component.remove_subpipeline(self):
            self.linked_canvas.delete_all_nodes()
            self.__remove_from_object_manager()

        # Don't delete.
        else:
            self.toggle_highlight(False)



class GProcCanvasSubpipelineComponent(GooCanvas.CanvasGroup):
    """
    This class is the implementation of the subpipeline container and revealer, placed on all aggregate steps. The user can reveal all available subpipelines and potentially add more depending on the type.

    See the docs for GooCanvas.CanvasGroup: http://lazka.github.io/pgi-docs/GooCanvas-2.0/classes/CanvasGroup.html
    """
    
    def __init__(self, *args, canvas_context, object_handle, aggregate_step_type, **kwargs):
        super().__init__(*args, **kwargs)

        # Class variables
        self.canvas_context      = canvas_context
        self.object_handle       = object_handle
        self.aggregate_step_type = aggregate_step_type
        self.parent_node         = self.props.parent # If a linter says self.props is an int, don't believe its lies.

        self.expanded          = False
        self.pseudo_inputs     = {} # Indexed by object handle. Values are port names. Maybe change it?

        # UI components and variables
        self.subpipelines      = []   # List of GProcCanvasSubpipelineNode-s
        self.subpipelines_view = None # The subpipelines, and the bracket on the left of it.
        self.default           = None # Default subpipeline.
        self.controls          = None # For p:choose, the buttons to add branches and defaults.
        self.expand_button     = self.__create_expand_button()

        self.subpipelines_gap_x  = float(theme["SUBPIPELINE_GAP_X"])
        self.subpipelines_gap_y  = float(theme["SUBPIPELINE_GAP_Y"])
        self.bracket_gap         = float(theme["SUBPIPELINE_BRACKET_GAP"])
        self.bracket_width       = float(theme["SUBPIPELINE_BRACKET_WIDTH"])
        self.controls_width      = float(theme["SUBPIPELINE_CONTROLS_WIDTH"])
        self.controls_height     = float(theme["SUBPIPELINE_CONTROLS_HEIGHT"])
        self.controls_gap        = float(theme["SUBPIPELINE_CONTROLS_GAP"])
        self.controls_fill_color = parse_rgba(theme["NODE_BACKGROUND_COLOUR"])
        self.controls_line_color = parse_rgba(theme["NODE_LINE_COLOUR"])
        self.controls_font       = Pango.FontDescription(theme["NODE_FONT_FACE_SMALL"])

        # Run initialisation.
        self.__init_subpipelines_view()
        self.subpipelines_view.props.visibility = GooCanvas.CanvasItemVisibility.INVISIBLE


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛ 

    def remove_subpipeline(self, subpipeline):
        """
        Removes the given subpipeline. Invoked by the subpipeline itself, since the 'delete current node' functionality of the canvas will invoke removal on the subpipeline element.
        The boolean result will determine whether the subpipeline will continue with the removal or not.
        """

        # Only delete if it's a p:choose.
        if self.aggregate_step_type == AggregateStepType.CHOOSE:
            # Deleting the default subpipeline, so make the default creation button visible again.
            if subpipeline is self.default:
                self.controls.default.props.visibility = GooCanvas.CanvasItemVisibility.VISIBLE
                self.default = None
                self.__update_subpipelines_view()
                self.parent_node.remove_xpath_context(subpipeline.object_handle)
                return True

            # Deleting the branches, but don't allow if it's the only one left.
            if subpipeline in self.subpipelines and len(self.subpipelines) > 1:
                self.subpipelines.remove(subpipeline)
                self.__update_subpipelines_view()
                self.parent_node.remove_xpath_context(subpipeline.object_handle)
                return True

        return False


    def remove_all_subpipelines(self):
        """
        Removes everything.
        """

        for subpipeline in self.subpipelines:
            subpipeline.remove(override=True)
        if self.default is not None:
            self.default.remove(override=True)


    def add_pseudo_input(self, object_handle, port_name):
        """
        Creates a pseudo-input in all available subpipelines.
        """

        # Normal subpipelines
        for subpipeline in self.subpipelines:
            subpipeline.add_pseudo_input(object_handle, port_name)

        # Default subpipeline
        if self.default is not None: self.default.add_pseudo_input(object_handle, port_name)

        # Add it to the internal dictionary.
        self.pseudo_inputs[object_handle] = port_name

    
    def remove_pseudo_input(self, object_handle, port_name):
        """
        Removes a pseudo-input from all available subpipelines.
        """

        # Normal subpipelines
        for subpipeline in self.subpipelines:
            subpipeline.remove_pseudo_input(object_handle, port_name)

        # Default subpipeline
        if self.default is not None: self.default.remove_pseudo_input(object_handle, port_name)

        # Remove it from the internal dictionary.
        if object_handle in self.pseudo_inputs: del self.pseudo_inputs[object_handle]


    def change_pseudo_input(self, object_handle, old_port_name, new_port_name):
        """
        Changes a pseudo-input in all available subpipelines.
        """

        # Normal subpipelines
        for subpipeline in self.subpipelines:
            subpipeline.change_pseudo_input(object_handle, old_port_name, new_port_name)

        # Default subpipeline
        if self.default is not None: self.default.change_pseudo_input(object_handle, old_port_name, new_port_name)

        # Change it in the internal dictionary.
        self.pseudo_inputs[object_handle] = new_port_name


    def get_subpipeline_title(self, object_handle):
        """
        Returns a string which will be reflected in the title of the workspace page, for a given object handle. Invoked by `gproc.workspace.GProcCanvasTab`.
        """

        # p:choose
        if self.aggregate_step_type == AggregateStepType.CHOOSE:
            handles = [spl.object_handle for spl in self.subpipelines]
            if object_handle in handles:
                return "case {}".format(handles.index(object_handle) + 1)
            elif self.default is not None and object_handle == self.default.object_handle:
                return "otherwise"

        # p:try
        elif self.aggregate_step_type == AggregateStepType.TRY:
            if object_handle in [spl.object_handle for spl in self.subpipelines]:
                return "try"
            elif object_handle == self.default.object_handle:
                return "catch"

        # Anything else
        return ""


    # ┏━━━━━━━━━━━━┓
    # ┃            ┃
    # ┃ UI methods ┃
    # ┃            ┃
    # ┗━━━━━━━━━━━━┛

    def __create_expand_button(self):
        """
        Creates the triangular expand button which goes on the left side of the container.
        """

        button = GooCanvas.CanvasPath(
            parent      = self,
            data        = """
                M 0 10
                L 0 1
                C 0 0 0 0 1 1
                L 9 9
                C 10 10 10 10 9 11
                L 1 19
                C 0 20 0 20 0 19
                L 0 10
            """,
            line_width   = 1,
            stroke_color = "black",
            fill_color   = "white"
        )

        pointer_on_hover(button)
        button.connect("button-release-event", self.__event_expand_button_click)
        return button


    def __create_subpipelines_controls(self):
        """
        Creates the subpipeline controls for p:choose (add branch and default).
        """

        # Initialise group and base properties.
        button_group      = GooCanvas.CanvasGroup() # Each button will be in this button group.
        button_properties = {
              "fill_color_gdk_rgba": self.controls_fill_color,
            "stroke_color_gdk_rgba": self.controls_line_color,
                       "line_width": 1,
                            "width": self.controls_width,
                           "height": self.controls_height,
                         "radius_x": self.controls_height/2,
                         "radius_y": self.controls_height/2
        }
        label_properties  = {
                      "alignment": Pango.Alignment.CENTER,
                         "anchor": GooCanvas.CanvasAnchorType.WEST,
                      "ellipsize": Pango.EllipsizeMode.END,
                      "font_desc": self.controls_font,
            "fill_color_gdk_rgba": self.controls_line_color,
                          "width": self.controls_width,
                              "y": self.controls_height/2
        }

        # Create branch button from the properties above.
        branch_button = GooCanvas.CanvasGroup()
        branch_button.add_child(GooCanvas.CanvasRect(
            **button_properties
        ), -1)
        branch_button.add_child(GooCanvas.CanvasText(
            **label_properties,
            text = "Add branch" #TRANSLATE#
        ), -1)
        branch_button.connect("button-release-event", self.__event_branch_button_click)
        pointer_on_hover(branch_button)

        # Create default button from the properties above.
        default_button = GooCanvas.CanvasGroup()
        default_button.add_child(GooCanvas.CanvasRect(
            **button_properties,
            x = self.controls_width + self.controls_gap
        ), -1)
        default_button.add_child(GooCanvas.CanvasText(
            **label_properties,
            text = "Add default", #TRANSLATE#
            x = self.controls_width + self.controls_gap
        ), -1)
        default_button.connect("button-release-event", self.__event_default_button_click)
        pointer_on_hover(default_button)

        # Add the two buttons to the group.
        button_group.add_child(branch_button, -1)
        button_group.add_child(default_button, -1)
        button_group.container_height  = self.controls_height
        button_group.default = default_button
        return button_group


    def __create_branch(self):
        """
        Creates a subpipeline when the branch button is clicked.
        """

        if self.aggregate_step_type == AggregateStepType.CHOOSE:
            return GProcCanvasSubpipelineNode(
                canvas_context = self.canvas_context,
                sp_component   = self,
                node_type      = "when",
                label_string   = "when"
            )
        raise ValueError("Attempted to create a branch on a step that is not p:choose")


    def __create_default(self):
        """
        Creates a subpipeline when the default button is clicked.
        """

        if self.aggregate_step_type == AggregateStepType.CHOOSE:
            return GProcCanvasSubpipelineNode(
                canvas_context = self.canvas_context,
                sp_component   = self,
                node_type      = "otherwise",
                label_string   = "otherwise"
            )
        raise ValueError("Attempted to create a default on a step that is not p:choose")


    def __init_subpipelines_view(self):
        """
        Initialised all UI elements based on the aggregate step type.
        """

        # COMPOUND or DECLARATION - single pipeline only, automatically default.
        if self.aggregate_step_type == AggregateStepType.COMPOUND or self.aggregate_step_type == AggregateStepType.DECLARATION:
            self.default = GProcCanvasSubpipelineNode(
                canvas_context = self.canvas_context,
                sp_component   = self,
                label_string   = "subpipeline"
            )

        # For CHOOSE, there's a first branch and also controls to add more.
        elif self.aggregate_step_type == AggregateStepType.CHOOSE:
            self.subpipelines.append(self.__create_branch())
            self.controls = self.__create_subpipelines_controls()

        # For TRY, there are two pipelines - designate one as a normal subpipeline, and one as the default subpipeline.
        elif self.aggregate_step_type == AggregateStepType.TRY:
            self.subpipelines.append(GProcCanvasSubpipelineNode(
                canvas_context = self.canvas_context,
                sp_component   = self,
                node_type      = "group",
                label_string   = "try"
            ))
            self.default = GProcCanvasSubpipelineNode(
                canvas_context = self.canvas_context,
                sp_component   = self,
                node_type      = "catch",
                label_string   = "catch"
            )
        
        # Align the subpipelines.
        self.__update_subpipelines_view()


    def __update_subpipelines_view(self):
        """
        Takes the current set of subpipelines, positions them accordingly, and adjusts the bracket on the left side.        
        """

        # We want to remove the old view first.
        if self.subpipelines_view is not None:
            self.subpipelines_view.remove()

        # Some cool variables.
        group  = GooCanvas.CanvasGroup()
        height = 0 
        layer  = 0

        # A routine to add an item to the group.
        def add_item(item):
            nonlocal height
            nonlocal layer
            item.props.x      = self.bracket_width + self.bracket_gap
            item.props.y      = height + (layer * self.subpipelines_gap_y)
            item.props.parent = group
            height           += item.container_height + (layer * self.subpipelines_gap_y)
            layer             = min(layer+1, 1)            

        # Apply routine for each subpipeline and control found.
        for subpipeline in self.subpipelines:
            add_item(subpipeline)
        if self.default is not None:
            add_item(self.default)
        if self.controls is not None:
            add_item(self.controls)

        # Create the bracket.
        bracket = GooCanvas.CanvasPolyline(
            line_width            = 2,
            stroke_color_gdk_rgba = Gdk.RGBA(0, 0, 0, 1), # black
            points                = Shapes.bracket(self.bracket_width, height)
        )
        group.add_child(bracket, -1)

        # Position the entire group, and add it to this subpipeline component.
        group.props.x = self.subpipelines_gap_x
        group.props.y = 10 - height/2
        self.subpipelines_view = group
        self.add_child(group, -1)


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_expand_button_click(self, ci, ti, e):
        """
        Reveal or hide the list of subpipelines.
        """

        if self.expanded:
            self.subpipelines_view.props.visibility = GooCanvas.CanvasItemVisibility.INVISIBLE
            self.expanded = False
        else:
            self.subpipelines_view.props.visibility = GooCanvas.CanvasItemVisibility.VISIBLE
            self.expanded = True


    def __event_branch_button_click(self, ci, ti, e):
        """
        Invoked when the branch button is clicked - create the branch, add all known pseudo-inputs to it, and update the view.
        """

        branch = self.__create_branch()
        self.subpipelines.append(branch)

        for object_handle, port_name in self.pseudo_inputs.items():
            branch.add_pseudo_input(object_handle, port_name)

        self.__update_subpipelines_view()

        # Notify the parent about the new set of subpipelines if this is a p:choose.
        if self.aggregate_step_type == AggregateStepType.CHOOSE: self.parent_node.set_available_xpath_contexts()


    def __event_default_button_click(self, ci, ti, e):
        """
        Invoked when the default button is clicked - create the default if it doesn't exist, add all known pseudo-inputs to it, and update the view.
        """
        if self.default is None:
            self.default = self.__create_default()

            for object_handle, port_name in self.pseudo_inputs.items():
                self.default.add_pseudo_input(object_handle, port_name)

            self.controls.default.props.visibility = GooCanvas.CanvasItemVisibility.INVISIBLE
            self.__update_subpipelines_view()

            # Notify the parent about the new set of subpipelines if this is a p:choose.
            if self.aggregate_step_type == AggregateStepType.CHOOSE: self.parent_node.set_available_xpath_contexts()