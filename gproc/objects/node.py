"""
This module contains the base class for all canvas node elements.
"""

import gi, cairo
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import Gtk, Gdk, Pango, GooCanvas

from gproc.shared        import theme, data
from gproc.utils.misc    import parse_rgba, pointer_on_hover
from gproc.utils.enums   import Orientation
from xproc               import object_manager


class GProcCanvasNode(GooCanvas.CanvasGroup):
    """
    A derived class which implements the base canvas element which can be added by the user. A node is characterised by its ability to be connected to something and have a related object handle to interact with the object manager.

    This base class provides a number of default methods for other components to use - subclasses should implement a subset of these to match how it should behave when the method is invoked.

    See the docs for GooCanvas.CanvasGroup: http://lazka.github.io/pgi-docs/GooCanvas-2.0/classes/CanvasGroup.html
    """
    
    def __init__(self, *args, canvas_context, container_width, container_height, **kwargs):
        super().__init__(*args, **kwargs)

        # Class variables.
        self.canvas_context = canvas_context # For pool ID and possibly the object handle.
        self.is_deleteable  = True # ┓
        self.is_moveable    = True # ┃
        self.is_connectable = True # ┃ These flags define how the user can interact with the node.
        self.is_selectable  = True # ┃
        self.is_editable    = True # ┛
        
        self.node_class       = None # Configuration pane uses the overwritten values of this to determine how it should set up the pane.
        self.node_ns_prefix   = None # Node type and prefix are stored differently.
        self.container_width  = container_width 
        self.container_height = container_height 

        self.is_activated           = False # Node was selected twice. This is used to open canvases from the subpipeline nodes.
        self.connection_orientation = None  # Which one of North/East/South/West was selected.

        # UI elements and its default properties - subclasses will use this along with additional parameters to set up the graphics.
        self.container            = None
        self.container_properties = { # Expected to be any GooCanvas element, but typically CanvasRect or CanvasPolyline.
              "fill_color_gdk_rgba": parse_rgba(theme["NODE_BACKGROUND_COLOUR"]),
            "stroke_color_gdk_rgba": parse_rgba(theme["NODE_LINE_COLOUR"]),
                       "line_width": float(theme["NODE_LINE_WIDTH"])
        }

        self.label            = None
        self.label_properties = { # Expected to be a GooCanvas.CanvasText.
                      "alignment": Pango.Alignment.CENTER,
                         "anchor": GooCanvas.CanvasAnchorType.WEST,
                      "ellipsize": Pango.EllipsizeMode.END,
            "fill_color_gdk_rgba": parse_rgba(theme["NODE_FONT_COLOUR"]),
                      "font_desc": Pango.FontDescription(theme["NODE_FONT_FACE"]),
                           "wrap": Pango.WrapMode.WORD_CHAR
        }
        self.connection_points = []
        self.connection_points_properties = { # Expected to be a GooCanvas.CanvasRect.
                            "width": float(theme["CONN_POINT_DIAMETER"]),
                           "height": float(theme["CONN_POINT_DIAMETER"]),
                         "radius_x": float(theme["CONN_POINT_DIAMETER"]),
                         "radius_y": float(theme["CONN_POINT_DIAMETER"]),
              "fill_color_gdk_rgba": Gdk.RGBA(0, 0, 0, 0), # Transparent
            "stroke_color_gdk_rgba": parse_rgba(theme["CONN_POINT_LINE_COLOUR"]),
                       "line_width": float(theme["CONN_POINT_WIDTH"]),
                       "visibility": GooCanvas.CanvasItemVisibility.INVISIBLE
        }
        
        # Keep track of connections.
        self.input_connections  = {} # Key: connection object, Value: port name
        self.output_connections = {} # Key: connection object, Value: port name
        self.input_port_names   = [] # List of string
        self.output_port_names  = [] # List of string

        # For revealing the connection points.
        self.connect("enter-notify-event", self.__event_hover)
        self.connect("leave-notify-event", self.__event_hover)


    # ┏━━━━━━━━━━━━━┓
    # ┃             ┃
    # ┃   Getters   ┃
    # ┃             ┃
    # ┗━━━━━━━━━━━━━┛

    def get_object_handle(self):
        """
        Returns the object handle.
        
        All nodes have some way of getting the object handle, whether it corresponds to an entity or some attribute of an entity.
        """

        return None


    def get_node_type(self, ncname=True):
        """
        Returns the node type. It should be a QName (potentially with a prefix), unless if 'ncname' parameter is set to True. 
        """

        return None


    def get_node_name(self):
        """
        Returns the node name. This may mean different things based on the exact node, but we'll just unite it under "name".
        """

        return None


    def get_node_namespace(self):
        """
        Returns the node namespace. Expected to be a URL.        
        """

        return None


    def get_node_documentation(self):
        """
        Returns the node documentation. Can be any content.  
        """

        return None


    def get_node_properties(self):
        """
        Returns a dictionary of given properties.        
        """

        return None


    def get_node_additionals(self):
        """
        Returns a dictionary of given additionals.        
        """

        return None


    def get_node_bounds(self):
        """
        Returns the bounds defined by the container. This will be used by the connection line to calculate its placement.        
        """

        return self.container.get_bounds()


    def get_input_connections(self):
        """
        Returns a list of input connections. This will be a new list, so the internal list cannot be modified.        
        """

        return [*self.input_connections.items()]


    def get_output_connections(self):
        """
        Returns a list of output connections. This will be a new list, so the internal list cannot be modified.        
        """

        return [*self.output_connections.items()]


    def get_input_port_names(self):
        """
        Returns a list of input port names. This will be a new list, so the internal list cannot be modified.        
        """

        return list(set(self.input_port_names))


    def get_output_port_names(self):
        """
        Returns a list of output port names. This will be a new list, so the internal list cannot be modified.           
        """

        return list(set(self.output_port_names))


    def get_specification(self):
        """
        Returns the specification of this node, or None if it doesn't exist.        
        """

        return object_manager.get_specification(
            pool_id       = self.canvas_context.pool_id,
            object_handle = self.get_object_handle()
        ).data



    # ┏━━━━━━━━━━━━━┓
    # ┃             ┃
    # ┃   Setters   ┃
    # ┃             ┃
    # ┗━━━━━━━━━━━━━┛

    def set_node_type(self, node_type):
        """
        Sets the node type. It should always be an NCName (without any prefix) 
        """

        pass


    def set_node_name(self, node_name):
        """
        Sets the node name. This may mean different things based on the exact node, but we'll just equate it under "name".
        """

        pass


    def set_node_namespace(self, node_namespace):
        """
        Sets the node namespace. Expected to be a URL.   
        """

        pass


    def set_node_documentation(self, node_documentation):
        """
        Sets the node documentation. Can be any content.  
        """

        pass


    def set_node_property(self, prop_name, prop_value):
        """
        Sets a property on this node.
        """

        pass


    def set_node_additional(self, addi_name, addi_value):
        """
        Sets an additional on this node.
        """

        pass


    # ┏━━━━━━━━━━━━━━━━━━━━┓
    # ┃                    ┃
    # ┃ Connection methods ┃
    # ┃                    ┃
    # ┗━━━━━━━━━━━━━━━━━━━━┛ 

    def connect_input(self, connection):
        """
        Puts the connection into the input connections dictionary. 
        Invoked by the connection itself (see `gproc.objects.connections.Connection`).
        """

        self.input_connections[connection] = ""


    def connect_output(self, connection):
        """
        Puts the connection into the output connections dictionary. 
        Invoked by the connection itself (see `gproc.objects.connections.Connection`).
        """

        self.output_connections[connection] = ""


    def disconnect_input(self, connection):
        """
        Removes the connection from the input connections dictionary.
        Invoked by the connection itself (see `gproc.objects.connections.Connection`).
        """

        if connection in self.input_connections.keys(): del self.input_connections[connection]


    def disconnect_output(self, connection):
        """
        Removes the connection from the output connections dictionary.
        Invoked by the connection itself (see `gproc.objects.connections.Connection`).
        """

        if connection in self.output_connections.keys(): del self.output_connections[connection]


    def assign_input_port_name(self, connection, new_port_name):
        """
        Update the input connections dictionary and notify the connection.
        Invoked by the configuration pane.
        """

        if connection in self.input_connections.keys():
            connection.set_to_port_name(new_port_name)
            self.input_connections[connection] = new_port_name


    def assign_output_port_name(self, connection, new_port_name):
        """
        Update the output connections dictionary and notify the connection.
        Invoked by the configuration pane.
        """

        if connection in self.output_connections.keys():
            connection.set_from_port_name(new_port_name)
            self.output_connections[connection] = new_port_name


    def notify_incoming_port_change(self, connection, old_port_name):
        """
        Invoked by the connection when the output port of the connection origin is changed
        """

        pass


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛ 

    def set_port_names_from_spec(self):
        """
        Sets the input and output port names based on this node's specification. Invoked by the children.
        """

        # Attempt to get the specification.
        object_handle   = self.get_object_handle() or ""
        get_spec_result = object_manager.get_specification(
            pool_id       = self.canvas_context.pool_id,
            object_handle = object_handle
        )

        if get_spec_result.data is None:
            self.input_port_names  = []
            self.output_port_names = []
        
        else:
            self.input_port_names  = [input_spec.port  for input_spec  in get_spec_result.data.inputs.values()]
            self.output_port_names = [output_spec.port for output_spec in get_spec_result.data.outputs.values()]

        
    def toggle_highlight(self, state):
        """
        Changes the outline of the container. Invoked by canvas.
        """

        if state:
            self.container.props.stroke_color_gdk_rgba = parse_rgba(theme["NODE_HIGHLIGHT_COLOUR"])
            self.container.props.line_width            = float(theme["NODE_LINE_WIDTH"]) + 1
        else:
            self.container.props.stroke_color_gdk_rgba = parse_rgba(theme["NODE_LINE_COLOUR"])
            self.container.props.line_width            = float(theme["NODE_LINE_WIDTH"])
            self.is_activated = False


    def activate(self):
        """
        Sets the activated flag to True.        
        """

        self.is_activated = True


    def update_text(self):
        """
        Default implementation for updating the content and position of the label.        
        """

        self.label.props.text = str(self)
        self.label.props.y    = self.container_height / 2


    def create_connection_points(self, allowed_orientations=[Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST], x_adj=0, y_adj=0):
        """
        Creates the connection points. A list of allowed orientations may be given to restrict the set of points.
        """

        if self.container is None: raise ValueError("The container for this node has not been initialised yet.")

        # Create all four points based on node bounds.
        bounds = self.get_node_bounds()
        radius = float(theme["CONN_POINT_DIAMETER"]) / 2
        nesw   = [
            (GooCanvas.CanvasRect(
                **self.connection_points_properties,
                parent = self,
                x      = (bounds.x2 - bounds.x1) / 2 - radius + x_adj,
                y      = -radius + y_adj
            ), Orientation.NORTH),
            (GooCanvas.CanvasRect(
                **self.connection_points_properties,
                parent = self,
                x      = (bounds.x2 - bounds.x1) - radius - 1 + x_adj,
                y      = (bounds.y2 - bounds.y1) / 2 - radius + y_adj
            ), Orientation.EAST),
            (GooCanvas.CanvasRect(
                **self.connection_points_properties,
                parent = self,
                x      = (bounds.x2 - bounds.x1) / 2 - radius + x_adj,
                y      = (bounds.y2 - bounds.y1) - radius - 1 + y_adj
            ), Orientation.SOUTH),
            (GooCanvas.CanvasRect(
                **self.connection_points_properties,
                parent = self,
                x      = -radius + x_adj,
                y      = (bounds.y2 - bounds.y1) / 2 - radius + y_adj
            ), Orientation.WEST),
        ]

        # Loop through each point, and add it to the list of connection points if it's allowed.
        for conn_point in nesw:
            if conn_point[1] in allowed_orientations:
                pointer_on_hover(conn_point[0])
                self.connection_points.append(conn_point[0]) # This makes it visible when hovered. Every point is created, but only some are visible.
                conn_point[0].orientation = conn_point[1] # IMPORTANT! This is used to find the connection point on canvas click.


    def find_connection_point(self, cursor_x, cursor_y):
        """
        Find the exact connection point from cursor click, and set the current orientation if found.
        Invoked by the canvas. 
        
        This method is used because finding the connection point from the canvas was tricky, but I think it could be refactored.
        """

        # Use GooCanvas.CanvasItem.get_items_at(...).
        cr    = self.canvas.create_cairo_context() # If a linter says self.canvas is an int, don't believe its lies.
        items = self.get_items_at(cursor_x, cursor_y, cr, True, True, [])

        # Look through all the items found on the cursor position.
        for item in items:
            if hasattr(item, "orientation"): 
                self.clear_orientation()
                self.connection_orientation = item.orientation
                item.props.stroke_color_gdk_rgba = parse_rgba(theme["CONN_POINT_HIGHLIGHT_COLOUR"])
                item.props.visibility            = GooCanvas.CanvasItemVisibility.VISIBLE
                break


    def clear_orientation(self):
        """
        Clears the orientation. Invoked by the canvas.        
        """

        for point in self.connection_points: 
            point.props.visibility            = GooCanvas.CanvasItemVisibility.INVISIBLE
            point.props.stroke_color_gdk_rgba = parse_rgba(theme["CONN_POINT_LINE_COLOUR"])
        self.connection_orientation = None


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_hover(self, ci, ti, e):
        """
        Make the connection points visible when the mouse hovers over the node.
        """

        if   e.type == Gdk.EventType.ENTER_NOTIFY:
            for point in self.connection_points:
                point.props.visibility = GooCanvas.CanvasItemVisibility.VISIBLE
        elif e.type == Gdk.EventType.LEAVE_NOTIFY:
            for point in self.connection_points:
                if point.orientation != self.connection_orientation: point.props.visibility = GooCanvas.CanvasItemVisibility.INVISIBLE
