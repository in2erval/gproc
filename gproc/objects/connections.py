"""
This module contains the connections class for maintaining the current set of connections.
"""

from gproc.objects.source   import GProcCanvasSource
from gproc.objects.steps    import GProcCanvasStep, GProcCanvasAtomicStep
from gproc.objects.io       import GProcCanvasInput, GProcCanvasOutput 
from gproc.objects.option   import GProcCanvasOption
from gproc.objects.opv      import GProcCanvasVariable, GProcCanvasWithBase
from gproc.objects.line     import GProcCanvasLine
from xproc import object_manager



class GProcConnections:
    """
    A class which takes on the responsibility of connecting nodes and maintaining the set of connections.    
    """

    def __init__(self, canvas):
        self.canvas = canvas
        self.data   = []


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛ 

    def connect(self, from_node, from_orientation, to_node, to_orientation):
        """
        Connects the two nodes based on orientation. 
        Returns True if it was successful, False if it was invalid, and None if the connection already exists and the orientation is not different.
        """

        # Helper function for validation.
        def any(*args):
            result = False
            for a in args: result = result or a
            return result
        
        # Find the existing connection, and update the orientation if found.
        from_node_connections = self.find_out_connections(from_node)
        to_node_connections   = self.find_in_connections(to_node)
        existing_connections  = list(filter((lambda connection: connection.to_node == to_node), from_node_connections))
        if len(existing_connections) > 0:
            connection = existing_connections[0]
            return connection.update_orientation(from_orientation, to_orientation) # This returns True if the orientations were different, and None if it was the same.

        # Check if connection is valid by listing all the invalid connections. If any of these are true, then the connection can't be created.
        is_same_node                     = from_node is to_node
        is_not_connectable               = not(from_node.is_connectable and to_node.is_connectable)
        is_to_source                     = isinstance(to_node, GProcCanvasSource)
        is_to_input                      = isinstance(to_node, GProcCanvasInput)
        is_to_option                     = isinstance(to_node, GProcCanvasOption)
        is_from_variable                 = isinstance(from_node, GProcCanvasVariable)
        is_from_output                   = isinstance(from_node, GProcCanvasOutput)
        is_from_step_to_variable         = isinstance(from_node, GProcCanvasStep)     and isinstance(to_node, GProcCanvasVariable)
        is_from_with_x_to_not_atom_step  = isinstance(from_node, GProcCanvasWithBase) and not isinstance(to_node, GProcCanvasAtomicStep)
        is_second_connection_to_variable = isinstance(to_node, GProcCanvasVariable)   and len(to_node_connections) > 0
        is_second_connection_to_with_x   = isinstance(to_node, GProcCanvasWithBase)   and len(to_node_connections) > 0
        is_second_connection_from_source = isinstance(from_node, GProcCanvasSource)   and len(from_node_connections) > 0
        if any(is_same_node, is_not_connectable, is_to_source, is_to_input, is_to_option,
            is_from_step_to_variable, is_from_with_x_to_not_atom_step,
            is_from_variable, is_from_output, is_second_connection_to_variable, 
            is_second_connection_to_with_x, is_second_connection_from_source):
            return False # Only return if the validation fails.

        # Create the new connection.
        new_connection = Connection(self.canvas, from_node, from_orientation, to_node, to_orientation)
        self.data.append(new_connection)
        return True


    def find_in_connections(self, node):
        """
        Finds all connections going into the given node.
        """

        return list(filter((lambda connection: connection.to_node == node), self.data))


    def find_out_connections(self, node):
        """
        Finds all connections originating from the given node.
        """
        return list(filter((lambda connection: connection.from_node == node), self.data))

    
    def find_all_connections(self, node):     
        """
        Finds all connections related to the given node.
        """   
        return list(filter((lambda connection: connection.from_node == node or connection.to_node == node), self.data))


    def remove_connection(self, from_node, to_node):
        """
        Removes the exact connection if it's found in the internal list.
        """

        for connection in [*self.data]:
            if connection.from_node == from_node and connection.to_node == to_node:
                connection.remove()
                self.data.remove(connection)
                return


    def remove_all_connections(self, node):
        """
        Removes all connections related to the given node.
        """

        for connection in [*self.data]:
            if (connection.from_node == node) or (connection.to_node == node):
                connection.remove()
                self.data.remove(connection)



class Connection:
    """
    A class which represents and maintains a single connection between two nodes.    
    """

    def __init__(self, canvas, from_node, from_orientation, to_node, to_orientation):
        # Class variables
        self.from_node = from_node
        self.to_node   = to_node
        self.from_port = None
        self.to_port   = None

        # The line.
        self.line = GProcCanvasLine(
            parent           = canvas.get_root_item(),
            canvas           = canvas,
            from_node        = from_node,
            from_orientation = from_orientation,
            to_node          = to_node,
            to_orientation   = to_orientation
        )

        # Notify the two nodes about this connection, and update the line.
        self.from_node.connect_output(self)
        self.to_node.connect_input(self)
        self.line.update()


    # ┏━━━━━━━━━━━━━━━━━━━┓
    # ┃                   ┃
    # ┃ Getters & Setters ┃
    # ┃                   ┃
    # ┗━━━━━━━━━━━━━━━━━━━┛ 

    def get_select(self):
        """
        Returns the 'select' value of the to-port.
        """

        if self.to_node is not None:
            object_handle = self.to_node.get_object_handle()
            get_select_result = object_manager.get_input_properties(
                pool_id = self.to_node.canvas_context.pool_id, 
                object_handle = object_handle,
                port_name = self.to_port
            )
            if get_select_result.exception is None:
                return get_select_result.data.get("select", "") # Return empty if the select has not been set yet.
            else: return None # Failed.


    def set_select(self, select):
        """
        Sets the 'select' value on the to-port.
        """

        if self.to_node is not None:
            object_handle = self.to_node.get_object_handle()
            set_select_result = object_manager.set_input_property(
                pool_id        = self.to_node.canvas_context.pool_id, 
                object_handle  = object_handle,
                port_name      = self.to_port,
                property_name  = "select",
                property_value = select
            ) 
            if set_select_result.exception is not None:
                pass # Report error?


    def set_from_port_name(self, port_name):
        """
        Sets the port name on the origin of the connection. If set to None, hides the label.

        This should be invoked by from_node.
        """

        # Set variable but keep the old.
        if port_name == "": port_name = None
        old_port_name = self.from_port
        self.from_port = port_name

        # Hard override on input nodes and source nodes to prevent displaying the label.
        if not isinstance(self.from_node, GProcCanvasInput) and not isinstance(self.from_node, GProcCanvasSource): 
            self.line.set_from_port_name(port_name)

        # The other node needs to be notified about the port change to update the in-memory repr.
        self.to_node.notify_incoming_port_change(self, old_port_name)


    def set_to_port_name(self, port_name):
        """
        Sets the port name on the destination of the connection. If set to None, hides the label.

        This should be invoked by to_node.
        """

        # Set variable and label.
        if port_name == "": port_name = None
        self.to_port = port_name
        self.line.set_to_port_name(port_name)


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Interfacing methods ┃ 
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛ 

    def update(self):
        """
        Exposes the line updating method.        
        """

        self.line.update()


    def update_orientation(self, from_orientation, to_orientation):
        """
        Check if the given orientation if different - returns True if it is, and None if it's the same.
        """

        if self.line.from_orientation == from_orientation and self.line.to_orientation == to_orientation: return None
        self.line.set_orientation(from_orientation, to_orientation)
        self.line.update()
        return True


    def remove(self):
        """
        Remove the connection by invoking the disconnect methods and removing the line.        
        """

        self.from_node.disconnect_output(self)
        self.to_node.disconnect_input(self)
        self.line.remove()
