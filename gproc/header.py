"""
This module contains the header.
"""

import gi, os, random
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf

from gproc.dialog    import GProcExportDialog
from gproc.publisher import publisher
from gproc.shared    import data



class GProcHeader(Gtk.HeaderBar):
    """
    A derived class which implements the main header. This will be used to house various main actions, such as creating new documents and exporting.

    See the docs for Gtk.HeaderBar: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/HeaderBar.html
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, show_close_button=True, **kwargs)

        # Left side.
        header_left_buttons = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        # New document button.
        self.new_button = Gtk.Button()
        self.new_button.set_image(Gtk.Image.new_from_icon_name("document-new", Gtk.IconSize.BUTTON))
        self.new_button.set_tooltip_text("New XProc document") #TRANSLATE#
        self.new_button.connect("clicked", self.__event_new_button_click)

        # Set up popover for the new document creation.
        self.new_popover = Gtk.Popover(position=Gtk.PositionType.BOTTOM)
        new_type_buttons = [
            (Gtk.Button(
                relief = Gtk.ReliefStyle.NONE,
                image  = Gtk.Image.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    filename = os.path.join(data.images_dir, "icon_pipeline.svg"), 
                    width    = 100,
                    height   = -1,
                    preserve_aspect_ratio = True
                ))
            ), "p:pipeline"),
            (Gtk.Button(
                relief = Gtk.ReliefStyle.NONE,
                image  = Gtk.Image.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    filename = os.path.join(data.images_dir, "icon_declare-step.svg"), 
                    width    = 100,
                    height   = -1,
                    preserve_aspect_ratio = True
                ))
            ), "p:declare-step"),
            (Gtk.Button(
                relief = Gtk.ReliefStyle.NONE,
                image  = Gtk.Image.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    filename = os.path.join(data.images_dir, "icon_library.svg"), 
                    width    = 100,
                    height   = -1,
                    preserve_aspect_ratio = True
                ))
            ), "p:library")
        ]
        new_grid = Gtk.Grid(column_spacing=10, row_spacing=10)
        for i, button_desc in enumerate(new_type_buttons):
            label = Gtk.Label(button_desc[1])
            new_grid.attach(button_desc[0], i, 0, 1, 1)
            new_grid.attach(label,          i, 1, 1, 1)
            button_desc[0].connect("clicked", self.__event_new_type_button_click, button_desc[1])
        self.new_popover.add(new_grid)      

        # Save workspace button.
        self.save_button = Gtk.Button()
        self.save_button.set_image(Gtk.Image.new_from_icon_name("document-save", Gtk.IconSize.BUTTON))
        self.save_button.set_tooltip_text("Save the current document") #TRANSLATE#
        self.save_button.connect("clicked", self.__event_save_button_click)
        self.save_button.set_sensitive(False)

        # Export document button.
        self.export_button = Gtk.Button()
        self.export_button.set_image(Gtk.Image.new_from_icon_name("document-send", Gtk.IconSize.BUTTON))
        self.export_button.set_tooltip_text("Export as an XProc file") #TRANSLATE#
        self.export_button.connect("clicked", self.__event_export_button_click)
        self.export_button.set_sensitive(False)

        # Pack them into the box.
        header_left_buttons.add(self.new_button)
        header_left_buttons.add(self.save_button)
        header_left_buttons.add(self.export_button)
        self.pack_start(header_left_buttons)

        # Right side.
        header_right_buttons = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        # Open settings button.
        self.settings_button = Gtk.Button()
        self.settings_button.set_image(Gtk.Image.new_from_icon_name("document-properties", Gtk.IconSize.BUTTON))
        self.settings_button.set_tooltip_text("Settings") #TRANSLATE#
        self.settings_button.connect("clicked", self.__event_settings_button_click)

        # Open help topic button.
        self.help_button = Gtk.Button()
        self.help_button.set_image(Gtk.Image.new_from_icon_name("help-about", Gtk.IconSize.BUTTON))
        self.help_button.set_tooltip_text("Help") #TRANSLATE#
        self.help_button.connect("clicked", self.__event_help_button_click)

        # Pack them into the box.
        header_right_buttons.add(self.settings_button)
        header_right_buttons.add(self.help_button)
        self.pack_end(header_right_buttons)

        # Connect publisher signal.
        publisher.connect("set_exportable", self.__event_exportable_change)


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_new_button_click(self, widget):
        """
        Reveal the popover relative to the new document button.
        """

        self.new_popover.set_relative_to(widget)
        self.new_popover.show_all()
        self.new_popover.popup()


    def __event_new_type_button_click(self, widget, doc_type):
        """
        Notify through the publisher to create a new document.
        """

        ent_class = {"p:pipeline": "declaration", "p:declare-step": "declaration","p:library": "library"}[doc_type]
        publisher.new_document(self, ent_class, doc_type.split(':')[-1])


    def __event_save_button_click(self, widget):
        """
        Save the current workspace as a file.
        """

        # TODO: Implement saving workspaces.
        pass


    def __event_export_button_click(self, widget):
        """
        Open the save file dialog for export, and notify to export the current document.
        """

        dialog = GProcExportDialog(window=self.get_parent())
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            filename = dialog.get_filename()
            filename += "" if filename[-4:] == ".xpl" else ".xpl"
            publisher.invoke_export(self, filename)
        dialog.destroy()


    def __event_settings_button_click(self, widget):
        """
        Open the settings window.
        """

        # TODO: Implement settings window.
        pass


    def __event_help_button_click(self, widget):
        """
        Open the help window.
        """

        # TODO: Implement help window. Right now this just tests the infobar messaging mechanism.
        publisher.infobar_message(self, "This is a test: {}".format(random.randint(0,100)), random.randint(0,3))


    def __event_exportable_change(self, caller, is_exportable):
        """
        Enable or disable the export document button.
        """

        self.export_button.set_sensitive(is_exportable)
        self.save_button.set_sensitive(is_exportable)