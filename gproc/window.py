"""
This module contains the main GProc window.
"""

import gi, os
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

from gproc.header    import GProcHeader
from gproc.infobar   import GProcInfoBar
from gproc.publisher import publisher 
from gproc.shared    import data
from gproc.sidebar   import GProcConfigPane
from gproc.workspace import GProcWorkspaceTabs



class GProcMainWindow(Gtk.ApplicationWindow):
    """
    A derived class which implements the main GProc window. 

    See the docs for Gtk.ApplicationWindow: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/ApplicationWindow.html
    """

    def __init__(self, *args, version, **kwargs):
        super().__init__(*args, **kwargs)

        # Class constants
        self.TITLE_BASE               = "GProc"
        self.DEFAULT_WIDTH            = 1200
        self.DEFAULT_HEIGHT           = 700
        self.DEFAULT_CONFIGS_WIDTH = 300

        # Working variables - these are used internally.
        self.__held_key  = None    # Keyboard key value
        self.__version   = version # Version string
        self.__prev_size = (0,0)   # Size of window

        # Misc variables for setup
        views_dir  = data.views_dir 
        images_dir = data.images_dir

        # Import GTK objects from the glade files
        builders = []
        for glade_file in sorted(os.listdir(views_dir)):
            # Find relevant files by convention.
            if glade_file.startswith("properties_") and os.path.splitext(glade_file)[1] == ".glade": 
                builder = Gtk.Builder()
                builder.add_from_file(os.path.join(views_dir, glade_file))
                builders.append(builder)

        # We now programmatically construct the window widgets.

        # Main box, splits the view sideways into two - configuration pane and workspace tabs.
        box_main = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL
        )

        # Configuration pane.
        self.config_pane = GProcConfigPane(
            bar_width  = self.DEFAULT_CONFIGS_WIDTH,
            builders   = builders
        )
        box_main.add(self.config_pane)
        box_main.set_child_packing(self.config_pane, expand=False, fill=True, padding=0, pack_type=Gtk.PackType.START)

        # Workspace tabs.
        self.workspace_tabs = GProcWorkspaceTabs()
        box_main.add(self.workspace_tabs)
        box_main.set_child_packing(self.workspace_tabs, expand=True, fill=True, padding=0, pack_type=Gtk.PackType.END)

        # The header, which contains action icons such as create new document, and export current document.
        header = GProcHeader(
            title = "{} - v{}".format(self.TITLE_BASE, self.__version) # Title of the window, which appears on the header.
        )
        self.set_titlebar(header) 

        # The infobar, which can be made visible by all components of GProc to display messages.
        info_bar = GProcInfoBar()

        # The overlay to combine the infobar and the main box. This will be the root widget of the window components.
        overlay = Gtk.Overlay()
        overlay.add(box_main)
        overlay.add_overlay(info_bar)
        self.add(overlay)

        # Connect GTK signals. 
        self.connect("key-press-event",   self.__event_key_change)
        self.connect("key-release-event", self.__event_key_change)
        self.connect("focus-out-event",   self.__event_key_change)
        self.connect("size-allocate",     self.__event_window_size_change)

        # General properties of self.
        self.set_size_request(self.DEFAULT_WIDTH, self.DEFAULT_HEIGHT) 
        self.set_icon_from_file(os.path.join(images_dir, "icon_gproc.svg"))

        # Reveal all widgets except for the infobar.
        self.show_all()
        info_bar.hide()


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_key_change(self, window, event):
        """
        Keep track of currently pressed key - broadcasted via the publisher. Invoked by GTK signal.
        """

        self.__held_key = event.keyval if isinstance(event, Gdk.EventKey) and (event.type is Gdk.EventType.KEY_PRESS) else None
        publisher.set_held_key(self, self.__held_key)


    def __event_window_size_change(self, window, alloc):
        """
        Notify when the window size changes - broadcasted via the publisher. Invoked by GTK signal.
        """

        w, h = self.__prev_size
        if w != alloc.width or h != alloc.height:
            self.__prev_size = (alloc.width, alloc.height)
            publisher.update_bounds(self, self.__prev_size)