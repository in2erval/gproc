"""
This module contains the method to create new popovers for the floating buttons on top of canvas pages.
"""

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango

from gproc.utils.enums import Environment



class GProcNodeButton(Gtk.ModelButton):
    """
    A derived class which implements a single node creation button. Holds values for the step class and type.

    See the docs for Gtk.ModelButton: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/ModelButton.html
    """

    def __init__(self, *args, node_class, node_type, node_ns=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.node_class = node_class
        self.node_type  = node_type
        self.node_ns    = node_ns


def __separator():
    """
    Creates a separator for the button list.
    """

    # For some reason, Gtk.Separator doesn't work. So here's a Gtk.Frame that acts like one.
    frame = Gtk.Frame()
    frame.set_size_request(-1, 1)
    return frame


def __label(text):
    """
    Creates a text label for the button list.
    """
    
    new_label = Gtk.Label(text, halign=Gtk.Align.START)
    new_label.get_style_context().add_class("popover-label")
    return new_label


def __generate_models(environment):
    """
    Creates buttons based on environment. Different environments have different nodes which can be inserted.
    """

    if environment == Environment.LIBRARY: # p:library
        return {
            "Declarations": [
                __label("Step Declarations"),
                GProcNodeButton(node_class="declaration", node_type="declare-step", text="declare-step"),
                GProcNodeButton(node_class="declaration", node_type="pipeline",     text="pipeline"),
                __separator(),
                __label("Imports"),
                GProcNodeButton(node_class="import", node_type="import", text="import")
            ]
        }

    elif environment == Environment.SUBPIPELINE: # Compound steps and other subpipelines
        return {
            "Singular": [
                __label("Atomic Steps"),
                GProcNodeButton(node_class="atomic", node_type="identity", text="New atomic step"),
                __separator(),        
                __label("Sources"),
                GProcNodeButton(node_class="source", node_type="inline",   text="inline"),
                GProcNodeButton(node_class="source", node_type="document", text="document"),
                GProcNodeButton(node_class="source", node_type="data",     text="data"),
                GProcNodeButton(node_class="source", node_type="empty",    text="empty")
            ],
            "Subpipelines": [
                __label("Compound Steps"),
                GProcNodeButton(node_class="compound", node_type="for-each", text="for-each"),
                GProcNodeButton(node_class="compound", node_type="viewport", text="viewport"),
                GProcNodeButton(node_class="compound", node_type="group",    text="group"),
                __separator(),
                __label("Multicontainer Steps"),
                GProcNodeButton(node_class="multicontainer", node_type="choose", text="choose"),
                GProcNodeButton(node_class="multicontainer", node_type="try",    text="try")
            ],
            "Declarations and User Values": [
                __label("Declarations"),
                GProcNodeButton(node_class="output", node_type="output", text="output"),
                __separator(),
                __label("User Values"),
                GProcNodeButton(node_class="with-option", node_type="with-option", text="with-option"), 
                GProcNodeButton(node_class="with-param",  node_type="with-param",  text="with-param"), 
                GProcNodeButton(node_class="variable",    node_type="variable",    text="variable")
            ]
        }

    else: # p:pipeline, p:declare-step
        return {
            "Singular": [
                __label("Atomic Steps"),
                GProcNodeButton(node_class="atomic", node_type="identity", text="New atomic step"),
                __separator(),        
                __label("Sources"),
                GProcNodeButton(node_class="source", node_type="inline",   text="inline"),
                GProcNodeButton(node_class="source", node_type="document", text="document"),
                GProcNodeButton(node_class="source", node_type="data",     text="data"),
                GProcNodeButton(node_class="source", node_type="empty",    text="empty")
            ],
            "Subpipelines": [
                __label("Step Declarations"),
                GProcNodeButton(node_class="declaration", node_type="declare-step", text="declare-step"),
                GProcNodeButton(node_class="declaration", node_type="pipeline",     text="pipeline"),
                __separator(),
                __label("Compound Steps"),
                GProcNodeButton(node_class="compound", node_type="for-each", text="for-each"),
                GProcNodeButton(node_class="compound", node_type="viewport", text="viewport"),
                GProcNodeButton(node_class="compound", node_type="group",    text="group"),
                __separator(),
                __label("Multicontainer Steps"),
                GProcNodeButton(node_class="multicontainer", node_type="choose", text="choose"),
                GProcNodeButton(node_class="multicontainer", node_type="try",    text="try")
            ],
            "Declarations and User Values": [
                __label("Declarations"),
                GProcNodeButton(node_class="input",  node_type="input",  text="input"),
                GProcNodeButton(node_class="output", node_type="output", text="output"),
                GProcNodeButton(node_class="option", node_type="option", text="option"),
                __separator(),
                __label("Imports"),
                GProcNodeButton(node_class="import", node_type="import", text="import"),
                __separator(),
                __label("User Values"),
                GProcNodeButton(node_class="with-option", node_type="with-option", text="with-option"), 
                GProcNodeButton(node_class="with-param",  node_type="with-param",  text="with-param"), 
                GProcNodeButton(node_class="variable",    node_type="variable",    text="variable")
            ]
        }
    

def create_popover(node_creation_callback, environment):
    """
    Creates a Gtk.Popover for the given environment. The node creation callback should be a function to invoke when the button is clicked, so that the node is created in the appropriate canvas.
    """

    # Create widgets.
    popover = Gtk.Popover(position=Gtk.PositionType.TOP)
    stack   = Gtk.Stack()
    items   = __generate_models(environment)

    # Populate each category as a stack page.
    for key, item_list in items.items():
        popover_buttons = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
        stack.add_titled(popover_buttons, key, key)
        for item in item_list:
            if isinstance(item, GProcNodeButton):
                item.connect("clicked", (lambda b: node_creation_callback(b.node_class, b.node_type)))
            popover_buttons.add(item)

    # Create a switcher to switch stack pages.
    switcher = Gtk.StackSwitcher()
    switcher.set_stack(stack)

    # Position the switcher and the stack vertically.
    box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
    box.add(switcher)
    box.add(stack)
    popover.add(box) 

    return popover