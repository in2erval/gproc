"""
This module contains the infobar.
"""

import gi, os
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from gproc.publisher import publisher



class GProcInfoBar(Gtk.InfoBar):
    """
    A derived class which implements the info bar. This will act as a way to relay messages to the user without creating any modal dialogs.

    See the docs for Gtk.InfoBar: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/InfoBar.html
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Have a reference to the label so the message can be modified.
        self.info_message = Gtk.Label()

        # Self setup.
        self.set_message_type(Gtk.MessageType.WARNING)
        self.set_valign(Gtk.Align.START)
        self.set_show_close_button(True)
        self.set_revealed(False)
        self.get_content_area().add(self.info_message)

        # Connect GTK and publisher signals.
        self.connect("response", self.__event_infobar_close)
        publisher.connect("infobar_message", self.__event_infobar_message_change)


    def __event_infobar_message_change(self, caller, message, message_type):
        """
        Change the message content and type, and reveal the infobar.
        """

        self.info_message.set_text(message)
        self.set_message_type(message_type)
        self.show()
        self.set_revealed(True)


    def __event_infobar_close(self, widget, response):
        """
        Hide the infobar.
        """

        self.hide()
        self.set_revealed(False)

