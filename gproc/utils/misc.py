"""
This module contains miscellaneous classes and functions used in this package for ease of use.
"""

import gi, re, traceback, validators
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import GooCanvas, Gdk

XPROC_NAMESPACE = "http://www.w3.org/ns/xproc"


def parse_rgba(colour_string):
    """
    Passes the given string into Gdk's RGBA pasing method. Returns a new Gdk.RGBA struct.
    """

    colour = Gdk.RGBA()
    if colour.parse(colour_string): return colour
    return None


def pointer_on_hover(canvas_element):
    """
    Creates a signal on the canvas element so that when the mouse hovers over, it becomes a pointer.
    """
    
    def __event_hover(ci, ti, e):
        if   e.type == Gdk.EventType.ENTER_NOTIFY:
            e.window.set_cursor(Gdk.Cursor.new_from_name(e.window.get_display(), "pointer"))
        elif e.type == Gdk.EventType.LEAVE_NOTIFY:
            e.window.set_cursor(None)
    canvas_element.connect("enter-notify-event", __event_hover)
    canvas_element.connect("leave-notify-event", __event_hover)


def str_none_or_empty(string):
    """
    Checks whether a string is None or empty.
    """

    return (string is None) or (string == "")


def is_qname(name_string):
    """
    Uses regex to match a proper qualified name for XML. Returns a tuple with [0] as the boolean, [1] as the prefix, and [2] as the name.

    Examples of accepted strings: `pipeline`, `validate-with-xml-schema`, `p:xslt`, `html:div`.
    
    Exampels of rejected strings: `1last`, `p:`, `xml-node`, `xml:node`.
    """

    # Regex with negative look-ahead (names and prefixes cannot start with "xml") and valid character sets.
    qname_regex = re.compile(r"^(?:(?!xml)([\w][\w\-\.]*)\:)?(?!xml)([\w][\w\-\.]*)$")
    qname_match = qname_regex.match((name_string or "").lower())
    if qname_match is None: return (False, None)
    else: return (True, qname_match.group(1), qname_match.group(2))


def is_ncname(name_string):
    """
    Uses regex to match a non-colonised name for XML. Returns a tuple with [0] as the boolean, and [1] as the name.

    Examples of accepted strings: `pipeline`, `validate-with-xml-schema`, `node_name`.
    
    Exampels of rejected strings: `1last`, `p:filter`, `xmlans`, `[quote here]`.
    """

    # Regex is similar to is_qname, but without the first part.
    ncname_regex = re.compile(r"^(?!xml)([\w][\w\-\.]*)$")
    ncname_match = ncname_regex.match((name_string or "").lower())
    if ncname_match is None: return (False, None)
    else: return (True, ncname_match.group(1))


def is_url(url_string):
    """
    Uses the validator package to check for proper URL.
    """

    return validators.url(url_string)


def print_exception(exc):
    """
    Simple method to print the stack trace and the passed in exception.
    """

    traceback.print_stack()
    print(str(exc))