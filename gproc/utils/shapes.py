"""
This module contains the class for creating GooCanvas.CanvasPolyline shapes.
"""

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GooCanvas", "2.0")
from gi.repository import GooCanvas



class Shapes:
    """
    A simple collection of static methods to generate the GooCanvas.CanvasPoints struct.
    """

    @staticmethod
    def octagon(width, height, chamfer):
        points = GooCanvas.CanvasPoints.new(8)
        for i, point in enumerate([ # Octagonal shape.
            (chamfer, 0), (width-chamfer, 0),
            (width, chamfer), (width, height-chamfer),
            (width-chamfer, height), (chamfer, height),
            (0, height-chamfer), (0, chamfer)
        ]): 
            points.set_point(i, point[0], point[1])
        return points

    
    @staticmethod
    def hexagon(width, height):
        points   = GooCanvas.CanvasPoints.new(6)
        min_half = min(width/2, height/2)
        for i, point in enumerate([
            (0, min_half), (min_half, 0),
            (width-min_half, 0), (width, min_half),
            (width-min_half, height), (min_half, height)
        ]):
            points.set_point(i, point[0], point[1])
        return points

    
    @staticmethod
    def bracket(width, height):
        points = GooCanvas.CanvasPoints.new(7)
        for i, point in enumerate([
            (width, 0), (width/2, 0),
            (width/2, height/2 - 5), (0, height/2), (width/2, height/2 + 5),
            (width/2, height), (width, height)
        ]):
            points.set_point(i, point[0], point[1])
        return points


    @staticmethod
    def diamond(width, height):
        points = GooCanvas.CanvasPoints.new(4)
        for i, point in enumerate([
            (width/2, 0),  (width, height/2),
            (width/2, height), (0, height/2)
        ]):
            points.set_point(i, point[0], point[1])
        return points


    @staticmethod
    def right_trapezoid(width, height, chamfer):
        points = GooCanvas.CanvasPoints.new(5)
        for i, point in enumerate([
            (0, 0), (width - chamfer, 0),
            (width, chamfer), (width, height),
            (0, height)
        ]):
            points.set_point(i, point[0], point[1])
        return points

    @staticmethod
    def parallelogram(width, height, adjacent):
        points = GooCanvas.CanvasPoints.new(4)
        for i, point in enumerate([
            (0, 0), (adjacent, height), 
            (width, height), (width-adjacent, 0)
        ]):
            points.set_point(i, point[0], point[1])
        return points
