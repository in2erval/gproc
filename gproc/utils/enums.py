"""
This module defines all enums used in this package.
"""

from enum import Enum



class AggregateStepType(Enum):
    """
    An enum for subpipeline container step types. All compound types are grouped into one as the behaviour of SubpipelineComponent does not depend on it.
    """

    COMPOUND    = 0
    CHOOSE      = 1
    TRY         = 2
    DECLARATION = 3
    
    def node_class(self):
        """
        Returns the class type of the enum's value.
        """

        class_map = {
            0: "compound",
            1: "subpipeline",
            2: "subpipeline",
            3: "declaration"
        }
        return class_map[self.value]



class Orientation(Enum):
    """
    An enum for determining the connection point on a node.
    """

    NORTH = 0
    EAST  = 1
    SOUTH = 2
    WEST  = 3


class Environment(Enum):
    """
    An enum for the canvas environment. Different environments have different properties, e.g. nodes that can be inserted.
    """

    DECLARATION = 0
    SUBPIPELINE = 1
    LIBRARY     = 2


    @staticmethod
    def get_value(step_type):
        """
        Returns the environment corresponding to the given step type. The step type should be an NCName (without prefix 'p:')
        """

        if step_type in ["declare-step", "pipeline"]: return Environment.DECLARATION
        if step_type == "library": return Environment.LIBRARY
        return Environment.SUBPIPELINE

