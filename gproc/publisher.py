"""
This module contains the publisher for inter-component communication.
"""

import re, functools



class Publisher:
    """
    The publisher allows communication between components without having any reference between each other.

    To subscribe to a signal, use `publisher.connect(signal, callback)`. If the signal does not exist, it will
    create a callable method on the publisher using the signal name. 
    The signal name has to start with an alphabet character, and only contain alphanumeric characters or underscores. 
    The callback has to take in two parameters, `caller` and `event_args`.

    To invoke a signal to all subscribers, call the signal name on the publisher as if it were a method, e.g.
    `publisher.signal_name(self, data)`. If the signal has not been created yet, nothing will happen.
    """
    def __init__(self):
        self.__subscription = {}


    def __getattr__(self, name):
        """
        Override the default attribute accessor to swallow AttributeError if no signal is present.
        """

        if name in self.__dict__.keys():
            return self.__dict__[name]
        else:
            # Return dummy function.
            def potential_signal(*args):
                pass
            return potential_signal


    def connect(self, signal, callback):
        """
        Subscribe to the publisher, and be notified when the signal is fired via `publisher.signal_name(...)`.
        
        :param signal: The signal name to subscribe to. 
        :type signal: `str`
        :param callback: Callback function to invoke when the signal is fired. Signature is `signal(caller, *event_args)`
        :type callback: `callable`
        """

        # Sanitise signal name
        if not re.match("^[A-Za-z][\\w]*$", signal): raise ValueError("signal name is invalid.")
        if signal == "connect": raise ValueError("signal name cannot be called 'connect' since that's a special method.")
        
        # Create new signal by adding to the internal memory as a list, and to the publisher as an instance method.
        if signal not in self.__subscription.keys():
            self.__subscription[signal] = []
            def response(caller, *event_args):
                for subscriber in self.__subscription[signal]:
                    subscriber(caller, *event_args)
            setattr(self, signal, response)

        # List of callbacks for the signal.
        self.__subscription[signal].append(callback)



class InternalError(Exception):
    pass



publisher = Publisher()