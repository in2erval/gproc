"""
This module contains various shared data for GProc, e.g. script directories and configuration values.
"""

import os, sys

from configparser import ConfigParser



class __GProcSharedData:
    # Private vars
    
    __script_dir  = os.path.join(sys._MEIPASS, "gproc") if getattr(sys, "frozen", False) else os.path.dirname(os.path.realpath(__file__))

    # Public vars
    views_dir   = os.path.join(__script_dir, "views/")
    images_dir  = os.path.join(__script_dir, "images/")
    config_file = os.path.join(__script_dir, "config.ini") 



data = __GProcSharedData()

config = ConfigParser()
config.read(data.config_file)

theme = config["Theme: {}".format(config.get("Settings", "CURRENT_THEME"))]