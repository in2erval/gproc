"""
This module contains various dialogs used in GProc.
"""

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk



class GProcExportDialog(Gtk.FileChooserDialog):
    """
    A derived class which implements the export dialog. The user will be able to choose where to save the document as an XProc file.

    See the docs for Gtk.FileChooserDialog: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/FileChooserDialog.html
    """

    def __init__(self, *args, window, **kwargs):
        super().__init__(
            *args, 
            title   = "Export XProc file",
            parent  = window,
            action  = Gtk.FileChooserAction.SAVE, 
            buttons = ("Cancel", Gtk.ResponseType.CANCEL, "Export", Gtk.ResponseType.OK), 
            **kwargs
        )

        xpl_filter = Gtk.FileFilter()
        xpl_filter.set_name("XProc files")
        xpl_filter.add_pattern("*.xpl")
        self.add_filter(xpl_filter)
