"""
This module contains the configuration pane class for configuring selected nodes. This sidebar will appear on the left side of the main window.
"""

import gi, os
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, GObject

from gproc.publisher  import publisher
from gproc.shared     import data
from gproc.utils.misc import str_none_or_empty
from xproc            import object_manager



class GProcConfigPane(Gtk.Notebook):
    """
    A derived class which implements the configuration pane. 
    
    This will be expecting a set of widget builders to be passed in for each "properties_[---].glade" found.
    For each one, this will create a new tab of configurations.
    In normal operation, this class will be listening for selected node changes, and populates the widgets based on values.

    See the docs for Gtk.Notebook: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/Notebook.html
    """

    def __init__(self, *args, bar_width, builders, **kwargs):
        super().__init__(*args, can_focus=False, margin_top=5, tab_pos=Gtk.PositionType.LEFT, **kwargs)

        # Class variables
        self.config_widgets = {} # Dictionary of widgets on the pane.
        self.icons          = {} # Dictionary of GdkPixbuf.Pixbuf for each 'icon' found in the images directory.
        self.stores         = {} # Dictionary of ListStore-s being used in different widgets.
        self.current_node   = None

        # Predefine the filter function before the for-loop below.
        def find_prop(obj):
            # Returns True if the name of the given builder object starts with 'prop', False otherwise or if get_name() is not defined.
            get_name = getattr(obj, "get_name", None)
            if not get_name: return False
            else: return obj.get_name().startswith("prop_")

        # From a list of builders, create notebook pages corresponding to the properties collections.
        for builder in builders:
            # Find all prop_... objects, and union it with the current properties widgets dictionary.
            config_widgets_list = list(filter(find_prop, builder.get_objects()))
            self.config_widgets = dict(self.config_widgets, **dict(map((lambda obj: (obj.get_name()[5:], obj)), config_widgets_list)))

            # Set up objects for the page.
            box_main = builder.get_object("box_main")
            box_main.connect("set-focus-child", lambda a,b: publisher.focus_change(self)) # Part of workspace's HACK.

            tab_label = Gtk.Label(label=box_main.get_name(), angle=90)

            gproc_image = Gtk.Image.new_from_pixbuf( # This is what will be displayed when no nodes are selected.
                GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    filename=os.path.join(data.images_dir, "logo_gproc.svg"),
                    width=bar_width - 70,
                    height=-1,
                    preserve_aspect_ratio=True
                )
            )
            gproc_image.set_halign(Gtk.Align.CENTER)
            gproc_image.set_valign(Gtk.Align.CENTER)
            gproc_image.get_style_context().add_class("fade")
            gproc_image.set_state_flags(Gtk.StateFlags.ACTIVE, True)

            # Create page. Also add references for easy access.
            overlay = Gtk.Overlay()
            overlay.add_overlay(gproc_image)
            overlay.add(box_main)
            overlay.child_set_property(gproc_image, "pass-through", True)
            overlay.inner = box_main
            overlay.image = gproc_image

            # Create and configure page properties.
            self.append_page(overlay, tab_label)
            self.child_set_property(overlay, "tab-expand", True)
            self.child_set_property(overlay, "reorderable", True)

        # Connect publisher signals, and carry out miscellaneous setups.
        publisher.connect("set_current_node",     self.__event_current_node_change)
        publisher.connect("get_input_port_names", self.__event_get_input_port_names)

        self.connect("focus-tab", lambda a,b: publisher.focus_change(self))
        self.__setup_widgets()
        self.__setup_icon_pixbufs(data.images_dir)        
        self.set_size_request(bar_width, -1)


    # ┏━━━━━━━━━━━━━━━┓
    # ┃               ┃
    # ┃ Setup methods ┃
    # ┃               ┃
    # ┗━━━━━━━━━━━━━━━┛ 

    def __setup_widgets(self):
        """
        Initialises config widgets and sets up events based on signals.        
        """

        # General widgets
        self.config_widgets["step-type"].get_child().connect("changed", self.__event_type_cmb_change)         # Expected: Gtk.ComboBoxText
        self.config_widgets["name"].connect("changed", self.__event_name_entry_change)                        # Expected: Gtk.Entry
        self.config_widgets["documentation"].get_buffer().connect("changed", self.__event_docs_buffer_change) # Expected: Gtk.TextView inside a Gtk.ScrolledWindow
        self.config_widgets["add-nsbinding"].connect("clicked", self.__event_add_nsbinding_button_click)      # Expected: Gtk.Button

        # TreeView widgets

        # Inputs, with three columns: "Node" (Label), "Port" (ComboBox with or without Entry), "Select" (Entry)
        self.stores["inputs_liststore"] = Gtk.ListStore(str, str, str, GObject.TYPE_PYOBJECT) # The storage of the actual data. The last one is for the connection object.
        self.stores["input_port_store"] = Gtk.ListStore(str) # The storage of input port names, for the Port column.

        inputs_port_renderer   = Gtk.CellRendererCombo(editable=True, model=self.stores["input_port_store"], text_column=0, has_entry=True)
        inputs_select_renderer = Gtk.CellRendererText(editable=True)
        inputs_port_renderer.connect("edited", self.__event_input_port_change)
        inputs_select_renderer.connect("edited", self.__event_input_select_change)
        self.inputs_port_renderer   = inputs_port_renderer # Have a reference to this so that the Entry can be disabled later.
        self.inputs_select_renderer = inputs_select_renderer # Same with this.

        inputs_node_column   = Gtk.TreeViewColumn(title="Node", cell_renderer=Gtk.CellRendererText(), text=0)
        inputs_port_column   = Gtk.TreeViewColumn(title="Port", cell_renderer=inputs_port_renderer, text=1)
        inputs_select_column = Gtk.TreeViewColumn(title="Select", cell_renderer=inputs_select_renderer, text=2)
        inputs_node_column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        inputs_port_column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        inputs_select_column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)

        self.config_widgets["inputs"].set_model(self.stores["inputs_liststore"])
        self.config_widgets["inputs"].append_column(inputs_node_column)
        self.config_widgets["inputs"].append_column(inputs_port_column)
        self.config_widgets["inputs"].append_column(inputs_select_column)

        # Outputs, with two columns: "Node" (Label), "Port" (ComboBox with or without Entry)
        self.stores["outputs_liststore"] = Gtk.ListStore(str, str, GObject.TYPE_PYOBJECT) # The storage of the actual data. The last one is for connection object.
        self.stores["output_port_store"] = Gtk.ListStore(str) # The storage of output port names, for the Port column.

        outputs_port_renderer = Gtk.CellRendererCombo(editable=True, model=self.stores["output_port_store"], text_column=0, has_entry=True)
        outputs_port_renderer.connect("edited", self.__event_output_port_change)
        self.outputs_port_renderer = outputs_port_renderer # Have a reference to this so that the Entry can be disabled later.

        outputs_node_column = Gtk.TreeViewColumn(title="Node", cell_renderer=Gtk.CellRendererText(), text=0)
        outputs_port_column = Gtk.TreeViewColumn(title="Port", cell_renderer=outputs_port_renderer, text=1)

        self.config_widgets["outputs"].set_model(self.stores["outputs_liststore"])
        self.config_widgets["outputs"].append_column(outputs_node_column)
        self.config_widgets["outputs"].append_column(outputs_port_column)

        # Properties, with one flag and two columns: "Required" (Pixbuf), "Name" (Label), "Value" (Entry)
        self.stores["properties_liststore"] = Gtk.ListStore(GdkPixbuf.Pixbuf, str, str) # The storage of the actual data.

        properties_value_renderer = Gtk.CellRendererText(editable=True)
        properties_value_renderer.connect("edited", self.__event_property_value_change)
        self.properties_value_renderer = properties_value_renderer # Have a reference to this so the 'editable' property later.
                
        properties_flag_column  = Gtk.TreeViewColumn(title="", cell_renderer=Gtk.CellRendererPixbuf(), pixbuf=0)
        properties_name_column  = Gtk.TreeViewColumn(title="Name", cell_renderer=Gtk.CellRendererText(), text=1)
        properties_value_column = Gtk.TreeViewColumn(title="Value", cell_renderer=properties_value_renderer, text=2)
        properties_name_column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        properties_value_column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)

        self.config_widgets["properties"].set_model(self.stores["properties_liststore"])
        self.config_widgets["properties"].append_column(properties_flag_column)
        self.config_widgets["properties"].append_column(properties_name_column)
        self.config_widgets["properties"].append_column(properties_value_column)

        # TODO: Implement namespace bindings.


    def __setup_icon_pixbufs(self, images_dir):
        """
        Look through the images folder and create Pixbufs for each icon.
        """

        for filename in os.listdir(images_dir):
            # Find icon by convention.
            if filename.startswith("icon_"):
                icon_name = os.path.splitext(filename)[0][5:]
                self.icons[icon_name] = GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    os.path.join(images_dir, filename),
                    16,
                    16,
                    True
                )

    
    # ┏━━━━━━━━━━━━━━━━━━━━┓
    # ┃                    ┃
    # ┃ Overridden methods ┃
    # ┃                    ┃
    # ┗━━━━━━━━━━━━━━━━━━━━┛ 

    def do_show_all(self):
        """
        Invoked when the window reveals all widgets. We hide the contents of each page so only the GProc logo shows.        
        """

        self.show()
        for child in self.get_children(): 
            child.show_all()
            child.inner.hide() # Hide the container, show the image


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Event Handlers ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __event_current_node_change(self, caller, gproc_node):
        """
        Show the properties when the node changes. Invoked by publisher signal.
        """

        self.current_node = gproc_node
        if gproc_node is not None: self.__show_properties()
        else: self.__hide_properties()

    
    def __event_type_cmb_change(self, widget):
        """
        Change the current node's type, unless it's the same type.
        """

        if self.current_node is not None:
            step_type_entry = self.config_widgets["step-type"].get_child()
            step_type       = step_type_entry.get_text()
            current_type    = self.current_node.get_node_type(ncname=False)
            if (current_type != step_type) and (step_type != ""): 
                self.current_node.set_node_type(step_type)
                self.__update_specs()


    def __event_name_entry_change(self, widget):
        """
        Change the current node's name.
        """

        if self.current_node is not None:
            step_name    = widget.get_text()
            current_name = self.current_node.get_node_name()
            if current_name != step_name: 
                self.current_node.set_node_name(step_name)
                publisher.update_tab_names(self)


    def __event_docs_buffer_change(self, widget):
        """
        Change the current node's documentation.
        """

        if self.current_node is not None:
            step_docs = widget.props.text # Not using get_text() because props.text is easier.
            self.current_node.set_node_documentation(step_docs)


    def __event_inline_button_click(self, widget):
        """
        Tell the workspace to create a new tab for the inline XML.
        """

        if self.current_node is not None:
            publisher.open_inline(self, self.current_node.canvas_context.pool_id, self.current_node.get_object_handle())


    def __event_property_value_change(self, crt, path, text):
        """
        Change the current node's property.
        """

        if self.current_node is not None:
            properties_store = self.stores["properties_liststore"]
            properties_store[path][2] = text
            self.current_node.set_node_property(properties_store[path][1], text)


    def __event_input_port_change(self, crc, path, value):
        """
        Change the port name the connection is going to.
        """

        if self.current_node is not None: 
            inputs_store  = self.stores["inputs_liststore"]
            new_port_name = value
            self.current_node.assign_input_port_name(inputs_store[path][3], new_port_name)        
            inputs_store[path][1] = new_port_name # Also update the data store.
            

    def __event_input_select_change(self, crt, path, text):
        """
        Change the select attribute of the input port connection.
        """
       
        inputs_store  = self.stores["inputs_liststore"]
        inputs_store[path][2] = text
        inputs_store[path][3].set_select(text) # Invoke the method on the connection object.


    def __event_output_port_change(self, crc, path, value):
        """
        Change the port name the connection is going to.
        """

        if self.current_node is not None: 
            outputs_store  = self.stores["outputs_liststore"]
            new_port_name = value
            self.current_node.assign_output_port_name(outputs_store[path][2], new_port_name)       
            outputs_store[path][1] = new_port_name # Also update the data store.


    def __event_add_nsbinding_button_click(self, widget):
        """
        Add a namespace binding.
        """

        pass # TODO: Implement namespace bindings.


    def __event_get_input_port_names(self, caller):
        """
        Refresh the current list of input port names. Invoked by a p:choose when new xpath-contexts are added.
        """

        if self.current_node is not None:
            input_port_store = self.stores["input_port_store"]
            input_port_store.clear()

            for port_name in self.current_node.get_input_port_names():
                input_port_store.append([port_name])


    # ┏━━━━━━━━━━━━━━━━┓
    # ┃                ┃
    # ┃ Helper Methods ┃
    # ┃                ┃
    # ┗━━━━━━━━━━━━━━━━┛ 

    def __sort_properties(self, specs, given):
        """
        Sort the given set of properties based on what's in the spec and what's not - the order will be required > optional > others.
        The given set of properties is expected to be a dictionary of property names to values.
        The specs should come from `AtomicSpecification.options` or `CoreSpecification.properties` (see the xproc.specifications module).
        """
 
        result = [] # List of tuples, each being (name, value, required).

        # Get specification properties.
        for name, spec_object in specs.items():
            if name in given.keys():
                result.append((name, given[name], spec_object.required))
                del given[name] # Not the iterator, so it's okay, I think.
            else:
                result.append((name, "", spec_object.required))
        result = sorted(sorted(result, key=(lambda t: t[0])), key=(lambda t: t[2]), reverse=True) # Sort by required and name.

        # Append rest of the properties.
        for name, value in given.items():
            result.append((name, value, False))

        return result


    def __update_specs(self):
        """
        Reflect the specifications of the current node to the configuration widgets. Four things will need to be updated - properties, additionals, input port names, and output port names.
        """

        if self.current_node is not None:
            specification = self.current_node.get_specification()
            prefixed_name = self.current_node.get_node_type(ncname=False)

            if specification is None:
                self.__set_description("", "")
                properties  = []
                additionals = []

            elif specification.kind == "atomic":
                self.__set_description(prefixed_name, specification.documentation)
                properties  = self.__sort_properties(specification.options, self.current_node.get_node_properties())
                additionals = [] # TODO: Configure additionals.

            elif specification.kind == "core":
                self.__set_description(prefixed_name, specification.documentation)
                properties  = self.__sort_properties(specification.properties, self.current_node.get_node_properties())
                additionals = specification.additionals

            inputs  = self.current_node.get_input_connections()
            outputs = self.current_node.get_output_connections()

            # Populate properties treeview
            properties_store = self.stores["properties_liststore"]
            properties_store.clear()

            for property in properties:
                properties_store.append([
                    self.icons["required_black"] if property[2] else None,
                    property[0],
                    property[1]
                ])

            self.properties_value_renderer.props.editable = self.current_node.is_editable

            # Populate inputs treeview.
            inputs_store     = self.stores["inputs_liststore"]
            input_port_store = self.stores["input_port_store"]
            inputs_store.clear()            
            input_port_store.clear()

            for port_name in self.current_node.get_input_port_names():
                input_port_store.append([port_name])
            for connection in inputs:
                inputs_store.append([
                    str(connection[0].from_node),
                    connection[1],
                    connection[0].get_select(),
                    connection[0]
                ])

            self.inputs_port_renderer.props.editable   = self.current_node.node_class in ["atomic", "compound", "multicontainer"]
            self.inputs_port_renderer.props.has_entry  = self.current_node.node_class == "atomic"
            self.inputs_select_renderer.props.editable = self.current_node.node_class != "output"

            # Populate outputs treeview.
            outputs_store     = self.stores["outputs_liststore"]
            output_port_store = self.stores["output_port_store"]
            outputs_store.clear()
            output_port_store.clear()

            for port_name in list(set(self.current_node.get_output_port_names())):
                output_port_store.append([port_name])
            for connection in outputs:
                outputs_store.append([
                    str(connection[0].to_node),
                    connection[1],
                    connection[0]
                ])

            self.outputs_port_renderer.props.editable  = self.current_node.node_class in ["atomic", "compound", "multicontainer"]
            self.outputs_port_renderer.props.has_entry = self.current_node.node_class == "atomic"

            # Populate additionals
            additionals_box = self.config_widgets["additionals"]
            for child in additionals_box.get_children():
                additionals_box.remove(child)

            for additional in additionals:
                if additional == "contents":
                    additional_widget = Gtk.Button(label="Edit contents") #TRANSLATE#
                    additional_widget.connect("clicked", self.__event_inline_button_click)
                # elif additional.type == "log":
                #     additional_widget = Gtk.Label("What?")
                # elif additional.type == "serialization":
                #     additional_widget = Gtk.Label("hello")
                    additionals_box.pack_start(additional_widget, False, False, 0)
                    additional_widget.show()


    def __set_description(self, description_type, description_text):
        """
        Sets the step description title and content.
        """

        self.config_widgets["description-type"].set_text(description_type)
        self.config_widgets["description-text"].set_text(description_text)


    def __populate_properties(self):
        """
        Enables and disables widgets based on the current node's class.        
        """

        enable_mapping = {
            "atomic": {
                    "step-type": True,
                         "name": True,
                "documentation": True
            },
            "compound": {
                    "step-type": False,
                         "name": True,
                "documentation": True
            },
            "declaration": {
                    "step-type": False,
                         "name": None, # Can be named if it's not p:library.
                "documentation": True
            },
            "multicontainer": {
                    "step-type": False,
                         "name": True,
                "documentation": True
            },
            "subpipeline": {
                    "step-type": False,
                         "name": None, # Can be named if it's p:try, can't if it's p:choose.
                "documentation": True
            },
            "source": {
                    "step-type": True,
                         "name": False,
                "documentation": None, # Can have documentation if it's not p:inline.
            },
            "import": {
                    "step-type": False,
                         "name": False,
                "documentation": False
            },
            "input": {
                    "step-type": False,
                         "name": True,
                "documentation": False
            },
            "output": {
                    "step-type": False,
                         "name": True,
                "documentation": False
            },
            "option": {
                    "step-type": False,
                         "name": True,
                "documentation": False
            },
            "variable": {
                    "step-type": False,
                         "name": True,
                "documentation": True
            },
            "with-option": {
                    "step-type": False,
                         "name": True,
                "documentation": True
            },
            "with-param": {
                    "step-type": False,
                         "name": True,
                "documentation": True
            }
        }

        # Get the mapping of widget enablings.
        node_class = self.current_node.node_class        
        editable   = self.current_node.is_editable
        enable_map = enable_mapping[node_class] if node_class in enable_mapping.keys() else {key: False for key in ["step-type", "name", "documentation"]}

        # Set widget state based on mapping.
        for widget_name, enable in enable_map.items():
            if widget_name == "step-type":
                step_type = self.config_widgets["step-type"]
                step_type.remove_all()
                step_type.get_child().set_editable(enable and editable)
                step_type.get_child().set_text(self.current_node.get_node_type(ncname=False) or "")
                
                # Populate combobox list with available steps if it's an atomic step.
                spec_kind = "atomic" if node_class == "atomic" else "core"
                get_available_steps = object_manager.get_all_specifications(self.current_node.canvas_context.pool_id, kind=spec_kind)
                if get_available_steps.exception is None:                    
                    if node_class == "atomic":
                        for step in get_available_steps.data["standard"]: 
                            step_ns_prefix = self.current_node.canvas_context.find_prefix(step.namespace)
                            if not str_none_or_empty(step_ns_prefix): step_ns_prefix += ":"
                            step_type.append_text("{}{}".format(step_ns_prefix, step.name))
                    elif node_class == "source":
                        for step in get_available_steps.data["source"]: 
                            xproc_ns_prefix = self.current_node.canvas_context.get_xproc_prefix()
                            step_type.append_text("{}:{}".format(xproc_ns_prefix, step.name))
                
            elif widget_name == "name":
                name = self.config_widgets["name"]
                name.set_text(self.current_node.get_node_name() or "")
                if enable is None:
                    # Special case - check the underlying type.
                    if self.current_node.get_node_type() in ["when", "otherwise", "library"]: name.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
                    else: name.set_state_flags(Gtk.StateFlags.NORMAL, True)
                else:
                    name.set_state_flags(Gtk.StateFlags.NORMAL if (enable and editable) else Gtk.StateFlags.INSENSITIVE, True)
                
            elif widget_name == "documentation":                
                documentation = self.config_widgets["documentation"]
                documentation.get_buffer().set_text(self.current_node.get_node_documentation() or "")
                if enable is None:
                    # Special case - check the underlying type.
                    if self.current_node.get_node_type() == "inline": documentation.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
                    else: documentation.set_state_flags(Gtk.StateFlags.NORMAL, True)
                else:
                    documentation.set_state_flags(Gtk.StateFlags.NORMAL if (enable and editable) else Gtk.StateFlags.INSENSITIVE, True)


    def __show_properties(self):
        """
        Reveals the configuration widgets.        
        """

        for child in self.get_children():
            # Each child is a Gtk.Overlay
            child.show_all()
            child.inner.set_state_flags(Gtk.StateFlags.ACTIVE, True)
            child.image.set_state_flags(Gtk.StateFlags.NORMAL, True)

        self.__populate_properties()
        self.__update_specs()


    def __hide_properties(self):
        """
        Hides the configuration widgets.
        """

        self.config_widgets["name"].grab_focus() # GTK bug workaround!
        for child in self.get_children():
            # Each child is a Gtk.Overlay
            child.show_all()
            child.image.set_state_flags(Gtk.StateFlags.ACTIVE, True)
            child.inner.set_state_flags(Gtk.StateFlags.NORMAL | Gtk.StateFlags.INSENSITIVE, True)