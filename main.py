"""
This is the entry point of the program. We define a GProcApplication class, and instantiate it.
"""

import gi, os, sys
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Gio
from gproc.window  import GProcMainWindow

# Set up constants and variables to use for launching the application
APPLICATION_ID      = "org.GProc.MainApplication"
APPLICATION_FLAG    = Gio.ApplicationFlags.FLAGS_NONE
APPLICATION_VERSION = "0.1.0"



class GProcApplication(Gtk.Application):
    """
    A derived class which implements GProc as an entire application. 

    See the docs for Gtk.Application: http://lazka.github.io/pgi-docs/Gtk-3.0/classes/Application.html
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def do_startup(self):
        """
        This is ran first from the app.run() call. Does startup and sets up actions.
        """

        Gtk.Application.do_startup(self)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.on_quit) # Callback for when the 'quit' action is invoked.
        self.add_action(quit_action)


    def do_activate(self):
        """ 
        This is ran after do_startup(). Creates the window and sets up CSS provider.
        """

        window = GProcMainWindow(
            application  = self, 
            version      = APPLICATION_VERSION, 
            show_menubar = True
        )
        window.set_name("main-window") # set CSS name.

        script_dir = sys._MEIPASS if getattr(sys, "frozen", False) else os.path.dirname(os.path.realpath(__file__))
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path(os.path.join(script_dir, "css/main.css"))
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


    def on_quit(self, action, param):
        self.quit()



# Run
if __name__ == "__main__":
    app = GProcApplication()
    app.run()
