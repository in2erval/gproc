"""
This module contains the exporting routine.
"""

from lxml                 import etree
from xproc.object_manager import object_manager
from xproc.utils          import XPROC_NAMESPACE



class ExportContext:
    """
    The class which keeps track of data relevant to the export process.
    """
    
    def __init__(self, pool_id, namespace_mapping, settings):
        # Private variables
        self.__settings          = settings
        self.__step_name_mapping = {}
        self.__counter           = 1 # For generating step names.

        # Public variables
        self.namespace_mapping = namespace_mapping
        self.root_element      = None
        self.pool_id           = None
        self.xproc_ns_string   = "{" + XPROC_NAMESPACE + "}"

        # Provide a default prefix for the XProc namespace if it's not in the mapping.
        if XPROC_NAMESPACE not in self.namespace_mapping.values():
            self.namespace_mapping["p"] = XPROC_NAMESPACE


    def get_setting(self, setting):
        """
        Returns the setting value.
        """

        return self.__settings.get(setting, None)


    def get_name(self, xproc_object):
        """
        Returns the designated name for the given XProc object. If there isn't one, a new name is generated.
        """

        if xproc_object not in self.__step_name_mapping.keys():        
            if xproc_object.name != "":
                self.__step_name_mapping[xproc_object] = xproc_object.name
            else:
                self.__step_name_mapping[xproc_object] = "step_" + str(self.__counter)
                self.__counter += 1

        return self.__step_name_mapping[xproc_object]



def export_to_file(pool_id, object_handle, namespace_mapping, filename, export_settings={}):
    """
    Exports the XProc object corresponding to the given object handle (in a given pool) as a file. 
    A mapping of prefixes is used to set up the namespace mappings, and a dictionary of settings
    can be passed in to modify the export process behaviour.
    
    :param pool_id: The ID of the pool the objects belong in.
    :type pool_id: `str`
    :param to_object_handle: The handle of the object to create a representation of.
    :type to_object_handle: `str`
    :param namespace_mapping: A dictionary of prefixes to URLs. Must contain a mapping for the XProc namespace.
    :type namespace_mapping: `dict`
    :param filename: The filename of the file to save as. Fails if it cannot be opened.
    :type filename: `str`
    :param export_settings: A dictionary of setting names to values, defaults to {}
    :type export_settings: `dict`, optional
    """

    with open(filename, mode="wb") as file: 
        xproc_object = object_manager.get_object(pool_id=pool_id, object_handle=object_handle).data
        export_context = ExportContext(
            pool_id           = pool_id,
            namespace_mapping = namespace_mapping,
            settings          = export_settings
        )

        representation = __generate_structure(xproc_object, export_context)
        file.write(etree.tostring(representation, pretty_print=True))


def __generate_structure(xproc_object, context):
    """
    Generates the XML structure of a given XProc object. This function is called recursively
    to create the hierarchy structure of the resulting XProc file.
    
    :param xproc_object: The XProc object to generate the XML representation of.
    :type xproc_object: `xproc.entity.Entity`
    :param context: The export context to use for the process.
    :type context: `ExportContext`
    :return: The etree representation of the XProc object.
    :rtype: `etree.Element`
    """

    # Make sure the root is instantiated
    if context.root_element is None:
        context.root_element = etree.Element(
            context.xproc_ns_string + xproc_object.type, 
            nsmap   = context.namespace_mapping, 
            version = context.get_setting("version") or "1.0"
        )
        element = context.root_element
    else:
        element = etree.Element(context.xproc_ns_string + xproc_object.type)

    # Populate the new XML element with attributes and children, based on the exact class of object.

    # 1 - Documentation (for all classes)
    if xproc_object.documentation != "":
        documentation_element = etree.Element(context.xproc_ns_string + "documentation")
        documentation_element.text = xproc_object.documentation
        element.append(documentation_element)

    # 2 - Properties (for all classes except atomic steps)
    if xproc_object.entity_class != "atomic":
        for prop_name, prop_value in xproc_object.properties.items():
            element.set(prop_name, prop_value)

    # 3 - Atomic steps
    if xproc_object.entity_class == "atomic":

        # 3.1 - Name
        element.set("name", context.get_name(xproc_object))

        # 3.2 - Options
        # First create the explicitly specified p:with-option elements.
        used_options = [] # Keep track of unique options elements per atomic step.
        for with_option in xproc_object.options:
            if with_option.name not in used_options:
                used_options.append(with_option.name)
                with_option_element = __generate_withX_element(with_option, context)
                element.append(with_option_element)
        # Then create the shorthand options.
        for option_name, option_value in xproc_object.properties.items():
            if option_name not in used_options:
                used_options.append(with_option.name)
                # If 'always-use-extended-options' setting is set to True, create a p:with-option for each shorthand option.
                if context.get_setting("always-use-extended-options"):
                    __generate_withX_element
                    with_option_element = __generate_withX_element(with_option, context)
                    element.append(with_option_element)
                else:
                    element.set(option_name, option_value)

        # 3.3 - Parameters
        for with_param in xproc_object.params:
            with_param_element = __generate_withX_element(with_param, context)
            element.append(with_param_element)

        # 3.4 - Inputs
        for input_element in __generate_io_elements(xproc_object.inputs, context, kind="input"):
            element.append(input_element)

    # 4 - Compound steps
    elif xproc_object.entity_class == "compound":

        # 4.1 - Name
        if xproc_object.type not in ["when", "otherwise"]:
            element.set("name", context.get_name(xproc_object))
        
        # 4.2 - Variables
        for variable_element in __generate_variable_elements(xproc_object.variables, context):
            element.append(variable_element)

        # 4.3 - Inputs
        # These won't be p:input elements, but instead will be special inputs (e.g. p:iteration-source).
        for input_element in __generate_io_elements(xproc_object.inputs, context, kind="input"):
            element.append(input_element)

        # 4.4 - Outputs
        for output_element in __generate_io_elements(xproc_object.outputs, context, kind="output"):
            element.append(output_element)

        # 4.5 - Children
        for child in xproc_object.contents:
            if child.entity_class in ["atomic", "compound", "multicontainer"]: # Whitelist of children class to create representations of.
                element.append(__generate_structure(child, context))

    # 5 - Multicontainer steps
    elif xproc_object.entity_class == "multicontainer":

        # 5.1 - Name
        element.set("name", context.get_name(xproc_object))

        # 5.2 - Variables
        for variable_element in __generate_variable_elements(xproc_object.variables, context):
            element.append(variable_element)

        # 5.3 - Inputs
        # Only relevant for p:choose, as p:xpath-context is treated as an input port.
        if hasattr(xproc_object, "inputs"):
            for input_element in __generate_io_elements(xproc_object.inputs, context, kind="input"):
                element.append(input_element)

        # 5.4 - Children
        for child in xproc_object.contents: 
            element.append(__generate_structure(child, context)) # No whitelist necessary as it will only ever have compound steps.

    # 6 - Sources
    elif xproc_object.entity_class == "source":

        # 7.1 - Inline contents
        # Only relevant for p:inline.
        if "contents" in xproc_object.additionals.keys():
            try: # Attempt to parse the inline as XML                
                inline_element = etree.fromstring(xproc_object.additionals["contents"])
                element.append(inline_element)
            except: # If it doesn't work, oh well.                
                pass

    # 7 - Step declarations
    elif xproc_object.entity_class == "declaration":

        # 7.1 - Name
        element.set("name", context.get_name(xproc_object))

        # 7.2 - Variables
        for variable_element in __generate_variable_elements(xproc_object.variables, context):
            element.append(variable_element)

        # 7.3 - Input specifications
        for input_spec in xproc_object.input_specs.values():
            input_spec_element = etree.Element(context.xproc_ns_string + "input")
            input_spec_element.set("port",     input_spec.port)
            input_spec_element.set("kind",     input_spec.kind)
            input_spec_element.set("select",   input_spec.select)
            input_spec_element.set("primary",  str(input_spec.primary))
            input_spec_element.set("sequence", str(input_spec.sequence))
            element.append(input_spec_element)

        # 7.4 - Output specifications and connections
        # First generate the outputs from connections.
        used_outputs = []
        for output_element in __generate_io_elements(xproc_object.outputs, context, kind="output"):
            output_name = output_element.get("port")
            if output_name not in used_outputs:
                used_outputs.append(output_name)
                if output_name in xproc_object.output_specs.keys(): # Refer to output specs if it can be found.
                    output_element.set("primary",  str(xproc_object.output_specs[output_name].primary))
                    output_element.set("sequence", str(xproc_object.output_specs[output_name].sequence))
                element.append(output_element)
        # Then generate outputs from specs.
        for output_name, output_spec in xproc_object.output_specs.items():
            if output_name not in used_outputs:
                output_element = etree.Element(context.xproc_ns_string + "output")
                output_element.set("primary",  str(output_spec.primary))
                output_element.set("sequence", str(output_spec.sequence))
                element.append(output_element)

        # 7.5 - Option specifications
        for option_spec in xproc_object.option_specs.values():
            option_spec_element = etree.Element(context.xproc_ns_string + "option")
            option_spec_element.set("name",     option_spec.name)
            option_spec_element.set("required", str(option_spec.required))
            option_spec_element.set("select",   option_spec.select)
            element.append(option_spec_element)

        # 7.6 - Imports
        for import_url in xproc_object.imports:
            import_element = etree.Element(context.xproc_ns_string + "import")
            import_element.set("href", import_url)
            element.append(import_element)

        # 7.7 - Children
        # Child step declarations should come first, then the rest.
        for child in xproc_object.contents: 
            if child.entity_class == "declaration":
                element.append(__generate_structure(child, context))
        for child in xproc_object.contents:
            if child.entity_class in ["atomic", "compound", "multicontainer"]: # Whitelist of children class to create representations of.
                element.append(__generate_structure(child, context))

    # 8 - Libraries
    elif xproc_object.entity_class == "library":

        # 8.1 - Imports
        for import_url in xproc_object.imports:
            import_element = etree.Element(context.xproc_ns_string + "import")
            import_element.set("href", import_url)
            element.append(import_element)

        # 8.2 - Children
        # Child step declarations should come first, then the rest.
        for child in xproc_object.contents: 
            element.append(__generate_structure(child, context)) # No whitelist necessary as it will only ever have step declarations.

    return element


def __generate_io_elements(portset, context, kind="input"):
    """
    Creates a list of elements from the given port set.
    This method simplifies the creation of connection elements like p:input.
    
    :param portset: The port set to create the connection elements from.
    :type portset: `xproc.portset.PortSet`
    :param context: The export context, for referring to the names of steps.
    :type context: `ExportContext`
    :param kind: The kind of element to create, defaults to "input"
    :type kind: `str in ['input', 'output']`
    :return: The list of etree representations of the input/output connections.
    :rtype: `list(etree.Element)`
    """

    io_elements = []

    # Look through each port and create the port connection element.
    for port_name, port_details in portset.ports.items():
        if port_name in ["iteration-source", "viewport-source", "xpath-context"]: 
            io_element = etree.Element(context.xproc_ns_string + port_name) # Special IO elements.
        else:
            io_element = etree.Element(context.xproc_ns_string + kind) # p:input or p:output.
            io_element.set("port", port_name)

        # Set the port properties, e.g. "select".
        for prop_name, prop_value in port_details.properties.items():
            io_element.set(prop_name, prop_value)

        # Only create the connection if there is at least one connection.
        if len(port_details.connections) > 0:
            # For each connection to the port, create its representation.
            for connection in port_details.connections:
                # Create the representation for source entities.
                if connection.entity.entity_class == "source":
                    io_element.append(__generate_structure(connection.entity, context))
                # Create the p:pipe element.
                else:
                    pipe_element = etree.Element(context.xproc_ns_string + "pipe")
                    pipe_element.set("step", context.get_name(connection.entity))
                    if connection.entity_port_name in ["iteration-source", "viewport-source"]:
                        pipe_element.set("port", "current") # For connections from these ports, we use "current" instead of the name.
                    else:
                        pipe_element.set("port", connection.entity_port_name)
                    io_element.append(pipe_element)

            io_elements.append(io_element)

    return io_elements


def __generate_withX_element(withX_object, context):
    """
    Create a single with-X object, either with-option or with-param.
    
    :param portset: The with-X object to create the representation of.
    :type portset: `xproc.opv.WithOption or xproc.opv.WithParam`
    :param context: The export context, for referring to the names of steps.
    :type context: `ExportContext`
    :return: The etree representation of the with-X object.
    :rtype: `etree.Element`
    """

    # Rely on __generate_io_elements() to create the representations, then modify it to get the appropriate element.
    # If none are returned, simply create the element.
    input_elements = __generate_io_elements(withX_object.inputs, context)
    if len(input_elements) > 0:
        withX_element = input_elements[0]
        withX_element.tag = context.xproc_ns_string + withX_object.entity_class
        withX_element.set("port", None) # Clear the 'port' attribute (set to 'default')
    else:
        withX_element = etree.Element(context.xproc_ns_string + withX_object.entity_class)
    
    # Set the name and properties.
    withX_element.set("name", withX_object.name)
    for prop_name, prop_value in withX_object.properties.items():
        withX_element.set(prop_name, prop_value)

    return withX_element


def __generate_variable_elements(variables_list, context):
    """
    Create a list of elements from the given list of variables.
    
    :param variables_list: A list of variable entities to create the element representations of.
    :type variables_list: `list(xproc.opv.Variable)`
    :param context: The export context, for referring to the names of steps.
    :type context: `ExportContext`
    :return: The list of etree representations of the variables.
    :rtype: `list(etree.Element)`
    """

    variables = []

    for variable in variables_list:
        # Rely on __generate_io_elements() to create the representations, then modify it to get the appropriate element.
        # If none are returned, simply create the element.
        input_elements = __generate_io_elements(variable.inputs, context)
        if len(input_elements) > 0:
            variable_element = input_elements[0]
            variable_element.tag = context.xproc_ns_string + "variable"
            variable_element.set("port", None) # Clear the 'port' attribute (set to 'default')
        else:
            variable_element = etree.Element(context.xproc_ns_string + "variable")
    
        # Set the name and properties.
        variable_element.set("name", variable.name)
        for prop_name, prop_value in variable.properties.items():
            variable_element.set(prop_name, prop_value)

        variables.append(variable_element)

    return variables