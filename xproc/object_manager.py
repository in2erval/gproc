"""
This module contains the object manager.
"""

import collections
import functools
import uuid
import xproc.specifications # Separately done to access the specifications dictionaries.

from xproc.entity         import Entity, NamedEntity
from xproc.errors         import ObjectManagerError, SemanticError
from xproc.opv            import OPV, Variable, WithOption, WithParam
from xproc.result         import Result
from xproc.source         import Source
from xproc.specifications import InputSpec, OutputSpec, OptionSpec
from xproc.steps          import AtomicStep, CompoundStep, Library, MulticontainerStep, Step, StepDeclaration
from xproc.utils          import str_none_or_empty, XPROC_NAMESPACE


def interface(func):
    """
    A decorator which checks the given pool ID, and captures errors into an error result object.
    """

    @functools.wraps(func)
    def check(self, pool_id, *args, **kwargs):
        try:
            if pool_id in self.object_pool.keys(): return func(self, pool_id, *args, **kwargs)
            else: raise ObjectManagerError("Invalid pool ID.")
        except Exception as e:
            return Result.error(e)
    return check



class ObjectManager:
    """
    The ObjectManager is used for creating and interacting with XProc object representations.

    Previously the canvas elements themselves would manage its object representations, but this is more difficult to co-ordinate and manage. By delegating the responsibility of creating and interacting with XProc objects to an external object, we aim to make the overall process more streamlined and maintainable.   
    """

    def __init__(self):
        self.object_pool = {} # Dictionary of pools. Each pool separates XProc documents.


    # ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                            ┃
    # ┃ Creation & Removal methods ┃
    # ┃                            ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛ 

    def create_pool(self):
        """
        Create a new object pool. Returns the newly created pool ID captured into a result object.   

        :return: A result with the newly created pool ID.
        :rtype: `xproc.result.Result`     
        """

        pool_handle = str(uuid.uuid4())
        self.object_pool[pool_handle] = ObjectPool()
        return Result(pool_handle)


    @interface
    def create_object(self, pool_id, ent_class, ent_type=None, ent_name=None, ent_ns=None, parent_handle=None):
        """
        Creates an object inside the given pool. If the object creation was successful, it will return a success result. Otherwise, it will either return a fail result or an error result.
        
        :param pool_id: The pool ID to create objects in.
        :type pool_id: `str`
        :param ent_class: The entity class of the new object.
        :type ent_class: `str in ['atomic', 'compound', 'multicontainer', 'declaration', 'library', 'source', 'with-option', 'with-param', 'variable']`
        :param ent_type: The type of the new object, if applicable. Must be given if ent_class is 'atomic', 'compound', 'multicontainer', 'declaration', or 'source'.
        :type ent_type: `str`
        :param ent_name: The name of the new object, if applicable. Must be given if ent_class is 'with-option', 'with-param', or 'variable'.
        :param ent_name: `str`
        :param ent_ns: The namespace of the new object. Ignored if ent_class is not 'atomic'.
        :param ent_ns: `str`
        :param parent_handle: The parent object's handle, if applicable.
        :param parent_handle: `str`, optional
        :return: A success result if the object was added, a fail result if parameters were invalid, error result if there are unexpected errors.
        :rtype: `xproc.result.Result`
        """
        
        # Split the objects into 'typed' and 'named'. They need the entity type and name, respectively.
        typed_class_map = {
                    "atomic": AtomicStep,
                  "compound": CompoundStep,
            "multicontainer": MulticontainerStep,
               "declaration": StepDeclaration,
                    "source": Source
        }
        named_class_map = {
            "with-option": WithOption,
             "with-param": WithParam,
               "variable": Variable
        }

        # Create a typed entity.
        new_entity = None # Initialise the field.
        if ent_class in typed_class_map.keys():
            if not str_none_or_empty(ent_type):
                new_entity = typed_class_map[ent_class](ent_type)
        
        # Create a named entity.
        elif ent_class in named_class_map.keys():
            if not str_none_or_empty(ent_name):
                new_entity = named_class_map[ent_class](ent_name)

        # Special case. Create a library.
        elif ent_class == "library":
            new_entity = Library()

        # Add the object, get the object handle.
        if new_entity is not None:
            object_handle = self.object_pool[pool_id].add_object(new_entity, parent_handle)

            # Assign step name if applicable and able.
            if isinstance(new_entity, Step) and not str_none_or_empty(ent_name):
                self.set_name(pool_id, object_handle, ent_name)

            return Result(object_handle)

        return Result.fail()


    @interface
    def remove_object(self, pool_id, object_handle):
        """
        Removes an object from the given pool. If no object is found, this returns a fail result.
        
        :param pool_id: The pool ID to remove the object from.
        :type pool_id: `str`
        :param object_handle: The handle of the object to remove.
        :type object_handle: `str`
        :return: A success result if the object was added, a fail result if parameters were invalid, error result if there are unexpected errors.
        :rtype: `xproc.result.Result`
        """

        if self.object_pool[pool_id].remove_object(object_handle): return Result.success()
        return Result.fail()


    # ┏━━━━━━━━━━━━━━━━━┓
    # ┃                 ┃
    # ┃ General getters ┃
    # ┃                 ┃
    # ┗━━━━━━━━━━━━━━━━━┛

    @interface
    def get_object(self, pool_id, object_handle):
        """
        Returns the object corresponding to the object handle, in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get.
        :type object_handle: `str`
        :return: A result with the object.
        :rtype: `xproc.result.Result`
        """

        return Result(self.object_pool[pool_id].get_object(object_handle))


    @interface
    def get_type(self, pool_id, object_handle):
        """
        Returns the type of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the type of.
        :type object_handle: `str`
        :return: A result with the type of the object.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        return Result(xp_object.type)


    @interface
    def get_namespace(self, pool_id, object_handle):
        """
        Returns the namespace of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the namespace of.
        :type object_handle: `str`
        :return: A result with the namespace of the object.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        return Result(xp_object.namespace)


    @interface
    def get_documentation(self, pool_id, object_handle):
        """
        Returns the documentation of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the documentation of.
        :type object_handle: `str`
        :return: A result with the documentation of the object.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        return Result(xp_object.documentation)


    @interface
    def get_properties(self, pool_id, object_handle):
        """
        Returns the properties of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the properties of.
        :type object_handle: `str`
        :return: A result with the properties of the object.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        return Result({**xp_object.properties})


    @interface
    def get_additionals(self, pool_id, object_handle):
        """
        Returns the additionals of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the additionals of.
        :type object_handle: `str`
        :return: A result with the additionals of the object.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        return Result({**xp_object.additionals})


    @interface
    def get_name(self, pool_id, object_handle):
        """
        Returns the name of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the name of.
        :type object_handle: `str`
        :return: A result with the name of the object.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        if isinstance(xp_object, NamedEntity): return Result(xp_object.name)
        else: raise ObjectManagerError("Cannot get name from the given object handle.")


    @interface
    def get_specification(self, pool_id, object_handle):
        """
        Returns the specification of the object in the given pool.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the specification of.
        :type object_handle: `str`
        :return: A result with the specification of the object, if it doesn't exist it will be a result with None.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        return Result(xp_object.get_specification())


    # ┏━━━━━━━━━━━━━━━━━┓
    # ┃                 ┃
    # ┃ General setters ┃
    # ┃                 ┃
    # ┗━━━━━━━━━━━━━━━━━┛

    @interface
    def set_type(self, pool_id, object_handle, new_type):
        """
        Sets the type of the object in the given pool, if possible.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the type of.
        :type object_handle: `str`
        :param new_type: The new type value. Cannot be empty.
        :type new_type: `str`
        :return: A success result if the type was set, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        if str_none_or_empty(new_type): return Result.fail()
        
        xp_object = self.object_pool[pool_id].get_object(object_handle)
        if isinstance(xp_object, AtomicStep) or isinstance(xp_object, Source):
            xp_object.type = new_type
            return Result.success()
        return Result.fail()


    @interface
    def set_namespace(self, pool_id, object_handle, new_namespace):
        """
        Sets the namespace of the object in the given pool, if possible.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the namespace of.
        :type object_handle: `str`
        :param new_namespace: The new namespace value.
        :type new_namespace: `str`
        :return: A success result if the namespace was set, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        if isinstance(xp_object, AtomicStep) or isinstance(xp_object, OPV):
            xp_object.namespace = new_namespace or ""
            return Result.success()
        return Result.fail()


    @interface
    def set_documentation(self, pool_id, object_handle, new_documentation):
        """
        Sets the documentation of the object in the given pool, if possible.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the documentation of.
        :type object_handle: `str`
        :param new_documentation: The new documentation value.
        :type new_documentation: `str`
        :return: A success result if the documentation was set.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        xp_object.documentation = new_documentation or ""
        return Result.success()


    @interface
    def set_property(self, pool_id, object_handle, property_name, property_value):
        """
        Sets the property of the object in the given pool, if possible.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the property of.
        :type object_handle: `str`
        :param property_name: The property name. Cannot be empty.
        :type property_name: `str`
        :param property_value: The property value. If empty and the property name exists, will remove the property.
        :type property_value: `object`
        :return: A success result if the property was set.
        :rtype: `xproc.result.Result`
        """

        if property_name == "": return Result.fail()

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        if (property_name in xp_object.properties.keys()) and (property_value == ""):
            del xp_object.properties[property_name]
        else: 
            xp_object.properties[property_name] = property_value
        return Result.success()


    @interface
    def set_additional(self, pool_id, object_handle, additional_name, additional_value):
        """
        Sets the additional of the object in the given pool, if possible.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the additional of.
        :type object_handle: `str`
        :param additional_name: The additional name. Cannot be empty.
        :type additional_name: `str`
        :param additional_value: The additional value.
        :type additional_value: `object`
        :return: A success result if the additional was set.
        :rtype: `xproc.result.Result`
        """

        if additional_name == "": return Result.fail()

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        if (additional_name in xp_object.additionals.keys()) and (additional_value == ""):
            del xp_object.additionals[additional_name]
        else: 
            xp_object.additionals[additional_name] = additional_value
        return Result.success()


    @interface
    def set_name(self, pool_id, object_handle, new_name):
        """
        Sets the name of the object in the given pool, if possible.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the name of.
        :type object_handle: `str`
        :param new_name: The new name value.
        :type new_name: `str`
        :return: A success result if the name was set, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)
        if isinstance(xp_object, NamedEntity):
            xp_object.name = new_name or ""
            return Result.success()

        return Result.fail()


    # ┏━━━━━━━━━━━━━━━━━━━━┓
    # ┃                    ┃
    # ┃ Connection methods ┃
    # ┃                    ┃
    # ┗━━━━━━━━━━━━━━━━━━━━┛

    @interface
    def connect_input(self, pool_id, to_object_handle, to_port, from_object_handle, from_port):
        """
        Creates a connection between two objects, connecting to an input port. If the source object is a with-option or a with-param, it must be connecting to an atomic step - otherwise the objects should follow their connection semantics.
        
        :param pool_id: The ID of the pool the objects belong in.
        :type pool_id: `str`
        :param to_object_handle: The handle of the object to connect to.
        :type to_object_handle: `str`
        :param to_port: The port name on the destination object.
        :type to_port: `str`
        :param from_object_handle: The handle of the object to connect from.
        :type from_object_handle: `str`
        :param from_port: The port name on the source object.
        :type from_port: `str`
        :return: A success result if the connection was made, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        from_object = self.object_pool[pool_id].get_object(from_object_handle)
        to_object   = self.object_pool[pool_id].get_object(to_object_handle)

        if isinstance(from_object, WithOption) or isinstance(from_object, WithParam):
            # "Connect" here means add as option or param of atomic step.
            kind = "option" if isinstance(from_object, WithOption) else "parameter"
            if not isinstance(to_object, AtomicStep): raise SemanticError("with-option or with-param may only be connected to atomic steps.")
            
            # Detect existing connection.
            old_parent_handle = self.object_pool[pool_id].get_parent(from_object_handle)
            if old_parent_handle is not None: raise SemanticError("with-option or with-param may only be connected to one atomic step.")

            # Set new parent, add to parent object.
            self.object_pool[pool_id].update_parent(from_object_handle, to_object_handle)
            if kind == "option": to_object.options.append(from_object)
            else: to_object.params.append(from_object)
            
            return Result.success()

        elif hasattr(to_object, "inputs"):
            # Only possible inputs are steps and sources.
            if isinstance(from_object, Step) or isinstance(from_object, Source):
                to_object.inputs.add_port_connection(to_port, from_object, from_port)
                return Result.success()

        return Result.fail()


    @interface
    def connect_output(self, pool_id, to_object_handle, to_port, from_object_handle, from_port):
        """
        Creates a connection between two objects, connecting to an output port. The objects should follow their connection semantics.
        
        :param pool_id: The ID of the pool the objects belong in.
        :type pool_id: `str`
        :param to_object_handle: The handle of the object to connect to.
        :type to_object_handle: `str`
        :param to_port: The port name on the destination object.
        :type to_port: `str`
        :param from_object_handle: The handle of the object to connect from.
        :type from_object_handle: `str`
        :param from_port: The port name on the source object.
        :type from_port: `str`
        :return: A success result if the connection was made, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        from_object = self.object_pool[pool_id].get_object(from_object_handle)
        to_object   = self.object_pool[pool_id].get_object(to_object_handle)

        if hasattr(to_object, "outputs"):
            # Only possible outputs are steps and sources.
            if isinstance(from_object, Step) or isinstance(from_object, Source):
                to_object.outputs.add_port_connection(to_port, from_object, from_port)
                return Result.success()

        return Result.fail()


    @interface
    def disconnect_input(self, pool_id, to_object_handle, to_port=None, from_object_handle=None, from_port=None):
        """
        Deletes a connection based on given parameters. If the source object is a with-option or a with-param, it must be disconnecting from an atomic step - otherwise the objects should follow their connection semantics.
        
        :param pool_id: The ID of the pool the objects belong in.
        :type pool_id: `str`
        :param to_object_handle: The handle of the object to disconnect from.
        :type to_object_handle: `str`
        :param to_port: The port name on the destination object.
        :type to_port: `str`
        :param from_object_handle: The handle of the object to disconnect.
        :type from_object_handle: `str`
        :param from_port: The port name on the source object.
        :type from_port: `str`
        :return: A success result if the connection was made, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        from_object = self.object_pool[pool_id].get_object(from_object_handle) if from_object_handle is not None else None
        to_object   = self.object_pool[pool_id].get_object(to_object_handle)

        if isinstance(from_object, WithOption) or isinstance(from_object, WithParam):
            # "Connect" here means remove from option or param of atomic step.
            kind = "option" if isinstance(from_object, WithOption) else "parameter"
            if not isinstance(to_object, AtomicStep): raise SemanticError("with-option or with-param may only be disconnected from atomic steps.")

            # Sanity check
            parent_handle = self.object_pool[pool_id].get_parent(from_object_handle)
            if parent_handle == to_object_handle:
                grand_parent_handle = self.object_pool[pool_id].get_parent(parent_handle)
                self.object_pool[pool_id].update_parent(from_object_handle, grand_parent_handle)
                if kind == "option": to_object.options.remove(from_object)
                else: to_object.params.remove(from_object)
                
                return Result.success()

        elif hasattr(to_object, "inputs"):
            # Only possible inputs are steps and sources.
            if to_object.inputs.remove_port_connections(port_name=to_port, entity=from_object, entity_port_name=from_port):
                return Result.success()

        return Result.fail()


    @interface
    def disconnect_output(self, pool_id, to_object_handle, to_port, from_object_handle=None, from_port=None):
        """
        Deletes a connection based on given parameters. The objects should follow their connection semantics.
        
        :param pool_id: The ID of the pool the objects belong in.
        :type pool_id: `str`
        :param to_object_handle: The handle of the object to disconnect from.
        :type to_object_handle: `str`
        :param to_port: The port name on the destination object.
        :type to_port: `str`
        :param from_object_handle: The handle of the object to disconnect.
        :type from_object_handle: `str`
        :param from_port: The port name on the source object.
        :type from_port: `str`
        :return: A success result if the connection was made, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        from_object = self.object_pool[pool_id].get_object(from_object_handle) if from_object_handle is not None else None
        to_object   = self.object_pool[pool_id].get_object(to_object_handle)

        if hasattr(to_object, "outputs"):
            # Only possible outputs are steps and sources.
            if to_object.outputs.remove_port_connections(port_name=to_port, entity=from_object, entity_port_name=from_port):
                return Result.success()

        return Result.fail()


    @interface
    def disconnect_all_inputs(self, pool_id, object_handle):
        """
        Removes all input connections on the given object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to remove input connections of.
        :type object_handle: `str`
        :return: A success result if the connections were removed, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "inputs"):
            xp_object.inputs.remove_all_ports()
            return Result.success()

        return Result.fail()


    @interface
    def disconnect_all_outputs(self, pool_id, object_handle):
        """
        Removes all output connections on the given object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to remove output connections of.
        :type object_handle: `str`
        :return: A success result if the connections were removed, a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "outputs"):
            xp_object.outputs.remove_all_ports()
            return Result.success()

        return Result.fail()


    @interface
    def has_input_connection(self, pool_id, to_object_handle, to_port=None, from_object_handle=None):
        """
        Checks whether an input connection exists based on given parameters.
        
        :param pool_id: The ID of the pool the objects belong in.
        :type pool_id: `str`
        :param to_object_handle: The handle of the object to check input connections of.
        :type to_object_handle: `str`
        :param to_port: The port to check for input connections.
        :param to_port: `str`
        :param from_object_handle: The handle of the object to check whether there are input connections originating from it.
        :param from_object_handle: `str`
        :return: A result object with True if there are connections, False if there aren't, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        from_object = self.object_pool[pool_id].get_object(from_object_handle) if from_object_handle is not None else None
        to_object   = self.object_pool[pool_id].get_object(to_object_handle)

        if isinstance(from_object, WithOption) or isinstance(from_object, WithParam):
            # "Connection" here means option or param is part of atomic step.
            kind = "option" if isinstance(from_object, WithOption) else "parameter"
            if not isinstance(to_object, AtomicStep): raise SemanticError("with-option or with-param may only be connected to atomic steps.")
            
            if kind == "option": return Result(from_object in to_object.options)
            else: return Result(from_object in to_object.params)

        elif hasattr(to_object, "inputs"):
            # Only possible inputs are steps and sources.
            if isinstance(from_object, Step) or isinstance(from_object, Source):
                # True if the number of connections found is greater than 0, False otherwise.
                return Result(len(to_object.inputs.find_port_connection(port_name=to_port, entity=from_object)) > 0)

        return Result.fail()


    @interface
    def has_output_connection(self, pool_id, to_object_handle, to_port=None, from_object_handle=None):
        """
        Checks whether an output connection exists based on given parameters.
        
        :param pool_id: The ID of the pool the objects belong in.
        :type pool_id: `str`
        :param to_object_handle: The handle of the object to check output connections of.
        :type to_object_handle: `str`
        :param to_port: The port to check for output connections.
        :param to_port: `str`
        :param from_object_handle: The handle of the object to check whether there are output connections originating from it.
        :param from_object_handle: `str`
        :return: A result object with True if there are connections, False if there aren't, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        from_object = self.object_pool[pool_id].get_object(from_object_handle) if from_object_handle is not None else None
        to_object   = self.object_pool[pool_id].get_object(to_object_handle)
        
        if hasattr(to_object, "outputs"):
            # Only possible outputs are steps and sources.
            if isinstance(from_object, Step) or isinstance(from_object, Source):
                # True if the number of connections found is greater than 0, False otherwise.
                return Result(len(to_object.outputs.find_port_connection(port_name=to_port, entity=from_object)) > 0)

        return Result.fail()


    @interface
    def get_input_properties(self, pool_id, object_handle, port_name):
        """
        Returns the set of properties for a given input port on an object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the port properties of.
        :type object_handle: `str`
        :param port_name: The port to get the properties of.
        :type port_name: `str`
        :return: A result object with the properties, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "inputs"):
            properties = xp_object.inputs.get_port_properties(port_name)
            if properties is not None: return Result(properties)

        return Result.fail()


    @interface
    def get_output_properties(self, pool_id, object_handle, port_name):
        """
        Returns the set of properties for a given output port on an object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the port properties of.
        :type object_handle: `str`
        :param port_name: The port to get the properties of.
        :type port_name: `str`
        :return: A result object with the properties, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "outputs"):
            properties = xp_object.outputs.get_port_properties(port_name)
            if properties is not None: return Result(properties)

        return Result.fail()


    @interface
    def set_input_property(self, pool_id, object_handle, port_name, property_name, property_value):
        """
        Sets a property value for a given input port on an object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the port properties for.
        :type object_handle: `str`
        :param port_name: The port to set the properties for.
        :type port_name: `str`
        :param property_name: The name of the property.
        :type property_name: `str`
        :param property_value: The property value.
        :type property_value: `object`
        :return: A result object with True if the property was set, False if it wasn't, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "inputs"):
            return Result(xp_object.inputs.set_port_properties(port_name, property_name, property_value))

        return Result.fail()


    @interface
    def set_output_property(self, pool_id, object_handle, port_name, property_name, property_value):
        """
        Sets a property value for a given output port on an object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to set the port properties for.
        :type object_handle: `str`
        :param port_name: The port to set the properties for.
        :type port_name: `str`
        :param property_name: The name of the property.
        :type property_name: `str`
        :param property_value: The property value.
        :type property_value: `object`
        :return: A result object with True if the property was set, False if it wasn't, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "outputs"):
            return Result(xp_object.outputs.set_port_properties(port_name, property_name, property_value))

        return Result.fail()


    @interface
    def change_output_port_name(self, pool_id, object_handle, old_port_name, new_port_name):
        """
        Changes the output port from old to new.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to change the output ports for.
        :type object_handle: `str`
        :param old_port_name: The old port name.
        :type old_port_name: `str`
        :param new_port_name: The new port name.
        :type new_port_name: `str`
        :return: A result object with True if the port was changed, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        if hasattr(xp_object, "outputs"):
            xp_object.outputs.change_port_name(old_port_name, new_port_name)
            return Result.success()

        return Result.fail()


    # ┏━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                     ┃
    # ┃ Declaration methods ┃
    # ┃                     ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━┛

    @interface
    def get_step_specification(self, pool_id, object_handle, spec_type, name):
        """
        Returns a specification object based on type and name, from a given object. If none are found, it will be None.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the specs of.
        :type object_handle: `str`
        :param spec_type: The type of spec, for input / output / option specs.
        :type spec_type: `str in ['input', 'output', 'option']`
        :param name: The name associated with the spec.
        :type name: `str`
        :return: A result with the specification object if found, None if not found, and a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        # Specs are only valid for step declarations
        if isinstance(xp_object, StepDeclaration):
            if spec_type in ["input", "output", "option"]:
                specs = getattr(xp_object, spec_type + "_specs")
                return Result(specs.get(name, None))

        return Result.fail()


    @interface
    def set_step_specification(self, pool_id, object_handle, spec_type, name, spec_properties):
        """
        Sets a specification based on type, name, and properties, on a given object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the specs of.
        :type object_handle: `str`
        :param spec_type: The type of spec, for input / output / option specs.
        :type spec_type: `str in ['input', 'output', 'option']`
        :param name: The name associated with the spec. 
        :type name: `str` 
        :param spec_properties: A dictionary of properties to set for the spec. If None or empty, it will delete the named spec.
        :type spec_properties: `dict` or `None`
        :return: A success result if the specification was set, and a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        # Specs are only valid for step declarations
        if isinstance(xp_object, StepDeclaration):
            spec_type_map = {
                 "input": InputSpec,
                "output": OutputSpec,
                "option": OptionSpec
            }
            if spec_type in spec_type_map.keys() and not str_none_or_empty(name):
                specs = getattr(xp_object, spec_type + "_specs")

                if spec_properties is None: del specs[name]
                else: specs[name] = spec_type_map[spec_type](name, **spec_properties) # Create a new specification object from the given properties.
                
                return Result.success()

        return Result.fail()


    @interface
    def get_imports(self, pool_id, object_handle):
        """
        Returns the list of import URIs for the given object.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the imports of.
        :type object_handle: `str`
        :return: A result with the list of imports, of a fail result if something went wrong. This is a new list, and modifying this will not alter the actual imports.
        :rtype: `list(str)`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        # Imports are only valid for step declarations and libraries
        if isinstance(xp_object, StepDeclaration) or isinstance(xp_object, Library):
            return Result([*xp_object.imports])

        return Result.fail()


    @interface
    def set_import(self, pool_id, object_handle, href, old_href=None):
        """
        Adds a new URI to the list of imports for a given object.

        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param object_handle: The handle of the object to get the imports of.
        :type object_handle: `str`
        :param href: The URI to add to the imports. If None, nothing will be added. Cannot be empty.
        :type href: `str`
        :param old_href: A URI to remove from the old list, if given.
        :param old_href: `str`, optional
        :return: A result with True if a URI was added or removed, False if it wasn't, and a fail result otherwise.
        :rtype: `xproc.result.Result`
        """

        xp_object = self.object_pool[pool_id].get_object(object_handle)

        # Imports are only valid for step declarations and libraries
        if isinstance(xp_object, StepDeclaration) or isinstance(xp_object, Library):
            changed = False

            # Remove old href if given.
            if not str_none_or_empty(old_href) and old_href in xp_object.imports:
                xp_object.imports.remove(old_href)
                changed = True

            # Add new href
            if not str_none_or_empty(href): 
                xp_object.imports.append(href)
                changed = True

            return Result(changed)

        return Result.fail()


    # ┏━━━━━━━━━━━━━━━━━━━━━━━┓
    # ┃                       ┃
    # ┃ Specification methods ┃
    # ┃                       ┃
    # ┗━━━━━━━━━━━━━━━━━━━━━━━┛

    @interface
    def get_specification_from_name(self, pool_id, ent_type, ent_ns=XPROC_NAMESPACE):
        """
        Returns the specification based on type and namespace, without an object handle.
        
        :param pool_id: The ID of the pool the object belongs in.
        :type pool_id: `str`
        :param ent_type: The type of the step or vocabulary to get the specifications of.
        :type ent_type: `str`
        :param ent_ns: The namespace it belongs in.
        :type ent_ns: `str`
        :return: A result with the specification of the object, if it doesn't exist it will be a result with None.
        :rtype: `xproc.result.Result`
        """

        # Create a dummy entity and invoke get_specification().
        dummy = Entity(ent_type, ent_ns)
        return Result(dummy.get_specification())


    @interface
    def get_all_specifications(self, pool_id, kind="atomic"):
        """
        Returns a list of all specifications of the given type.
        
        :param pool_id: The pool ID to search in.
        :type pool_id: `str`
        :param kind: The kind of specification to return, "atomic" or "core". Defaults to "atomic".
        :param kind: `str in ['atomic', 'core']`, optional
        :return: A result with the list of specifications (separated by classes), of a fail result if something went wrong.
        :rtype: `xproc.result.Result`
        """

        specifications = {}

        if kind == "atomic":
            specifications["standard"] = [spec for (name, spec) in xproc.specifications.standard_atomic_specs.items()]
            if pool_id in xproc.specifications.user_defined_specs.keys():
                specifications["user"] = [spec for (name, spec) in xproc.specifications.user_defined_specs[pool_id]]
            return Result(specifications)

        elif kind == "core":
            for core_class, specs in xproc.specifications.core_specs.items():
                specifications[core_class] = [spec for (name, spec) in specs.items()]
            return Result(specifications)

        return Result.fail()



class ObjectPool:
    """
    The Object Pool is a way to contain the scope of a single pipeline. This makes it so that steps inside one pipeline aren't visible to others.    
    """

    # A named tuple for a single pool object entry.
    PoolObject = collections.namedtuple("PoolObject", ["object", "parent_handle"])


    def __init__(self):
        self.pool = {} # Collection of pool objects, mapped by object handle.


    def add_object(self, entity, parent_handle):
        """
        Adds a new object into the pool. If the semantics are violated, this will raise a `SemanticError`.
        
        :param entity: The entity to add to the pool.
        :type entity: `xproc.entity.Entity`
        :param parent_handle: The handle of the parent object, if applicable. Raises an `ObjectManagerError` if the handle is invalid.
        :type parent_handle: `str`
        :return: The handle of the newly created object.
        :rtype: `str`
        """        

        # New object handle.
        object_handle = str(uuid.uuid4())

        # Add the new object as part of a parent object - not applicable to WithOption or WithParam
        if not (isinstance(entity, WithOption) or isinstance(entity, WithParam)):
            if parent_handle in self.pool.keys():
                # Library check
                if isinstance(entity, Library): raise SemanticError("A library cannot be a child of any entity.")

                parent_object = self.pool[parent_handle].object

                # Variables
                if isinstance(entity, Variable):
                    if not hasattr(parent_object, "variables"): 
                        raise SemanticError("Cannot add a variable to something that does not have a subpipeline.")
                    parent_object.variables.append(entity)

                # Normal contents
                else:
                    if not hasattr(parent_object, "contents"): 
                        raise SemanticError("The parent object does not support children.")
                    parent_object.contents.append(entity)

                self.pool[object_handle] = self.PoolObject(entity, parent_handle)

            # Add the new object as root object.
            elif parent_handle is None:
                self.pool[object_handle] = self.PoolObject(entity, None)

            # A parent handle was supplied but no parent object was found.
            else: raise ObjectManagerError("Invalid parent handle.")

        # A WithOption or WithParam is always added with no parent - parent_handle is attached upon connection.
        else:
            self.pool[object_handle] = self.PoolObject(entity, None)

        return object_handle


    def remove_object(self, object_handle):
        """
        Removes the object from the pool.
        
        :param object_handle: The handle of the object to remove.
        :type object_handle: `str`
        :return: True if it was successfully deleted, otherwise it will throw an exception.
        :rtype: `bool`
        """

        if object_handle not in self.pool.keys(): raise ObjectManagerError("Invalid object handle.")

        removing_object = self.pool[object_handle].object
        parent_handle   = self.pool[object_handle].parent_handle

        # Remove the object from parent if applicable.
        if parent_handle is not None:
            if parent_handle not in self.pool.keys(): raise ObjectManagerError("Invalid parent handle.")

            parent_object = self.pool[parent_handle].object

            # Enclose the removal in a try-except just in case something goes wrong.
            try:
                # Options
                if isinstance(removing_object, WithOption):
                    parent_object.options.remove(removing_object)
                    
                # Parameters
                elif isinstance(removing_object, WithParam):
                    parent_object.params.remove(removing_object)

                # Variables
                elif isinstance(removing_object, Variable):
                    parent_object.variables.remove(removing_object)

                # Normal contents
                else:
                    parent_object.contents.remove(removing_object)

            except Exception:
                raise ObjectManagerError("For some reason, the removal didn't work.")

            # Make sure that this object is removed from the parent's output.
            if hasattr(parent_object, "outputs"): 
                parent_object.outputs.remove_port_connections(entity=removing_object)

        # Remove child objects.
        if hasattr(removing_object, "options"):
            for option in removing_object.options:
                child_handle = self.get_handle(option)
                if child_handle is not None: self.remove_object(child_handle)

        if hasattr(removing_object, "params"):
            for param in removing_object.params:
                child_handle = self.get_handle(param)
                if child_handle is not None: self.remove_object(child_handle)

        if hasattr(removing_object, "variables"):
            for variable in removing_object.variables:
                child_handle = self.get_handle(variable)
                if child_handle is not None: self.remove_object(child_handle)

        if hasattr(removing_object, "contents"):
            for child in removing_object.contents:
                child_handle = self.get_handle(child)
                if child_handle is not None: self.remove_object(child_handle)

        del self.pool[object_handle]
        return True

                
    def get_handle(self, object_to_find):
        """
        Gets the object handle of the specified object.
        
        :param object_to_find: An object expected to be in the pool, typically an entity.
        :type object_to_find: `xproc.entity.Entity`
        :return: The handle of the object if found, None of not found.
        :rtype: `str` or `None`
        """

        for object_handle in self.pool.keys():
            if self.pool[object_handle].object is object_to_find: return object_handle
        return None


    def get_object(self, object_handle):
        """
        Gets the underlying object from the handle. Raises an `ObjectManagerError` if the object handle is not found.
        
        :param object_handle: The handle of the object.
        :type object_handle: `str`
        :return: The object corresponding to the handle.
        :rtype: `xproc.entity.Entity`
        """

        if object_handle not in self.pool.keys(): 
            raise ObjectManagerError("Invalid object handle.")
        return self.pool[object_handle].object


    def get_parent(self, object_handle):
        """
        Gets the parent object handle of the given handle. Raises an `ObjectManagerError` if the object handle is not found.
        
        :param object_handle: The handle of the object.
        :type object_handle: `str`
        :return: The object handle of the parent.
        :rtype: `str`
        """

        if object_handle not in self.pool.keys(): 
            raise ObjectManagerError("Invalid object handle.")
        return self.pool[object_handle].parent_handle


    def update_parent(self, object_handle, new_parent_handle):
        """
        Sets a new parent for the object. Raises an `ObjectManagerError` if the object handle is not found, or the new parent handle is not None and not found.
        
        :param object_handle: The handle of the object.
        :type object_handle: `str`
        :param new_parent_handle: The new parent handle for the object.
        :type new_parent_handle: `str` or `None`
        """

        if object_handle not in self.pool.keys(): 
            raise ObjectManagerError("Invalid object handle.")
        if new_parent_handle is not None and new_parent_handle not in self.pool.keys(): 
            raise ObjectManagerError("Invalid new parent handle.")
                
        pool_object = self.pool[object_handle]
        self.pool[object_handle] = self.PoolObject(pool_object.object, new_parent_handle)



object_manager = ObjectManager()
