"""
This module contains concrete classes for step-related entities.
"""

from xproc.entity  import Entity, NamedEntity, AggregateEntity
from xproc.portset import PortSet
from xproc.utils   import XPROC_NAMESPACE



class Step:
    """
    An empty class, meant to identify steps.
    """
    
    pass



class AtomicStep(Step, NamedEntity):
    """
    The class representing an atomic step in the pipeline. Along with inputs, an instance can specify a list of options and parameters.
    """

    def __init__(self, ent_type, ent_name="", ent_ns=XPROC_NAMESPACE):
        super().__init__(ent_type, ent_name, ent_ns)
        self.entity_class = "atomic"

        self.inputs  = PortSet(singular=False)
        self.options = []
        self.params  = []



class CompoundStep(Step, AggregateEntity):
    """
    The class representing a compound step in the pipeline. Note that inputs are always specific language constructs (p:iteration-source, p:viewport-source, or p:xpath-context).
    """

    def __init__(self, ent_type, ent_name=""):
        super().__init__(ent_type, ent_name)
        self.entity_class = "compound"
        
        if ent_type == "viewport":
            self.inputs = PortSet(singular=True, unary=True)
        else:
            self.inputs = PortSet(singular=False, unary=True)

        self.outputs = PortSet(singular=False)
        


class MulticontainerStep(Step, AggregateEntity):
    """
    The class representing a multicontaier step in the pipeline. A single input is allowed if the type is a p:choose (p:xpath-xontext)
    """

    def __init__(self, ent_type, ent_name=""):
        super().__init__(ent_type, ent_name)
        self.entity_class = "multicontainer"

        if ent_type == "choose":
            self.inputs = PortSet(singular=True, unary=True)




class StepDeclaration(Step, AggregateEntity):
    """
    The class representing a step declaration in the pipeline. There are three "specifications": inputs, outputs, and options. These are different from connections, and the values for each of them are defined in specification.py.

    Declaration can also have a list of URLs for import.
    """

    def __init__(self, ent_type, ent_name=""):
        super().__init__(ent_type, ent_name)
        self.entity_class = "declaration"
        
        self.input_specs          = {}
        self.output_specs         = {}
        self.option_specs         = {}
        self.namespace            = None # Namespace is used for underlying type.
        self.atomic_specification = None

        self.outputs = PortSet(singular=False)
        self.imports = []


    def __setattr__(self, name, value):
        """
        Attribute setter override to ensure namespace is not the XProc namespace.
        """

        if name == "namespace":
            if value != XPROC_NAMESPACE:
                self.__dict__[name] = value

        else: self.__dict__[name] = value



    def compile_specs(self):
        """
        Creates an atomic specification based on values. If the specification has already been created, it will be updated.        
        """

        pass



class Library(Entity):
    """
    The class representing a step library. The contents must be step declarations, and can also have a list of URLs for import.
    """
    
    def __init__(self):
        super().__init__("library")
        self.entity_class = "library"

        self.contents = []
        self.imports  = []
