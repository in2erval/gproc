"""
This module contains the Source entity class.
"""

from xproc.entity import Entity



class Source(Entity):
    """
    The class representing a data source in the pipeline. 
    """

    def __init__(self, ent_type):
        super().__init__(ent_type)
        self.entity_class = "source"