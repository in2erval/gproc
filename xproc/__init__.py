"""
The main module setup - exposes the object manager and loads known specifications into specs dictionary.
"""

import os, xproc

from lxml                 import etree
from xproc.export         import export_to_file
from xproc.object_manager import object_manager
from xproc.specifications import parse_library, parse_core, set_standard_atomic_specs, set_core_specs

DATA_FOLDER         = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/")
STANDARD_SPECS_FILE = os.path.join(DATA_FOLDER, "xproc-1.0.xpl")
CORE_SPECS_FILE     = os.path.join(DATA_FOLDER, "xproc-1.0-core.xml")


# Read standard atomic steps.
standard_atomic_specs_xmlroot = etree.parse(STANDARD_SPECS_FILE).getroot()
set_standard_atomic_specs(parse_library(standard_atomic_specs_xmlroot))

# Read core specifications
core_specs_xmlroot = etree.parse(CORE_SPECS_FILE).getroot()
set_core_specs(parse_core(core_specs_xmlroot))