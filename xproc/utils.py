"""
This module contains various helper functions and values used in this package.
"""

XPROC_NAMESPACE = "http://www.w3.org/ns/xproc"


def str_none_or_empty(string):
    """
    Checks whether a string is None or empty.
    """
    
    return (string is None) or (string == "")