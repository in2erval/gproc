"""
This module contains classes related to step specifications, including Input / Output / Option declarations, and specifications for atomic and core steps.
"""

from xproc.errors import ParseError
from xproc.utils  import XPROC_NAMESPACE

GPROC_NAMESPACE = "http://inf.ed.ac.uk"



class InputSpec:
    """
    A class representing an input declaration.
    """
    
    def __init__(self, port, sequence=False, primary=False, kind="document", select=""):
        self.port     = port
        self.sequence = sequence
        self.primary  = primary
        self.kind     = kind
        self.select   = select

    
    def __setattr__(self, name, value):
        """
        Attribute setter override to ensure valid values.
        """

        if (name == "sequence") or (name == "primary"):
            if str(value).lower() in ["true", "false"]:
                self.__dict__[name] = value

        elif name == "kind":
            if str(value) in ["document", "parameter"]:
                self.__dict__[name] = value

        else:
            self.__dict__[name] = value



class OutputSpec:
    """
    A class representing an output declaration.
    """
    
    def __init__(self, port, sequence=False, primary=False):
        self.port     = port
        self.sequence = sequence
        self.primary  = primary

    
    def __setattr__(self, name, value):
        """
        Attribute setter override to ensure valid values.
        """

        if (name == "sequence") or (name == "primary"):
            if str(value).lower() in ["true", "false"]:
                self.__dict__[name] = value

        else:
            self.__dict__[name] = value



class OptionSpec:
    """
    A class representing an option declaration.
    """
    
    def __init__(self, name, namespace=None, required=False, select=""):
        self.name      = name
        self.namespace = namespace
        self.required  = required
        self.select    = select

    
    def __setattr__(self, name, value):
        """
        Attribute setter override to ensure valid values.
        """

        if name == "required":
            if str(value).lower() in ["true", "false"]:
                self.__dict__[name] = value

        else:
            self.__dict__[name] = value


class PropertySpec:
    """
    A class representing a generic property in a core specification.    
    """
    
    def __init__(self, name, required):
        self.name     = name
        self.required = required

    
    def __setattr__(self, name, value):
        """
        Attribute setter override to ensure valid values.
        """

        if name == "required":
            if str(value).lower() in ["true", "false"]:
                self.__dict__[name] = value

        else:
            self.__dict__[name] = value



class AtomicSpecification:
    """
    A class representing an atomic step's specifications - also includes its inputs, outputs, and options.    
    """
    
    def __init__(self, name, namespace, documentation, inputs, outputs, options):
        self.kind          = "atomic"
        self.name          = name
        self.namespace     = namespace or XPROC_NAMESPACE
        self.documentation = documentation

        self.inputs  = {**inputs}
        self.outputs = {**outputs}
        self.options = {**options}
 


class CoreSpecification:
    """
    A class representing an XProc step which is part of the core vocabulary - also includes its inputs, outputs, properties, and additionals.   
    """

    def __init__(self, core_class, name, documentation, inputs, outputs, properties, additionals):
        self.kind          = "core"
        self.core_class    = core_class
        self.name          = name
        self.documentation = documentation

        self.inputs      = {**inputs}  # dict of InputSpec
        self.outputs     = {**outputs} # dict of OutputSpec
        self.properties  = {**properties} # dict of PropertySpec
        self.additionals = [*additionals] # list of string



# Dictionary of specifications.
standard_atomic_specs = {} # str -> AtomicSpecification
user_defined_specs    = {} # str -> dict<str -> AtomicSpecification>
core_specs            = {} # str -> dict<str -> CoreSpecification>


def set_standard_atomic_specs(specs):
    """
    Setter for the standard atomic specs dictionary.
    """

    global standard_atomic_specs
    standard_atomic_specs = specs


def set_core_specs(specs):
    """
    Setter for the core specs dictionary.
    """

    global core_specs
    core_specs = specs


def parse_library(lxml_node):
    """
    Parses the given lxml node as a p:library element, and returns a dictionary of specifications.
    
    :param lxml_node: An lxml Element which corresponds to a p:library.
    :type lxml_node: `lxml.etree._Element`
    :return: A dictionary of atomic specifications.
    :rtype: `dict(str, AtomicSpecification)`
    """

    specifications = {}
    error          = ""

    # Enclose the parsing process to capture error messages.
    try:
        if lxml_node.tag != "{{{}}}library".format(XPROC_NAMESPACE):
            error = "Expected p:library as root element."

        else:
            for declaration in lxml_node.findall("{{{}}}declare-step".format(XPROC_NAMESPACE)):
                step_type          = declaration.get("type")
                step_specification = parse_step_declaration(declaration) # If this throws an error, nothing ends up returning.
                specifications[step_type] = step_specification
            
            return specifications

    except Exception as exc:
        error = str(exc)

    # If it gets to this point, then the parse failed.
    raise ParseError("Parsing error: {}".format(error))


def parse_step_declaration(lxml_node):
    """
    Parses the given lxml node as a p:declare-step element, and returns an atomic specification.
    
    :param lxml_node: An lxml Element which corresponds to a p:declare-step
    :type lxml_node: `lxml.etree._Element`
    :return: An atomic specification based on contents of the node.
    :rtype: `AtomicSpecification`
    """

    error = ""

    # Enclose the parsing process to capture error messages.
    try:
        if lxml_node.tag != "{{{}}}declare-step".format(XPROC_NAMESPACE):
            error = "Expected p:declare-step as root element."

        else:
            # Namespace URL
            namespace_split = lxml_node.get("type").split(':')
            namespace_tag   = namespace_split[0]
            namespace_url   = lxml_node.nsmap[namespace_tag]

            # Step type
            step_type = namespace_split[-1]

            # Documentation
            documentation = ""
            documentation_node = lxml_node.find("{{{}}}documentation".format(XPROC_NAMESPACE))
            if documentation_node is not None:
                documentation = documentation_node.text

            # Inputs
            inputs = {}
            for step_input in lxml_node.findall("{{{}}}input".format(XPROC_NAMESPACE)):
                port     = step_input.get("port")
                sequence = step_input.get("sequence", "false").lower() == "true" # string to bool conversion.
                primary  = step_input.get("primary", "false").lower() == "true"
                kind     = step_input.get("kind", "document")
                select   = step_input.get("select", "")

                inputs[port] = InputSpec(port, sequence, primary, kind, select)

            # Outputs
            outputs = {}
            for step_output in lxml_node.findall("{{{}}}output".format(XPROC_NAMESPACE)):
                port     = step_output.get("port")
                sequence = step_output.get("sequence", "false").lower() == "true"
                primary  = step_output.get("primary", "false").lower() == "true"
                
                outputs[port] = OutputSpec(port, sequence, primary)

            # Options
            options = {}
            for step_option in lxml_node.findall("{{{}}}option".format(XPROC_NAMESPACE)):
                qname       = step_option.get("name") # Options can have namespace prefixes as part of their name.
                qname_split = qname.split(':')
                ncname      = qname_split[-1]
                namespace   = None if len(qname_split) == 1 else lxml_node.nsmap[qname_split[0]] # Refer to nsmap for the URL.
                required    = step_option.get("required", "false").lower() == "true"
                select      = step_option.get("select", "")

                options[qname] = OptionSpec(ncname, namespace, required, select)
            
            return AtomicSpecification(step_type, namespace_url, documentation, inputs, outputs, options)

    except Exception as exc:
        error = str(exc)

    # If it gets to this point, then the parse failed.
    raise ParseError("Parsing error: {}".format(error))


def parse_core(lxml_node):
    """
    Parses the given lxml node as a g:core element, and returns a dictionary of core specifications.
    
    :param lxml_node: An lxml Element which corresponds to a p:declare-step
    :type lxml_node: `lxml.etree._Element`
    :return: A dictionary of core specifications based on contents of the node.
    :rtype: `dict(str, dict(str, CoreSpecification))`
    """

    specifications = {}
    error          = ""

    # Enclose the parsing process to capture error messages.
    try:
        if lxml_node.tag != "{{{}}}core".format(GPROC_NAMESPACE):
            error = "Expected g:core as root element."

        else:
            for specification in lxml_node.findall("{{{}}}specification".format(GPROC_NAMESPACE)):
                # Step class and type
                core_class = specification.get("class")
                core_type  = specification.get("type").split(':')[-1] # The step types are QNames with the XProc namespace - just get the NCName.

                # Documentation
                documentation = ""
                documentation_node = specification.find("{{{}}}documentation".format(GPROC_NAMESPACE))
                if documentation_node is not None:
                    documentation = documentation_node.text

                # Inputs
                inputs = {}
                for core_input in specification.findall("{{{}}}input".format(GPROC_NAMESPACE)):
                    port     = core_input.get("port")
                    sequence = core_input.get("sequence", "false").lower() == "true" # string to bool conversion.
                    primary  = core_input.get("primary", "false").lower() == "true"
                    kind     = core_input.get("kind", "document")
                    select   = core_input.get("select", "")

                    inputs[port] = InputSpec(port, sequence, primary, kind, select)

                # Outputs
                outputs = {}
                for core_output in specification.findall("{{{}}}output".format(GPROC_NAMESPACE)):
                    port     = core_output.get("port")
                    sequence = core_output.get("sequence", "false").lower() == "true"
                    primary  = core_output.get("primary", "false").lower() == "true"
                    
                    outputs[port] = OutputSpec(port, sequence, primary)

                # Properties
                properties = {}
                for core_property in specification.findall("{{{}}}property".format(GPROC_NAMESPACE)):
                    name     = core_property.get("name")
                    required = core_property.get("required", "false").lower() == "true"

                    properties[name] = PropertySpec(name, required)

                # Additionals
                additionals = []
                for core_additional in specification.findall("{{{}}}additional".format(GPROC_NAMESPACE)):
                    additionals.append(core_additional.get("type"))

                # Create a dictionary for each step class.
                if core_class not in specifications.keys(): specifications[core_class] = {}
                specifications[core_class][core_type] = CoreSpecification(core_class, core_type, documentation, inputs, outputs, properties, additionals)

            return specifications

    except Exception as exc:
        error = str(exc)

    # If it gets to this point, then the parse failed.
    raise ParseError("Parsing error: {}".format(error))