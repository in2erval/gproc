"""
This module contains base class declarations for all entities in the in-memory representation.
"""

import xproc.specifications

from xproc.utils import XPROC_NAMESPACE



class Entity:    
    """
    The base class for all entities. 
    """    
    
    def __init__(self, ent_type, ent_ns=XPROC_NAMESPACE):
        self.type         = ent_type
        self.namespace    = ent_ns
        self.entity_class = "entity"

        self.documentation = ""
        self.properties    = {}
        self.additionals   = {}
    

    def get_specification(self, pool_id=None):
        """
        Returns the matching Specification object based on type and namespace. It is also possible to supply a pool ID to restrict the search on.
        
        :param pool_id: The pool ID to search in, defaults to None. If None, look through every pool.
        :type pool_id: `str`, optional
        :return: A specification object, either atomic or core.
        :rtype: `xproc.specification.AtomicSpecification` or `xproc.specification.CoreSpecification`
        """


        if (self.namespace == XPROC_NAMESPACE) or (self.entity_class != "atomic"):
            # Look through the standard atomic specs and core specs.
            for name, spec in xproc.specifications.standard_atomic_specs.items():
                if name.split(':')[-1] == self.type: return spec

            for _, specs in xproc.specifications.core_specs.items():
                for name, spec in specs.items():
                    if name.split(':')[-1] == self.type: return spec

        else:
            # Look through the user-defined specs.
            if pool_id is not None:
                # Restrict to specific pool.
                for name, spec in xproc.specifications.user_defined_specs[pool_id].items():
                    if (name == (self.type, self.namespace)): return spec

            else:
                for _, specs in xproc.specifications.user_defined_specs.items():
                    for name, spec in specs.items():
                        if (name == (self.type, self.namespace)): return spec

        return None



class NamedEntity(Entity):
    """
    The base class for all named entites - steps, options, parameters, and variables.    
    """
    
    def __init__(self, ent_type, ent_name="", ent_ns=XPROC_NAMESPACE):
        super().__init__(ent_type, ent_ns)

        self.name = ent_name



class AggregateEntity(NamedEntity):
    """
    The base class for all named entities with subpipelines - compound steps, multicontainer steps, step declarations, and libraries.
    """
    
    def __init__(self, ent_type, ent_name="", ent_ns=XPROC_NAMESPACE):
        super().__init__(ent_type, ent_name, ent_ns)

        self.variables = []
        self.contents  = []
