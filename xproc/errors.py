"""
This module contains Exception classes for other modules to use.
"""



class SemanticError(Exception):
    pass



class ObjectManagerError(Exception):
    pass



class ParseError(Exception):
    pass