"""
This module contains the WithOption, WithParam, and Variable classes.
"""

from xproc.entity  import NamedEntity
from xproc.portset import PortSet
from xproc.utils   import XPROC_NAMESPACE



class OPV:
    """
    An empty class, meant to identify options, parameters, and variables.
    """
    
    pass



class WithOption(OPV, NamedEntity):
    """
    The class representing a supplied option value for an atomic step. A name and a namespace can be specified.
    """
    
    def __init__(self, ent_name):
        super().__init__("with-option", ent_name)
        self.entity_class = "with-option"

        self.inputs = PortSet(singular=True, unary=True)



class WithParam(OPV, NamedEntity):
    """
    The class representing a supplied parameter value for an atomic step. A name and a namespace can be specified.
    """
    
    def __init__(self, ent_name):
        super().__init__("with-param", ent_name)
        self.entity_class = "with-param"

        self.inputs = PortSet(singular=True, unary=True)



class Variable(OPV, NamedEntity):
    """
    The class representing a variable in the pipeline. A name and a namespace can be specified.
    """
    
    def __init__(self, ent_name):
        super().__init__("variable", ent_name)
        self.entity_class = "variable"

        self.inputs = PortSet(singular=True, unary=True)
