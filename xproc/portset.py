"""
This module contains classes related to ports.
"""

from xproc.errors import SemanticError
from xproc.source import Source
from xproc.utils  import str_none_or_empty



class PortSet:
    """
    The class representing a set of ports in an entity. The portset can be singular (each port can only take one connection) or unary (only one port connection is allowed).
    """
    
    def __init__(self, singular, unary=False):
        self.__is_singular = singular
        self.__is_unary    = unary

        self.ports = {} # Mapping of port names to Port objects.


    def add_port_connection(self, port_name, entity, entity_port_name):
        """
        Adds a new port connection, or overwrites an existing port connection. If the portset is unary, then this raises a `SemanticError` if a different connection exists. port_name and entity cannot be None.
        
        :param port_name: The name of the port to create a connection for.
        :type port_name: `str`
        :param entity: The XProc entity to connect.
        :type entity: `xproc.entity.Entity`
        :param entity_port_name: The port name on the connected entity to connect to.
        :type entity_port_name: `str`
        """

        if len(self.ports) > 0 and port_name not in self.ports.keys() and self.__is_unary: 
            raise SemanticError("This entity can only accept one connection.")
        if str_none_or_empty(port_name): 
            raise SemanticError("Port name cannot be None or empty.")

        # Initialise a new Port object if this is a new port.
        if port_name not in self.ports.keys():
            self.ports[port_name] = Port(port_name, self.__is_singular)

        self.ports[port_name].add_connection(entity, entity_port_name)


    def change_port_name(self, old_port_name, new_port_name):
        """
        Changes the port name from old to new.
        
        :param old_port_name: The port name to change.
        :type old_port_name: `str`
        :param new_port_name: The new port name to change to.
        :type new_port_name: `str`
        """

        if str_none_or_empty(old_port_name) or str_none_or_empty(new_port_name): raise SemanticError("Port names cannot be none.")

        # Move around references and also update the field in the Port object.
        if old_port_name in self.ports.keys():
            self.ports[new_port_name] = self.ports[old_port_name]
            self.ports[new_port_name].port_name = new_port_name
        else: raise SemanticError("The port name to change from was not found.")



    def get_all_connections(self):
        """
        Returns a list of all connections.        
        """

        return [conn for port in self.ports.values() for conn in port.connections]


    def find_port_connections(self, *, port_name=None, entity=None):
        """
        Finds the list of port connections based on port name or entity. If both are given, port name is prioritised.
        
        :param port_name: The name of the port from the portset, defaults to None
        :param port_name: `str`, optional
        :param entity: The entity to find connections from, defaults to None
        :param entity: `xproc.entity.Entity`, optional
        :return: List of `xproc.portset.Connection` objects.
        :rtype: `list(xproc.portset.Connection)`
        """

        # Port name given, and it exists in the portset
        if not str_none_or_empty(port_name) and (port_name in self.ports.keys()):
            return [*self.ports[port_name].connections]

        # Entity given
        elif entity is not None:
            # List comprehension to get all connections with matching entity.
            connections = [conn for conn in self.get_all_connections() if conn.entity is entity]
            return connections

        # Neither were given, or port name does not exist in portset.
        return []


    def remove_port_connections(self, *, port_name=None, entity=None, entity_port_name=None):
        """
        Removes port connections based on given conditions. If the entity port name is given, the entity must also be given. Returns True if the deletion was successful, and False if it wasn't.
        
        :param port_name: The name of the port from the portset, defaults to None
        :param port_name: `str`, optional
        :param entity: The entity to find connections from, defaults to None
        :param entity: `xproc.entity.Entity`, optional
        :param entity_port_name: The port name on the entity, defaults to None
        :param entity_port_name: `str`, optional
        :return: True if at least one connection was deleted, False otherwise.
        :rtype: `bool`
        """

        if not str_none_or_empty(port_name):
            if port_name in self.ports.keys():
                if entity is None:
                    del self.ports[port_name]
                    return True
                else:
                    result = self.ports[port_name].remove_connection(entity, entity_port_name)
                    if (self.ports[port_name].connections) == 0: del self.ports[port_name]
                    return result

            return False

        elif entity is not None:
            deleted = False

            # Iterate through all ports.
            for port_name in [*self.ports.keys()]: # Use a separate list of keys since the iterator may change.
                port = self.ports[port_name]
                deleted = deleted or port.remove_connection(entity, entity_port_name)
                if len(port.connections) == 0: del self.ports[port_name]

            return deleted

        return False


    def remove_all_ports(self):
        """
        Resets the portset to have no connections.        
        """

        self.ports = {}


    def get_port_properties(self, port_name):
        """
        Returns the set of properties for the given port. 
        
        :param port_name: The port name to get the properties of.
        :type port_name: `str`
        :return: A dictionary of properties. This is a new copy.
        :rtype: `dict`
        """

        if port_name in self.ports.keys():
            return {**self.ports[port_name].properties} # Return a new dict so that editing it does not change contents.
        return None


    def set_port_properties(self, port_name, property_name, property_value):
        """
        Sets a given port property.
        
        :param port_name: The port name to set the properties of.
        :type port_name: `str`
        :param property_name: The name of the property. Cannot be None or empty.
        :type property_name: `str`
        :param property_value: The value for the property.
        :type property_value: `object`
        :return: True if the property was set properly, False otherwise.
        :rtype: `bool`
        """

        if (port_name in self.ports.keys()) and not str_none_or_empty(property_name):
            self.ports[port_name].properties[property_name] = property_value
            return True
        return False



class Port:
    """
    A class for describing a single port. Various properties can also be set.    
    """
    
    def __init__(self, port_name, singular):
        self.__is_singular = singular

        self.port_name   = port_name
        self.connections = []
        self.properties  = {}


    def add_connection(self, entity, entity_port_name):
        """
        Adds a new connection to this port. If this is singular, then it will not be accepted if a connection already exists. `entity` cannot be none, and the same connection must not exist already.
        
        :param entity: The XProc entity to connect.
        :type entity: `xproc.entity.Entity`
        :param entity_port_name: The port name on the connected entity.
        :type entity_port_name: `str`
        """

        if entity is None: raise SemanticError("The connecting entity cannot be None.")

        if self.__is_singular and len(self.connections) > 0: 
            raise SemanticError("This port is singular, so only one connection can be made at a time.")

        else: 
            # A p:empty cannot be connected unless it is the only one.
            if isinstance(entity, Source) and (entity.type == "empty"):
                if len(self.connections) > 0: raise SemanticError("An empty source can only be connected as a single connection.")

            # Check if it exists.
            exists = None
            for connection in self.connections:
                if connection.entity is entity: exists = Connection

            # Change the entity port name.
            if exists is not None: exists.entity_port_name = entity_port_name or ""

            # Add to internal list. 
            else:
                new_connection = Connection(entity, entity_port_name or "")
                self.connections.append(new_connection)


    def remove_connection(self, entity, entity_port_name=None):
        """
        Removes a connection from this port. If the removal makes the list of connections empty, this Port object should be dereferenced. Returns True if a connection was deleted, and False otherwise.
        
        :param entity: The XProc entity to remove.
        :type entity: `xproc.entity.Entity`
        :param entity_port_name: The port name on the connected entity. If None, any port name is matched.
        :type entity_port_name: `str`
        :return: True if a connection was removed, False otherwise.
        :rtype: `bool`
        """

        for conn in self.connections:
            if conn == Connection(entity, entity_port_name):
                self.connections.remove(conn)
                return True
        
        return False



class Connection:
    """
    A class for describing a single connection.    
    """
    
    def __init__(self, entity, entity_port_name):
        if entity is None: raise SemanticError("The connecting entity cannot be None.")

        self.entity           = entity
        self.entity_port_name = entity_port_name


    def __eq__(self, other):
        """
        Equality comparison overload.
        """
        
        if isinstance(other, Connection):
            if other.entity_port_name is None: return other.entity is self.entity
            return (other.entity is self.entity) and (other.entity_port_name == self.entity_port_name)

        return False
