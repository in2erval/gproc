"""
This module contains classes which encapsulate results from interfacing with the object manager.
"""


class Result:
    """
    A class which represents a result of applying an operation. If it was successful, the relevant data will be in the "data" field. If it was unsuccessful, the exception object will be in the "exception" field.    
    """

    def __init__(self, data, exception=None):
        self.data      = data
        self.exception = exception


    @staticmethod
    def error(exception):
        """
        Creates a result object based on the given exception.
        
        :param exception: The exception to encode in the result.
        :type exception: `Exception`
        :return: A result object with the exception encoded.
        :rtype: `Result`
        """

        return Result(None, exception)

    
    @staticmethod
    def success():
        """
        Creates a result object which indicates a success.
        
        :return: A result object with True as the data.
        :rtype: `Result`
        """
        return Result(True, None)


    @staticmethod
    def fail():
        """
        Creates a result object which indicates a failure.
        
        :return: A result object with a generic exception.
        :rtype: `Result`
        """
        return Result(None, Exception("Task failed."))

