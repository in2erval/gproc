from xproc import object_manager



pool_id = object_manager.create_pool()

result1 = object_manager.create_object(pool_id, "with-option", ent_name="new")
if result1.exception is None:
    # do things
    pass

withoption_handle = result1.data

withoption_object = object_manager.get_specification(pool_id, withoption_handle)

input_specs_result = object_manager.get_specification_from_name(pool_id, "input")

all_atomic_specs = object_manager.get_all_specifications(pool_id)
all_core_specs = object_manager.get_all_specifications(pool_id, kind="core")

k = 1